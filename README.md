# Chatify Chat App

Chatify is a chat application that allows you to connect with friends, create group chats, and communicate using various features such as emojis, GIFs, photos, and files. The app also provides additional functionalities like profile editing, notifications, friend management, and audio/video calls, everything happening in real-time. Additionally, it includes a playground room where you can enjoy music, watch YouTube videos, draw, and play brain-training games.

## What to expect as features ?
- **Desktop app version:** You can choose if you prefer a desktop version of the app or use it directly in the browser.
- **Responsive design:** You can enjoy the app regardless of your screen size or whether you use a mobile device.
- **Edit Profile:** You can customize your profile information, including username, display name, profile picture, about info, phone number, password and delete your profile if needed.
- **Chat:** You can have one-on-one conversations with your friends in real-time.
- **Create/Delete Group Chats:** You have the ability to create group chats, invite friends, and delete group chats if needed.
- **React with Emojis:** You can express your emotions and reactions using a wide range of emojis.
- **Send GIFs:** You can search and send animated GIFs within the chat.
- **Send Photos and Files:** You can share photos and different file types directly through the chat interface.
- **Receive Chat Notifications:** You will receive notifications when you receive new messages in your chats.
- **Add/Remove Friends:** You can add or remove friends from your contact list.
- **Single and Group Calls:** You can make audio and video calls to individuals or groups.
- **Review your call history:** You can review recent or old conversations history.
- **Playground Room:** You can access a dedicated room where you can listen to music, watch YouTube videos, draw, and play brain-training games.

## Technologies Used

- Firebase: Used for real-time data synchronization, authentication, and cloud storage.
- JavaScript: The primary programming language used in the project.
- React: JavaScript library for building the user interface.
- Chakra UI: Component library for creating a visually appealing and responsive UI.
- Vite: Used for fast server development.
- Electron: Served for creating a desktop application version of the app.

## Installation

1. Clone the repository: `git clone https://gitlab.com/AlexTsari/final-project.git`
2. Navigate to the project directory: `cd chat-app`
3. Install the dependencies: `npm install --force`
4. Write `npm run dev` in the console, it will automatically load the app.
5. If you prefer the browser version you can use the url displayed in the console.
6. Enjoy our chat app.

## Contact

If you have any questions or suggestions, feel free to reach out to the project maintainers:

- Krasimir Mihov: kmihov99@gmail.com
- Petko Simeonov: petkosimeonov@yahoo.com
- Aleksander Tsarigradski: aleksandertsarigradski@gmail.com


We hope you enjoy using Chatify Chat App!