const dummy = {
    "users": {
        "uid1": {
          "username": "Tom",
          "phoneNumber": "Cruise",
          "photo": "https://jsonformatter.org/img/tom-cruise.jpg",
          "email": "lala@abv.bg",
          "birthDay": "12/05/2015",
          "nationality": "armenian",
          "createdOn": "19/04/2022",
          "friendList": {
            "some_user": true,
            "other_user": true
          },
          "isAdmin": true,
          "isBlocked": true,
        },
        "uid2": {
          "username": "Alice",
          "phoneNumber": "Smith",
          "photo": "https://jsonformatter.org/img/alice-smith.jpg",
          "email": "alice@example.com",
          "birthDay": "02/10/1990",
          "nationality": "american",
          "createdOn": "25/07/2021",
          "friendList": {
            "another_user": true
          },
          "isAdmin": false,
          "isBlocked": false
        }
    },
    "messages": {
        "uid5": {
          "channel_id": "uid3",
          "sender_id": "owner",
          "content": "Hello, how are you?",
          "timestamp": "2023-05-19T10:30:00Z"
        },
        "uid6": {
          "channel_id": "uid3",
          "sender_id": "sender",
          "content": "I'm good, thanks!",
          "timestamp": "2023-05-19T10:31:00Z"
        },
        "uid7": {
            "channel_id": "uid3",
            "sender_id": "owner",
            "content": "Hello, how are you?",
            "timestamp": "2023-05-19T10:30:00Z"
        },
        "uid8": {
            "channel_id": "uid3",
            "sender_id": "owner",
            "content": "Hello, how are you?",
            "timestamp": "2023-05-19T10:30:00Z"
        },
        "uid9": {
            "channel_id": "uid3",
            "sender_id": "owner",
            "content": "Hello, how are you?",
            "timestamp": "2023-05-19T10:30:00Z"
        },
        "uid10": {
            "channel_id": "uid3",
            "sender_id": "owner",
            "content": "Hello, how are you?",
            "timestamp": "2023-05-19T10:30:00Z"
        },
        "uid11": {
            "channel_id": "uid3",
            "sender_id": "owner",
            "content": "Hello, how are you?",
            "timestamp": "2023-05-19T10:30:00Z"
        },
        "uid12": {
            "channel_id": "uid3",
            "sender_id": "owner",
            "content": "Hello, how are you?",
            "timestamp": "2023-05-19T10:30:00Z"
        },
        "uid13": {
            "channel_id": "uid3",
            "sender_id": "owner",
            "content": "Hello, how are you?",
            "timestamp": "2023-05-19T10:30:00Z"
        },
        "uid14": {
            "channel_id": "uid3",
            "sender_id": "owner",
            "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "timestamp": "2023-05-19T10:30:00Z"
        },
        "uid15": {
            "channel_id": "uid3",
            "sender_id": "owner",
            "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "timestamp": "2023-05-19T10:30:00Z"
        },
        "uid16": {
            "channel_id": "uid3",
            "sender_id": "owner",
            "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "timestamp": "2023-05-19T10:30:00Z"
        },
    },
}

export default dummy;