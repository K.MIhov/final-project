const FAQData = [
 { 
    id: 0,
    questions: 'How do I create an account?',
    answer : 'To create an account, click on the "Sign Up" button on the homepage and follow the on-screen instructions. You will need to provide a valid email address and choose a secure password.'
 },
 { 
    id: 0,
    questions: 'How can I update my profile picture?',
    answer : 'To update your profile picture, go to your account settings and find the "Profile" section. Click on the "Edit" button next to your current picture and follow the prompts to upload a new image.'
 },
 { 
    id: 0,
    questions: 'Can I change my username?',
    answer : 'Yes, you can change your username. Navigate to your account settings, locate the "Username" field, and click on the "Edit" button next to it. Enter your desired username and save the changes.'
 },
 { 
    id: 0,
    questions: 'How do I reset my password?',
    answer : 'If you forget your password, click on the "Forgot Password" link on the login page. Enter your email address associated with the account, and you will receive instructions on how to reset your password'
 },
 { 
    id: 0,
    questions: 'How can I delete my account?',
    answer : 'To delete your account, go to your account settings, find the "Delete Account" option, and follow the provided instructions. Please note that this action is irreversible and will permanently remove all your data.'
 },
 { 
    id: 1,
    questions: 'How do I create a new server?',
    answer : 'To create a new server, click on the "+" button next to the server list. Choose "Create a Server" and follow the prompts to set a server name, select a region, and customize settings.'
 },
 { 
    id: 1,
    questions: 'How can I invite people to my server?',
    answer : 'To invite people to your server, select the server from the server list, click on the dropdown arrow next to the server name, and choose "Invite People." You can generate an invitation link to share with others.'
 },
 { 
    id: 1,
    questions: 'How do I create a new channel within a server?',
    answer : 'To create a new channel within a server, right-click on the server name, select "Create Channel," and choose the desired channel type (text or voice). Enter the channel name and adjust other settings as needed.'
 },
 { 
    id: 1,
    questions: 'Can I set permissions for different server roles?',
    answer : 'Yes, you can set permissions for different server roles. Go to the server settings, navigate to the "Roles" section, and customize the permissions for each role by selecting or deselecting various options.'
 },
 { 
    id: 1,
    questions: 'How do I delete a channel in my server?',
    answer : 'To delete a channel, right-click on the channel name and select "Edit Channel." Scroll down and click on the "Delete Channel" button. Confirm your decision, and the channel will be permanently removed.'
 },
 { 
    id: 2,
    questions: 'How do I send a direct message to another user?',
    answer : 'To send a direct message, locate the user in your friends list or the server member list. Right-click their username and choose "Message" from the context menu. A private chat window will open.'
 },
 { 
    id: 2,
    questions: 'Can I format my text in messages?',
    answer : 'Yes, you can format your text in messages. Discord supports Markdown syntax, allowing you to apply formatting such as bold, italic, and more. Refer to the formatting guide in the app for a list of available options.'
 },
 { 
    id: 2,
    questions: 'How do I start a voice call with someone?',
    answer : 'To start a voice call with someone, open a direct message with that person or join a voice channel in a server they are part of. Click on the phone icon in the top right corner of the chat window or voice channel to initiate the call.'
 },
 { 
    id: 2,
    questions: 'Can I share my screen during a voice call?',
    answer : 'Yes, you can share your screen during a voice call. While in a call, look for the "Screen" button in the bottom left corner of the call window. Click on it and choose the screen you want to share from the available options.'
 },
 { 
    id: 2,
    questions: 'How can I mute or adjust volume for individual users?',
    answer : 'To mute or adjust the volume for an individual user in a voice call, hover over their username in the call window and click on the microphone or speaker icon that appears. You can choose to mute/unmute or adjust their volume separately.'
 }
]

export default FAQData;
