import { Grid, Container, Box, Text, UnorderedList, FormLabel, FormControl, Input, Textarea, Button, Image, Flex } from "@chakra-ui/react";
import { faEnvelope, faPhone, faMapLocationDot } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Footer from "../Footer/Footer";
import { useState } from "react";
import Header from "../../components/Header/Header";
import image from '../../assets/3d-business-man-studying-online-352x456.png';

const ContactUs = () => {

    const [formData, setFormData] = useState({
        firstName: '',
        lastName: '',
        email: '',
        message: ''
    })

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevData) => ({
            ...prevData,
            [name]: value
        }));
    }


    const handleSubmit = (e) => {
        e.preventDefault();
        const { firstName, lastName, email, message } = formData;
        // eslint-disable-next-line no-useless-escape
        const emailContent = `First Name: ${firstName}n\ Last Name: ${lastName}n\ Email:${email}n\ Message: ${message}`;
        window.open(`mailto:gamehub463@gmail.com?subject=Contact Form Submission&body=${encodeURIComponent(emailContent)}`);
    }

    return (
        <Box bg="#F8F8F8" minHeight="100vh">
        <Header/>
            
            <Container maxW="75%" mt="80px" mb="50px" textAlign="center">
                <Grid templateColumns="repeat(3, 1fr)" gap={10}>
                    
                </Grid>
            </Container>
            <Container maxW="75%" mt="80px" mb="50px" h="600px">
                <Grid templateColumns="repeat(2, 1fr)" gap={6}>
                    <Box h="400px">
                        <Text fontSize="50px" color="black" fontWeight="bold">
                            We would love to hear
                        </Text>
                        <Text fontSize="40px" color="green.300" fontWeight="bold">
                            from you
                        </Text>
                        <UnorderedList m="0px" fontSize="20px">
                            <Text mt="10px" color="black">
                                <FontAwesomeIcon icon={faEnvelope} size="sm" style={{ marginRight: '10px' }} />
                                gamehub463@gmail.com
                            </Text>
                            <Text mt="10px" color="black">
                                <FontAwesomeIcon icon={faPhone} size="sm" style={{ marginRight: '10px' }} />
                                088888888
                            </Text>
                            <Text mt="10px" color="black">
                                <FontAwesomeIcon icon={faMapLocationDot} size="sm" style={{ marginRight: '10px' }} />
                                Sofia, Studentski grad
                            </Text>
                        </UnorderedList>
                        <Flex mt="10px"> 
                        <Image
                        boxSize="400px"
                        objectFit="contain"
                        src={image}
                        alt="Contact us"
                        />
                        </Flex>
                    </Box>
                    <Box border="1px" borderColor="green.300" borderRadius="50px" p="40px" bg="green.300">
                        <form onSubmit={handleSubmit}>
                            <FormControl isRequired>
                                <FormLabel color="white">First name</FormLabel>
                                <Input
                                    name="firstName"
                                    value={formData.firstName}
                                    onChange={handleInputChange}
                                    placeholder="First name"
                                    backgroundColor="white"
                                    color="black"
                                    _placeholder={{
                                        color: "green.200"
                                    }}
                                />
                                <FormLabel mt="20px" color="white">Last name</FormLabel>
                                <Input
                                    name="lastName"
                                    value={formData.lastName}
                                    onChange={handleInputChange}
                                    placeholder="Last name"
                                    backgroundColor="white"
                                    color="black"
                                    _placeholder={{
                                        color: "green.200"
                                    }}
                                />
                                <FormLabel mt="20px" color="white">Email</FormLabel>
                                <Input
                                    name="email"
                                    value={formData.email}
                                    onChange={handleInputChange}
                                    placeholder="Email"
                                    backgroundColor="#F8F8F8"
                                    color="black"
                                    _placeholder={{
                                        color: "green.200"
                                    }}
                                />
                                <Textarea
                                    name="message"
                                    value={formData.message}
                                    onChange={handleInputChange}
                                    placeholder="Message"
                                    size="sm"
                                    mt="20px"
                                    backgroundColor="#F8F8F8"
                                    color="black"
                                    _placeholder={{
                                        color: "green.200"
                                    }}
                                />
                                <Button
                                    mt={4}
                                    colorScheme="teal"
                                    type="submit"
                                    bg="green.200"
                                    _hover={{
                                        bg: 'green.500',
                                    }}
                                >
                                    Submit
                                </Button>
                            </FormControl>
                        </form>
                    </Box>
                </Grid>
            </Container>
            <Footer />
        </Box>
    );

}

export default ContactUs;
