import { useRef, useState } from "react";
import { Box, Input, Button, Flex, Text, Center, IconButton, Tooltip} from "@chakra-ui/react";
import { ChevronUpIcon } from "@chakra-ui/icons";
import axios from "axios";
import ReactPlayer from "react-player";
import { YOUTUBE_API_KEY } from "../../common/constants";
import NavBar from '../../components/NavBar/NavBar';

const YouTube = () => {
    const [searchTerm, setSearchTerm] = useState("");
    const [videos, setVideos] = useState([]);
    const videoRef = useRef();

    const handleSearch = async () => {
        try {
            const response = await axios.get(
                `https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&maxResults=25&q=${searchTerm}&key=${YOUTUBE_API_KEY}`
            );
            console.log(response);
            const videoData = response.data.items;
            setVideos(videoData);
        } catch (error) {
            console.error(error);
        }
    };

    const handleInputChange = (event) => {
        setSearchTerm(event.target.value);
    };

    const scrollToBottom = () => {
        videoRef.current.scrollIntoView({ behavior: "smooth", block: "end" })
    }


    return (
        <>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
            <div style={{ flex: 1 }}>
                <NavBar />
            </div>
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    flex: 4,
                    marginTop: "2em",
                }}
            >
        <Box>
            <Flex align="center" mb={4}>
                <Input
                    type="text"
                    value={searchTerm}
                    onChange={handleInputChange}
                    placeholder="Enter a search term"
                    mr={4}
                    borderColor="green.300"
                />
                <Button colorScheme="green" onClick={handleSearch}>
                    Search
                </Button>
            </Flex>
            <Box>
                {videos.map((video, index) => (
                    <Center key={video.id.videoId} mb={4}>
                        <Box ref={index === 0 ? videoRef : null} width="100%">
                            <Center>
                                <ReactPlayer
                                    key={video.id.videoId}
                                    url={`https://www.youtube.com/watch?v=${video.id.videoId}`}
                                    width="100%"
                                    height="50vh"
                                    controls
                                    style={{ marginBottom: "1rem" }}
                                />
                            </Center>
                            <Center>
                                <Text fontWeight="bold" color="green.300">
                                    {video.snippet.title}
                                </Text>
                            </Center>
                            <Center>
                                <Text>By&nbsp;</Text>
                                <Text color="green.100">{video.snippet.channelTitle}</Text>
                            </Center>
                        </Box>
                    </Center>
                ))}
            </Box>
            <Tooltip label="Go back to the top" placement="right">
            <IconButton
                icon={<ChevronUpIcon />}
                aria-label="Scroll Down"
                variant="ghost"
                onClick={scrollToBottom}
                isDisabled={videos.length === 0}
                color="white"
                bg="green.400"
            />
            </Tooltip>
        </Box>
        </div>
        </div>
        </>
    );
};

export default YouTube;

