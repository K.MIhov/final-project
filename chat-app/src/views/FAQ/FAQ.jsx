import { Box, Button, Container, Flex, Grid, Image, List, ListItem } from "@chakra-ui/react";
import { useState } from "react";
import AccordionComponent from "../../components/Accordion/Accordion";
import Footer from "../Footer/Footer";
import Header from "../../components/Header/Header";

const FAQ = () => {
  const [active, setActive] = useState(null);

  const handleClick = (index) => {
    setActive(index === active ? null : index);
  };

  return (
    <>
      <Box bg={'gray.50'}>
        <Header />
        <Box w={'100%'} h={'650px'} bg={'gray.50'}>
        <Flex justify="center" align="center" my={"20px"}>
            <Image borderRadius={"10px 10px 10px 10px"} w="40%" src={'https://i.ibb.co/mhN8D8K/concentrated-young-caucasian-businessman.jpg'} alt="FAQ"/>
          </Flex>
        </Box>
        <Container maxW={"90%"}  mb={"10px"} textAlign={"center"} bg={'gray.50'}>
          <Grid templateColumns={"30% 70%"} gap={6} bg={'gray.50'}>
            <Box
            bg={'gray.50'}
              style={{
                border: '1px solid ',
                borderRadius: '25px',
                lineHeight: '2',
                padding: '50px',
                fontSize: '18px',
                maxHeight: '235px',
              }}
            >
              <List spacing={3}>
                <ListItem>
                  <Button
                    onClick={() => handleClick(0)}
                    w="50%"
                    bg="green.300"
                    _hover={{
                      bg: "green.500",
                    }}
                  >
                    Common problems
                  </Button>
                </ListItem>
                <ListItem>
                  <Button
                    onClick={() => handleClick(1)}
                    w="50%"
                    bg="green.300"
                    _hover={{
                      bg: "green.500",
                    }}
                  >
                    Cookies
                  </Button>
                </ListItem>
                <ListItem>
                  <Button
                    onClick={() => handleClick(2)}
                    w="50%"
                    bg="green.300"
                    _hover={{
                      bg: "green.500",
                    }}
                  >
                    Privacy policies
                  </Button>
                </ListItem>
              </List>
            </Box>

            <Box bg={'gray.50'}>
              {active === null ? <AccordionComponent activate={0} /> : <AccordionComponent activate={active} />}
            </Box>
          </Grid>
        </Container>
        <Footer />
      </Box>
    </>
  );
}

// eslint-disable-next-line react-refresh/only-export-components
export default FAQ;
