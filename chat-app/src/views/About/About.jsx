import {
    Avatar,
    Box,
    Button,
    Container,
    Divider,
    Flex,
    Heading,
    Text
} from '@chakra-ui/react';
import { FaLinkedin } from 'react-icons/fa';
import { ALEX_IMG, KRASI_IMG, PETKO_IMG } from '../../common/constants';
import Footer from '../Footer/Footer';
import Header from '../../components/Header/Header';

export const About = () => {

    return (
        <Box bg="gray.50">
            <Header />
            <Container p='6' rounded='md'>
                <br />
                <br />
                <Box>
                    <Box textAlign="center" pb="10px" color="gray.700">
                        <Heading>OUR STORY</Heading>
                    </Box>
                    <Divider orientation='horizontal' />
                    <Text style={{ marginLeft: '-250px', textAlign: 'center' }} pb="20px" pt="20px" w="1000px" color="gray.700">
                        Founded in 2023, Chatify emerged as a pioneering force in the world of communication apps, revolutionizing the way teams and communities connect and collaborate. With a strong focus on technology and seamless user experiences, Chatify aims to redefine the way people communicate and stay connected.
                        Headquartered in Bulgaria and Germany, Chatify operates on a global scale, serving users from various industries and sectors. Our goal is to create an intuitive and user-friendly interface that enables seamless real-time messaging, voice and video calls, file sharing, and team collaboration. By leveraging the latest advancements in technology, Chatify empowers individuals and organizations to connect, share ideas, and work together effectively, regardless of geographical boundaries.
                    </Text>
                    <Divider orientation='horizontal' />
                    <Box textAlign="center" pb="20px" pt="20px">
                        <Heading color="gray.700">GREAT TOPICS START WITH GREAT PEOPLE</Heading>
                    </Box>
                    <Divider orientation='horizontal' />
                    <Text style={{ marginLeft: '-250px', textAlign: 'center' }} pb="20px" pt="20px" w="1000px" color="gray.700">
                        At Chatify, our mission is to bring together a vibrant and engaged global community, united by a shared passion for whatever the heck you like. Our success is driven by our dedicated team of passionate and diverse individuals who are committed to delivering exceptional forum experiences centered around world-class franchises.
                    </Text>

                    <Divider orientation='horizontal' />
                    <Box textAlign="center" pb="10px" pt="20px" color="gray.700">
                        <Heading>OUR TEAM</Heading>
                    </Box>
                    <br />
                    <br />
                    <Flex justify="center" align="center">
                        <Flex direction="column" align="center" mx={4}>
                            <Avatar src={KRASI_IMG} name="Krasimir Mihov" bg="gray" style={{ width: '240px', height: '240px' }} />
                            <Button as="a" href='https://www.linkedin.com' target='_blank' rel='noopener noreferrer'>
                                <FaLinkedin color='black' size="32" style={{ margin: '5px'}}/>
                            </Button>
                            <Text mt={2} fontWeight="bold" color="gray.700">Krasimir Mihov</Text>
                            <Text fontSize="sm" color="gray.700">Co-Founder And Managing Partner</Text>
                        </Flex>
                        <Flex direction="column" align="center" mx={4}>
                            <Avatar src={ALEX_IMG} name="Aleksander Tsarigradski" bg="gray" style={{ width: '240px', height: '240px' }} />
                            <Button as="a" href='https://www.linkedin.com' target='_blank' rel='noopener noreferrer'>
                                <FaLinkedin color='black' size="32" style={{ margin: '5px'}}/>
                            </Button>
                            <Text mt={2} fontWeight="bold" color="gray.700">Aleksander Tsarigradski</Text>
                            <Text fontSize="sm" color="gray.700">Co-Founder And Managing Partner</Text>
                        </Flex>
                        <Flex direction="column" align="center" mx={4}>
                            <Avatar src={PETKO_IMG} name="Petko Simeonov" bg="gray" style={{ width: '240px', height: '240px' }} />
                            <Button as="a" href='https://www.linkedin.com/in/petkosimeonov/' target='_blank' rel='noopener noreferrer'>
                                <FaLinkedin color='black' size="32" style={{ margin: '5px'}}/>
                            </Button>
                            <Text mt={2} fontWeight="bold" color="gray.700">Petko Simeonov</Text>
                            <Text fontSize="sm" color="gray.700">Co-Founder And Managing Partner</Text>
                        </Flex>
                    </Flex>
                </Box>
            </Container>
            <Footer></Footer>
        </Box>
    );
}


export default About;