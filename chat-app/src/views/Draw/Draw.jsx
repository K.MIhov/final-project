import { useRef, useState } from "react";
import CanvasDraw from "react-canvas-draw";
import NavBar from '../../components/NavBar/NavBar';

const Draw = () => {
    const canvasRef = useRef();
    const [brushColor, setBrushColor] = useState("#000000");
    const [brushRadius, setBrushRadius] = useState(5);
    const [canvasWidth, setCanvasWidth] = useState(1080);
    const [canvasHeight, setCanvasHeight] = useState(720);

    const handleClear = () => {
        canvasRef.current.clear();
    };

    return (
        <> 
         <div style={{display: 'flex', flexDirection: 'row'}}>
        <div style={{flex: 1}}>
            <NavBar/>
        </div>
        <div
            style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                marginTop: "2em",
                flex: 4
            }}
        >
            <div
                style={{
                    width: `${canvasWidth}px`,
                    height: `${canvasHeight}px`,
                    marginBottom: "1em",
                    marginLeft:"auto",
                    marginRight:"auto",
                }}
            >
                <CanvasDraw
                    brushColor={brushColor}
                    brushRadius={brushRadius}
                    canvasWidth={canvasWidth}
                    canvasHeight={canvasHeight}
                    ref={canvasRef}
                />
            </div>


            <div style={{ display: "flex", alignItems: "center" }}>
                <label style={{ marginRight: "1em" }}>
                    Brush Color:
                    <input
                        type="color"
                        value={brushColor}
                        onChange={(e) => setBrushColor(e.target.value)}
                        style={{
                            backgroundColor: brushColor,
                            color: "white",
                            padding: "0.5em",
                            borderRadius: "3px",
                            border: "none",
                            marginLeft: "0.5em",
                            width: "70px",
                        }}
                    />
                </label>

                <label style={{ marginRight: "1em" }}>
                    Brush Radius:
                    <input
                        type="number"
                        value={brushRadius}
                        onChange={(e) =>
                            setBrushRadius(e.target.value > 0 ? Number(e.target.value) : 1)
                        }
                        style={{ marginLeft: "0.5em" }}
                    />
                </label>

                <label style={{ marginRight: "1em" }}>
                    Canvas Width:
                    <input
                        type="number"
                        value={canvasWidth}
                        onChange={(e) =>
                            setCanvasWidth(e.target.value > 0 ? Number(e.target.value) : 1)
                        }
                        style={{ marginLeft: "0.5em" }}
                    />
                </label>

                <label style={{ marginRight: "1em" }}>
                    Canvas Height:
                    <input
                        type="number"
                        value={canvasHeight}
                        onChange={(e) =>
                            setCanvasHeight(e.target.value > 0 ? Number(e.target.value) : 1)
                        }
                        style={{ marginLeft: "0.5em" }}
                    />
                </label>

                <button
                    style={{
                        marginLeft: "1em",
                        transition: "transform 0.3s",
                        background: "red",
                        color: "white",
                        fontSize: "1em",
                        padding: "0.5em 1em",
                        borderRadius: "3px",
                    }}
                    onClick={handleClear}
                    onMouseOver={(e) => (e.target.style.transform = "scale(1.05)")}
                    onMouseOut={(e) => (e.target.style.transform = "scale(1)")}
                >
                    Erase
                </button>
            </div>

            <style>{`
        input[type="number"] {
            background-color: rgb(58, 203, 74);
          color: white;
          padding: 0.3em;
          border-radius: 3px;
          border: none;
          width: 70px;
        }
      `}</style>
        </div>
        </div>
        </>
    );
};

export default Draw;
