/* eslint-disable react/no-unescaped-entities */
import { Box, Heading, Text, Center } from '@chakra-ui/react';

export default function PrivacyPolicy() {
  return (
    <>
      <Center bg="#F8F8F8">
        <Box maxW="600px" p={4} color="black">
          <Heading as="h2" size="lg" mb={4}>
            Privacy Policy
          </Heading>
          <br />
          <Text fontSize="md" mb={4}>
            Welcome to Chatify! By accessing and using this website, you agree to be bound by our Privacy Policy:
          </Text>
          <Text pl="10px" mb={2}>
            1. Information We Collect:
            We may collect personal information that you provide to us, such as your name, email address, and any other information you choose to provide when using the web chat application.
          </Text>
          <Text pl="10px" mb={2}>
            2. Use of Information:
            We use the collected information to provide and improve the web chat application, respond to user inquiries and support requests, personalize user experience, and send notifications about updates or changes.
          </Text>
          <Text pl="10px" mb={2}>
            3. Data Security:
            We take reasonable measures to protect the security of your personal information. However, please note that no method of transmission over the internet or electronic storage is completely secure, and we cannot guarantee absolute security.
          </Text>
          <Text pl="10px" mb={2}>
            4. Third-Party Services:
            We may use third-party services that collect, monitor, and analyze information to improve the functionality and performance of the web chat application. These third-party services have their own privacy policies governing the use of information.
          </Text>
          <Text pl="10px" mb={2}>
            5. Cookies:
            Our web chat application may use cookies to enhance user experience. You can disable cookies in your browser settings, but please note that some features of the application may not function properly.
          </Text>
          <Text pl="10px" mb={2}>
            6. Children's Privacy:
            Our web chat application is not intended for children under the age of 13. We do not knowingly collect personal information from children. If you believe we have inadvertently collected information from a child, please contact us immediately.
          </Text>
          <Text pl="10px" mb={2}>
            7. Changes to the Privacy Policy:
            We reserve the right to modify or update our Privacy Policy at any time. Any changes will be effective immediately upon posting the revised Privacy Policy. Please review the Privacy Policy periodically for changes.
          </Text>
          <Text pl="10px" mb={2}>
            If you have any questions or concerns about our Privacy Policy, please contact us at gamehub463@gmai.com.
          </Text>
          <Center height="100%">
            <Heading as="h4" size="md" mt={4} color="green.500">
              Thank you for using Chatify!
            </Heading>
          </Center>
        </Box>
      </Center>
    </>
  );
}
