import { Box, Flex, Text, Avatar } from "@chakra-ui/react";
import image1 from '../../assets/young-bearded-man-with-striped-shirt.jpg';
import image2 from '../../assets/lifestyle-people-emotions-casual-concept-confident-nice-smiling-asian-woman-cross-arms-chest-confident-ready-help-listening-coworkers-taking-part-conversation.jpg';

const Customers = () => {
    return (
        <Box py={16} bg="#121634">
            <Text fontSize="5xl" fontWeight="bold" textAlign="center" mb={8}>
                What Our Customers Say
            </Text>
            <Flex justifyContent="center">
                <Box maxW="700px" width="100%">
                    <Flex flexDirection="column" alignItems="center" mb={8}>
                        <Avatar
                            src={image1}
                            name="mike"
                            size="3xl"
                            mb={4}
                        />
                        <Text fontSize="3xl" fontWeight="bold" mt={4} color="whiteAlpha.1000">
                            Mike John from Google
                        </Text>
                        <Text fontSize="lg" textAlign="center" color="green.400">
                            "Chatify has completely transformed the way I communicate with others. The intuitive interface, seamless messaging, and reliable audio and video calls make it my go-to platform. It's truly the best communication experience I've ever had!"
                        </Text>
                    </Flex>
                    <Flex flexDirection="column" alignItems="center">
                        <Avatar
                            src={image2}
                            name="emily"
                            size="3xl"
                            mb={4}
                        />
                        <Text fontSize="3xl" fontWeight="bold" mt={4} color="whiteAlpha.1000">
                            Emily Dane from Apple
                        </Text>
                        <Text fontSize="lg" textAlign="center" color="green.400">
                        "I can't express enough how much I love using Chatify. It's the perfect blend of functionality and user-friendliness. Whether it's chatting with friends, organizing team collaborations, or hosting virtual meetings, Chatify excels in every aspect. It's hands down the best communication platform out there!"
                        </Text>
                    </Flex>
                </Box>
            </Flex>
        </Box>
    );
};

export default Customers;
