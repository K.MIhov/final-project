/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect } from "react";
import { Box, Button, Flex, IconButton, Text, Center, useColorModeValue } from "@chakra-ui/react";
import { FaPlay, FaPause, FaStepForward, FaStepBackward, FaVolumeUp, FaVolumeDown, FaVolumeMute } from "react-icons/fa";
import NavBar from '../../components/NavBar/NavBar';

const Music = () => {
    const [stations, setStations] = useState([]);
    const [currentStationIndex, setCurrentStationIndex] = useState(0);
    const [isPlaying, setIsPlaying] = useState(false);
    const [volume, setVolume] = useState(50);
    const [isMuted, setIsMuted] = useState(false);
    const [audio, setAudio] = useState(null);
    const [currentStation, setCurrentStation] = useState(null);
    const [lastClickedButton, setLastClickedButton] = useState(null);

    useEffect(() => {
        fetchStations();
    }, []);

    const fetchStations = async () => {
        try {
            const language = "bulgarian"; // language to search for
            const url = `https://de1.api.radio-browser.info/json/stations/bylanguage/${language}`
            const response = await fetch(url);
            console.log(response)
            const data = await response.json();
            console.log(data);
            setStations(data);
        } catch (error) {
            console.error(error);
        }
    };

    const playStation = (station) => {
        setIsPlaying(true);
        if (!audio || station.url_resolved !== audio.src) {
            if (audio) {
                audio.pause();  // Pause the current station if it exists
            }
            const newAudio = new Audio(station.url_resolved);  // Create a new audio object
            newAudio.play();  // Start playing the new station
            setAudio(newAudio);  // Store the new audio object in state
            setCurrentStation(station);
            setLastClickedButton("play");
        } else {
            audio.play();  // Continue playing the current station
        }
    };

    const pauseStation = () => {
        if (audio) {  // Check if there is a station to pause
            setIsPlaying(false);
            audio.pause();
            setLastClickedButton("pause")
        }
    };

    useEffect(() => {
        return () => {  // Clean up function when the component is unmounted
            if (audio) {
                audio.pause();
            }
        };
    }, [audio]);

    const skipForward = () => {
        if (currentStationIndex < stations.length - 1) {
            if (audio) {
                audio.pause();
            }
            setCurrentStationIndex(currentStationIndex + 1);
            const nextStation = stations[currentStationIndex + 1];
            playStation(nextStation);
            setLastClickedButton("skipForward")
        }
    };

    const skipBackward = () => {
        if (currentStationIndex > 0) {
            if (audio) {
                audio.pause();
            }
            setCurrentStationIndex(currentStationIndex - 1);
            const previousStation = stations[currentStationIndex - 1];
            playStation(previousStation);
            setLastClickedButton("skipBackward")
        }
    };

    const changeVolume = (amount) => {
        const newVolume = Math.max(0, Math.min(100, volume + amount));
        setVolume(newVolume);
        if (audio) {  // Check if there is a station to adjust the volume of
            audio.volume = newVolume / 100;  // The Audio object's volume is a value between 0 and 1
        }
        setLastClickedButton("changeVolume")
    };

    const toggleMute = () => {
        setIsMuted(!isMuted);
        if (audio) {  // Check if there is a station to mute or unmute
            audio.muted = !audio.muted;
        }
        setLastClickedButton("toggleMute")
    };

    // const currentStation = stations[currentStationIndex];
    console.log(currentStation);

    return (
        <>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ flex: 1 }}>
                    <NavBar />
                </div>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center",
                        flex: 4,
                        marginTop: "2em",
                    }}
                >
                    <Center>
                        <Box>
                            <Flex align="center" color={useColorModeValue("black", "white")} mb={4}>
                                <Button onClick={skipBackward} disabled={currentStationIndex === 0} color={lastClickedButton === "skipBackward" ? "yellow.300" : useColorModeValue("black", "white")}>
                                    <FaStepBackward />
                                </Button>
                                <IconButton
                                    icon={isPlaying ? <FaPause /> : <FaPlay />}
                                    onClick={isPlaying ? pauseStation : () => playStation(currentStation)}
                                    color={lastClickedButton === "pause" ? "yellow.300" : useColorModeValue("black", "white")}
                                />
                                <Button onClick={skipForward} disabled={currentStationIndex === stations.length - 1} color={lastClickedButton === "skipForward" ? "yellow.300" : useColorModeValue("black", "white")}>
                                    <FaStepForward />
                                </Button>
                                <IconButton
                                    icon={<FaVolumeDown />}
                                    onClick={() => changeVolume(-10)}
                                    disabled={isMuted}
                                    color={lastClickedButton === "changeVolume" ? "yellow.300" : useColorModeValue("black", "white")}
                                />
                                <IconButton
                                    icon={<FaVolumeMute />}
                                    onClick={toggleMute}
                                    disabled={isMuted || volume === 0}
                                    color={lastClickedButton === "toggleMute" ? "yellow.300" : useColorModeValue("black", "white")}
                                />
                                <IconButton
                                    icon={<FaVolumeUp />}
                                    onClick={() => changeVolume(10)}
                                    disabled={isMuted}
                                    color={lastClickedButton === "changeVolume" ? "yellow.300" : useColorModeValue("black", "white")}

                                />
                                <Text ml={2}>{`${volume}%`}</Text>
                            </Flex>
                            {stations
                                .map((station) => (
                                    <Box key={station.changeuuid}>
                                        <Text fontWeight={station === currentStation ? "bold" : "normal"} color={station === currentStation ? "green.500" : useColorModeValue("black", "white")} textAlign="center">
                                            {station.name}
                                        </Text>
                                    </Box>
                                ))}
                        </Box>
                    </Center>
                </div>
            </div>
        </>
    );
};

export default Music;
