/* eslint-disable react/no-unescaped-entities */
import { useState, useCallback, useEffect } from 'react'
import { formatDate } from '@fullcalendar/core'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction'
import { Box, Heading, Checkbox, UnorderedList, ListItem, Button, FormControl, FormLabel, Input, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, useDisclosure } from "@chakra-ui/react"
import { addEvent, deleteEvent } from '../../services/calendar.service';
import { getUserByHandle, getUserByUid } from '../../services/users.service'
import { auth } from '../../config/firebase';
import { db } from '../../config/firebase'
import { off, onChildAdded, ref, onChildRemoved } from 'firebase/database'

const Calendar = () => {
    const [weekendsVisible, setWeekendsVisible] = useState(true);
    const [currentEvents, setCurrentEvents] = useState([]);
    const [currentUserHandle, setCurrentUserHandle] = useState('');
    const { isOpen: isAddModalOpen, onOpen: onAddModalOpen, onClose: onAddModalClose } = useDisclosure();
    const { isOpen: isDeleteModalOpen, onOpen: onDeleteModalOpen, onClose: onDeleteModalClose } = useDisclosure();
    const [selectedDate, setSelectedDate] = useState(null);
    const [selectedEvent, setSelectedEvent] = useState(null);
    const [title, setTitle] = useState("");
    const [participantHandles, setParticipantHandles] = useState("");

    useEffect(() => {
        const fetchUserHandle = async () => {
            const uid = auth.currentUser?.uid;

            if (uid) {
                const snapshot = await getUserByUid(uid);

                if (snapshot.exists()) {
                    const user = snapshot.val();
                    const handle = Object.values(user)[0].handle;
                    setCurrentUserHandle(handle);
                }
            }
        }

        fetchUserHandle();
    })

    useEffect(() => {
        const eventsRef = ref(db, "calendar");

        const onEventAdded = onChildAdded(eventsRef, (snapshot) => {
            const eventId = snapshot.key;
            const newEvent = snapshot.val();

            if (newEvent.participants && newEvent.participants[currentUserHandle]) {
                setCurrentEvents((prevEvents) => [
                    ...prevEvents,
                    {
                        id: eventId,
                        title: newEvent.title,
                        start: new Date(newEvent.start),
                        end: new Date(newEvent.end),
                        allDay: newEvent.allDay,
                        backgroundColor: "lightgreen",
                    }
                ]);
            }
        });

        const onEventRemoved = onChildRemoved(eventsRef, (snapshot) => {
            const eventId = snapshot.key;

            setCurrentEvents((prevEvents) => {
                return prevEvents.filter((event) => event.id !== eventId);
            });
        });

        return () => {
            off(eventsRef, 'child_added', onEventAdded);
            off(eventsRef, 'child_removed', onEventRemoved);
        }
    }, [currentUserHandle]);





    const handleWeekendsToggle = useCallback(() => {
        setWeekendsVisible(!weekendsVisible);
    }, [weekendsVisible]);

    const handleDateSelect = (selectInfo) => {
        setSelectedDate(selectInfo);
        onAddModalOpen();
    };

    const handleAddEvent = async () => {
        let handles = participantHandles.split(',').map(handle => handle.trim());
        let validHandles = [];

        for (let handle of handles) {
            const snapshot = await getUserByHandle(handle);
            if (snapshot.exists()) {
                validHandles.push(handle);
            }
        }

        let calendarApi = selectedDate.view.calendar;
        calendarApi.unselect();

        if (title) {
            addEvent({
                title,
                start: selectedDate.startStr,
                end: selectedDate.endStr,
                allDay: selectedDate.allDay
            }, currentUserHandle, validHandles);
        }

        onAddModalClose();
    }

    const handleEventClick = (clickInfo) => {
        setSelectedEvent(clickInfo.event);
        onDeleteModalOpen();
    };

    const handleDeleteEvent = () => {
        deleteEvent(selectedEvent.id);
        onDeleteModalClose();
    }

    const renderSidebar = () => {
        return (
            <Box className='demo-app-sidebar' m="20px">
                <Box className='demo-app-sidebar-section'>
                    <Heading size="lg">Instructions</Heading>
                    <UnorderedList>
                        <ListItem>Select dates and you will be prompted to create a new event</ListItem>
                        <ListItem>Drag, drop, and resize events</ListItem>
                        <ListItem>Click an event to delete it</ListItem>
                    </UnorderedList>
                </Box>
                <Box className='demo-app-sidebar-section'>
                    <Checkbox isChecked={weekendsVisible} onChange={handleWeekendsToggle}>
                        toggle weekends
                    </Checkbox>
                </Box>
                <Box className='demo-app-sidebar-section'>
                    <Heading size="lg">All Events ({currentEvents.length})</Heading>
                    <UnorderedList>
                        {currentEvents.map(renderSidebarEvent)}
                    </UnorderedList>
                </Box>
            </Box>
        )
    }

    return (
        <Box className='demo-app' m="20px">
            {renderSidebar()}
            <Box className='demo-app-main'>
                <FullCalendar
                    plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                    headerToolbar={{
                        left: 'prev,next today',
                        center: 'title',
                        right: 'dayGridMonth,timeGridWeek,timeGridDay'
                    }}
                    initialView='dayGridMonth'
                    editable={true}
                    selectable={true}
                    selectMirror={true}
                    dayMaxEvents={true}
                    weekends={weekendsVisible}
                    events={currentEvents}
                    select={handleDateSelect}
                    eventContent={renderEventContent}
                    eventClick={handleEventClick}
                    slotLabelFormat={{ hour: "2-digit", minute: "2-digit", hour12: false }}
                    eventTimeFormat={{ hour: "2-digit", minute: "2-digit", hour12: false }}
                />
                <Modal isOpen={isAddModalOpen} onClose={onAddModalClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Add event</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody pb={6}>
                            <FormControl>
                                <FormLabel>Title</FormLabel>
                                <Input placeholder="Title" value={title} onChange={(e) => setTitle(e.target.value)} />
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>Participant handles</FormLabel>
                                <Input placeholder="Participant handles" value={participantHandles} onChange={(e) => setParticipantHandles(e.target.value)} />
                            </FormControl>
                        </ModalBody>

                        <ModalFooter>
                            <Button colorScheme="green" mr={3} onClick={handleAddEvent}>
                                Save
                            </Button>
                            <Button onClick={onAddModalClose}>Cancel</Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>

                <Modal isOpen={isDeleteModalOpen} onClose={onDeleteModalClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Delete event</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody pb={6}>
                            Are you sure you want to delete the event '{selectedEvent?.title}'?
                        </ModalBody>

                        <ModalFooter>
                            <Button colorScheme="red" mr={3} onClick={handleDeleteEvent}>
                                Delete
                            </Button>
                            <Button onClick={onDeleteModalClose}>Cancel</Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>
            </Box>
        </Box>
    )
}

function renderEventContent(eventInfo) {
    return (
        <>
            <b>{eventInfo.timeText}</b>
            <i>{eventInfo.event.title}</i>
        </>
    )
}

function renderSidebarEvent(event) {
    return (
        <ListItem key={event.id}>
            <b>{formatDate(event.start, { year: 'numeric', month: 'short', day: 'numeric' })}</b>
            <i>{event.title}</i>
        </ListItem>
    )
}

export default Calendar;
