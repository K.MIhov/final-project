import {
    Image,
    HStack
} from '@chakra-ui/react';
import logo from '../../assets/White-Chatting-Letter-Initial-Logo-14.png'

const Logo = () => {
    return (
        <>
            <HStack>
                <Image src={logo} boxSize={10} />
                <Image src={'https://i.ibb.co/n3c0bBY/White-Chatting-Letter-Initial-Logo-13.png'} boxSize={10} w="40%" h="40%" />
            </HStack>
        </>
    );
};
export default Logo;