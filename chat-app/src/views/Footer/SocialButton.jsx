
import {
    VisuallyHidden,
    chakra
} from '@chakra-ui/react';
import PropTypes from 'prop-types';

const SocialButton = ({
    children,
    label,
    href,
}) => {
    return (
        <chakra.button
            bg={'blackAlpha.100'}
            rounded={'full'}
            w={8}
            h={8}
            cursor={'pointer'}
            as={'a'}
            href={href}
            display={'inline-flex'}
            alignItems={'center'}
            justifyContent={'center'}
            transition={'background 0.3s ease'}
            _hover={{
                bg: 'blackAlpha.200',
            }}>
            <VisuallyHidden>{label}</VisuallyHidden>
            {children}
        </chakra.button>
    );
};
SocialButton.propTypes = {
    children: PropTypes.object.isRequired,
    label: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired
}
export default SocialButton