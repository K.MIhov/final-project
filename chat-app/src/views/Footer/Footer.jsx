import {
  Box,
  Container,
  Link,
  SimpleGrid,
  Stack,
  Text,
  Input,
  IconButton,
  useToast,
} from '@chakra-ui/react';
import { FaInstagram, FaTwitter, FaYoutube, FaGitlab } from 'react-icons/fa';
import { BiMailSend } from 'react-icons/bi';
import { useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import ListHeader from './ListHeader';
import Logo from './Logo';
import SocialButton from './SocialButton';
import {FAQ_PAGE, ABOUT_US_PAGE, CONTACT_US_PAGE, CAREERS_PAGE, TERMS_PAGE, PRIVACY_PAGE} from '../../common/constants.js'

export default function Footer() {
  const emailInputRef = useRef(null);
  const toast = useToast();
  const navigate = useNavigate()
  const handleEmailClick = () => {
    toast({
      title: "You will be notified for the latest changes in our website!",
      status: "success",
      duration: 5000,
      isClosable: true
    });
    if (emailInputRef.current) {
      emailInputRef.current.value = '';
    }
  }

  return (
    <Box
      bg={'gray.50'}
      color={'gray.700'}
      >
      <Container as={Stack} maxW={'6xl'} py={10}>
        <SimpleGrid
          templateColumns={{ sm: '1fr 1fr', md: '2fr 1fr 1fr 2fr' }}
          spacing={8}>
          <Stack spacing={6}>
            <Box>
              <Logo color={'gray.700'} />
            </Box>
            <Text fontSize={'sm'}>
              © 2022 Chakra Templates. All rights reserved
            </Text>
            <Stack direction={'row'} spacing={6}>
            <SocialButton  label={'Gitlab'} href={'https://gitlab.com/AlexTsari/final-project.git'}>
                <FaGitlab />
              </SocialButton>
              <SocialButton   label={'Twitter'} href={'https://twitter.com/'}>
                <FaTwitter />
              </SocialButton>
              <SocialButton label={'YouTube'} href={'https://www.youtube.com/'}>
                <FaYoutube />
              </SocialButton>
              <SocialButton label={'Instagram'} href={'https://www.instagram.com/'}>
                <FaInstagram />
              </SocialButton>
            </Stack>
          </Stack>
          <Stack align={'flex-start'}>
            <ListHeader>Company</ListHeader>
            <Link onClick={() => navigate(`${ABOUT_US_PAGE}`)}>About us</Link>
            <Link onClick={() => navigate(`${CONTACT_US_PAGE}`)}>Contact us</Link>
            <Link onClick={() => navigate(`${CAREERS_PAGE}`)}>Careers</Link>
          </Stack>
          <Stack align={'flex-start'}>
            <ListHeader>Support</ListHeader>
            <Link onClick={() => navigate(`${FAQ_PAGE}`)}>Help Center</Link>
            <Link onClick={() => navigate(`${TERMS_PAGE}`)}>Terms of Service</Link>
            <Link onClick={() => navigate(`${PRIVACY_PAGE}`)} >Privacy Policy</Link>
          </Stack>
          <Stack align={'flex-start'}>
            <ListHeader>Stay up to date by entering an email</ListHeader>
            <Stack direction={'row'}>
              <Input
              color={"gray.500"}
                ref={emailInputRef} // Assign the ref to the input element
                placeholder={'Your email address'}
                bg={'blackAlpha.100'}
                border={0}
                _focus={{
                  bg: 'whiteAlpha.300',
                }}
              />
              <IconButton
                bg={'green.400'}
                onClick={handleEmailClick}
                color={'white'}
                _hover={{
                  bg: 'green.600',
                }}
                aria-label="Subscribe"
                icon={<BiMailSend />}
              />
            </Stack>
          </Stack>
        </SimpleGrid>
      </Container>
    </Box>
  );
}