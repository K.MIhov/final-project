/* eslint-disable react/no-unescaped-entities */
import {
    Box,
    Text,
    Heading,
    Center
} from "@chakra-ui/react"

const TermsAndRules = () => {
    return (
        <>
   <Center bg="#F8F8F8">
        <Box maxW="600px" p={4} color="black">
          <Heading as="h2" size="lg" mb={4}>
            Terms of Service
          </Heading>
          <br />
          <Text fontSize="md" mb={4}>
            Please read these terms and conditions carefully before using our web chat application.
          </Text>
          <Text fontSize="md" mb={4}>
            1. Acceptance of Terms
            By accessing and using our web chat application, you agree to be bound by these Terms of Service. If you do not agree to these terms, please refrain from using the application.
          </Text>
          <Text fontSize="md" mb={4}>
            2. Use of the Application
            Our web chat application is intended for personal and non-commercial use. You agree not to use the application for any illegal or unauthorized purpose.
          </Text>
          <Text fontSize="md" mb={4}>
            3. User Conduct
            You are solely responsible for your conduct while using the web chat application. You agree not to harass, abuse, or harm others, and not to upload or transmit any inappropriate or offensive content.
          </Text>
          <Text fontSize="md" mb={4}>
            4. Privacy and Data Collection
            We respect your privacy and handle your personal information in accordance with our Privacy Policy. By using the web chat application, you consent to the collection and use of your information as described in the Privacy Policy.
          </Text>
          <Text fontSize="md" mb={4}>
            5. Intellectual Property
            All intellectual property rights in the web chat application belong to us. You are granted a limited, non-exclusive, non-transferable license to use the application for personal purposes.
          </Text>
          <Text fontSize="md" mb={4}>
            6. Disclaimer of Warranty
            The web chat application is provided on an "as is" basis, without any warranties or representations. We do not guarantee the accuracy, reliability, or availability of the application.
          </Text>
          <Text fontSize="md" mb={4}>
            7. Limitation of Liability
            We shall not be liable for any direct, indirect, incidental, special, or consequential damages arising out of or in connection with the use of the web chat application.
          </Text>
          <Text fontSize="md" mb={4}>
            8. Modifications to the Terms
            We reserve the right to modify these Terms of Service at any time. Updated terms will be effective immediately upon posting. Your continued use of the application after the changes constitutes your acceptance of the modified terms.
          </Text>
          <Text fontSize="md" mb={4}>
            9. Governing Law
            These Terms of Service are governed by and construed in accordance with the laws of Europe. Any dispute arising out of or in connection with these terms shall be subject to the exclusive jurisdiction of the courts of your country.
          </Text>
          <Text fontSize="md" mb={4}>
            If you have any questions or concerns about these Terms of Service, please contact us at gamehub463@gmail.com.
          </Text>
          <Center height="100%">
            <Heading as="h4" size="md" mt={4} color="green.500">
              Thank you for using Chatify!
            </Heading>
          </Center>
        </Box>
      </Center>
        </>
    );
};

export default TermsAndRules;
