/* eslint-disable react/no-unescaped-entities */
import {
    Avatar,
    Box,
    Container,
    Flex,
    Heading,
    Stack,
    Text
} from '@chakra-ui/react';
import image2 from '../../assets/lifestyle-people-emotions-casual-concept-confident-nice-smiling-asian-woman-cross-arms-chest-confident-ready-help-listening-coworkers-taking-part-conversation.jpg';
import image1 from '../../assets/young-bearded-man-with-striped-shirt.jpg';
import PropTypes from 'prop-types';

const Testimonial = ({ children }) => {
    return <Box>{children}</Box>;
};

const TestimonialContent = ({ children }) => {
    return (
        <Stack
            bg="#00B998"
            boxShadow={'lg'}
            p={8}
            rounded={'xl'}
            align={'center'}
            pos={'relative'}
            _after={{
                content: `""`,
                w: 0,
                h: 0,
                borderLeft: 'solid transparent',
                borderLeftWidth: 16,
                borderRight: 'solid transparent',
                borderRightWidth: 16,
                borderTop: 'solid',
                borderTopWidth: 16,
                borderTopColor: "green.200",
                pos: 'absolute',
                bottom: '-16px',
                left: '50%',
                transform: 'translateX(-50%)',
            }}>
            {children}
        </Stack>
    );
};

const TestimonialHeading = ({ children }) => {
    return (
        <Heading as={'h3'} fontSize={'xl'}>
            {children}
        </Heading>
    );
};

const TestimonialText = ({ children }) => {
    return (
        <Text
            textAlign={'center'}
            color="gray.50"
            fontSize={'sm'}>
            {children}
        </Text>
    );
};

const TestimonialAvatar = ({
    src,
    name,
    title,
}) => {
    return (
        <Flex align={'center'} mt={8} direction={'column'}>
            <Avatar src={src} alt={name} mb={2} />
            <Stack spacing={-1} align={'center'}>
                <Text fontWeight={600}>{name}</Text>
                <Text fontSize={'sm'} color="gray.800">
                    {title}
                </Text>
            </Stack>
        </Flex>
    );
};

export default function WithSpeechBubbles() {
    return (
        <Box bg={'gray.50'}
        >
            <Container maxW={'7xl'} py={16} as={Stack} spacing={12}>
                <Stack spacing={0} align={'center'}>
                    <Heading color="gray.700">Our Clients Speak</Heading>
                    <Text color="gray.700">We have been working with clients around the world</Text>
                </Stack>
                <Stack
                    direction={{ base: 'column', md: 'row' }}
                    spacing={{ base: 10, md: 4, lg: 10 }}>
                    <Testimonial>
                        <TestimonialContent >
                            <TestimonialHeading>Efficient Collaborating</TestimonialHeading>
                            <TestimonialText>
                                "Chatify has completely transformed the way I communicate with others. The intuitive interface, seamless messaging, and reliable audio and video calls make it my go-to platform. It's truly the best communication experience I've ever had!"
                            </TestimonialText>
                        </TestimonialContent>
                        <TestimonialAvatar
                            src={
                                image1
                            }
                            title={'Mike John from Google'}
                        />
                    </Testimonial>
                    <Testimonial>
                        <TestimonialContent>
                            <TestimonialHeading>Intuitive Design</TestimonialHeading>
                            <TestimonialText>
                                "Chatify's design is simply superb! It's sleek, intuitive, and visually pleasing. The perfect combination of aesthetics and functionality, making it the ultimate communication platform."
                            </TestimonialText>
                        </TestimonialContent>
                        <TestimonialAvatar
                            src={
                                image2
                            }
                            title={'Emily Dane from Apple'}
                        />
                    </Testimonial>
                    <Testimonial>
                        <TestimonialContent>
                            <TestimonialHeading>Mindblowing Service</TestimonialHeading>
                            <TestimonialText>
                                "I can't express enough how much I love using Chatify. It's the perfect blend of functionality and user-friendliness. Whether it's chatting with friends, organizing team collaborations, or hosting virtual meetings, Chatify excels in every aspect. It's hands down the best communication platform out there!"
                            </TestimonialText>
                        </TestimonialContent>
                        <TestimonialAvatar
                            src={
                                'https://images.unsplash.com/photo-1586297135537-94bc9ba060aa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=100&q=80'
                            }
                            title={' Jane Cooper, CEO at ABC Corporation'}
                        />
                    </Testimonial>
                </Stack>
            </Container>
        </Box>
    );
}

Testimonial.propTypes = {
    children: PropTypes.node.isRequired,
};

TestimonialContent.propTypes = {
    children: PropTypes.node.isRequired,
};

TestimonialHeading.propTypes = {
    children: PropTypes.node.isRequired,
};

TestimonialText.propTypes = {
    children: PropTypes.node.isRequired,
};

TestimonialAvatar.propTypes = {
    src: PropTypes.string,
    name: PropTypes.string,
    title: PropTypes.string,
};