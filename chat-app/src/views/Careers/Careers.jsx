/* eslint-disable react/no-unescaped-entities */
import { Box, Text, Flex, Image, Link } from "@chakra-ui/react";
import image1 from '../../assets/brooke-cagle--uHVRvDr7pg-unsplash.jpg';
import image2 from '../../assets/brooke-cagle-g1Kr4Ozfoac-unsplash.jpg';
import image3 from '../../assets/annie-spratt-QckxruozjRg-unsplash.jpg';
import image4 from '../../assets/jason-goodman-Oalh2MojUuk-unsplash.jpg';
import image5 from '../../assets/emma-dau-n_4iTY1KmDE-unsplash.jpg';
import image6 from '../../assets/mimi-thian--VHQ0cw2euA-unsplash.jpg';
import image7 from '../../assets/austin-distel-mpN7xjKQ_Ns-unsplash.jpg';
import AOS from 'aos';
import 'aos/dist/aos.css';
import { useEffect } from "react";
import Header from "../../components/Header/Header";

const Careers = () => {

    useEffect(() => {
        AOS.init({duration: 2000});
    }, []); 

  return (
    <Box>
      <Header/>
      <Flex
        bg="gray.50"
        justifyContent="center"
        alignItems="center"
        height="700px"
        
      >
        <Flex
          maxW="1200px"
          width="90%"
          flexWrap="wrap"
          justifyContent="center"
          alignItems="center"
          data-aos="fade-up"
          // bg={"gray.50"}
        >
          <Image
            src={image1}
            alt="Image 1"
            width={{ base : '90%', md: '40%'}}
            borderRadius='10px'
            m='10px'
          />
          <Image
            src={image2}
            alt="Image 2"
            width={{base: "90%", md: "30%"}}
            borderRadius="10px"
            m="10px"
          />
          <Image
            src={image3}
            alt="Image 3"
            width={{base: "90%", md: "20%"}}
            borderRadius="10px"
            m="10px"
          />
          <Image
            src={image4}
            alt="Image 4"
            width={{base: "90%", md: "40%"}}
            borderRadius="10px"
            m="10px"
          />
          <Image
            src={image5}
            alt="Image 5"
            width={{base: "90%", md: "50%"}}
            borderRadius="10px"
            m="10px"
          />
        </Flex>
      </Flex>

      <Box bg="gray.50" py="50px" textAlign="center" data-aos="fade-up">
        <Text fontSize="4xl" fontWeight="bold" mb="30px" color="gray.700" data-aos="fade-up" data-aos-offset="200">
          Come Build Belonging With Us
        </Text>
        <Flex
          maxW="1200px"
          width="90%"
          justifyContent="center"
          alignItems="center"
          flexWrap="wrap"
          data-aos= "fade-up"
        >
          <Box w={{ base: "100%", md: "50%" }} mb={{ base: "20px", md: "0" }} data-aos="fade-up">
            <Image src={image6} alt="Image 6" borderRadius="10px" />
          </Box>
          <Box
            w={{ base: "100%", md: "50%" }}
            textAlign={{ base: "center", md: "start" }}
            px={{ base: "20px", md: "50px" }}
            data-aos="fade-up"
          >
            <Text fontSize="xl" color="gray.700" data-aos="fade-up">
            At Chatify, our team mirrors the diverse and vibrant community of users we serve. Each team member brings their unique personality and experiences, creating a harmonious blend of vibrant characters. We are united by our shared passion: we deeply care about our colleagues, the work we put forth, and the future we are shaping. We are all about collaboration and respect - there's no room for negativity or dismissive behavior here.
            </Text>
          </Box>
        </Flex>

        <Flex
          maxW="1200px"
          width="90%"
          justifyContent="center"
          alignItems="center"
          flexWrap="wrap"
          mt="50px"
          direction={{ base: "column", md: "row-reverse"}}
          data-aos="fade-up"
        >
          <Box
            w={{ base: "100%", md: "50%" }}
            textAlign={{ base: "center", md: "end" }}
            px={{ base: "20px", md: "50px" }}
            data-aos="fade-up"
          >
            <Text fontSize="xl" color="gray.700" data-aos="fade-up">
            Think of a work environment where everyone feels at home. That's what we are striving to build at Chatify. We're not merely envisioning such a place, we are actualizing it. Our aim is to construct a diverse, welcoming, and inclusive setting that mirrors the multifaceted world we inhabit, engage, and contribute to. We strongly believe that our diversity sparks innovation, enhances our product, fosters a positive work atmosphere, and ultimately, cultivates a superior organization.
            </Text>
          </Box>
          <Box w={{ base: "100%", md: "50%" }} mb={{ base: "20px", md: "0" }} data-aos="fade-up">
            <Image src={image7} alt="Image 7" borderRadius="10px" />
          </Box>
        </Flex>

        <Text fontSize="xl" mt="50px" color="gray.700" >
          Want to find out more?{" "}
          <Link href="mailto:gamehub463@gmail.com" color="green.400" fontWeight="bold">
            Click here
          </Link>{" "}
          and shoot us an email!
        </Text>
      </Box>
    </Box>
  );
};

export default Careers;
