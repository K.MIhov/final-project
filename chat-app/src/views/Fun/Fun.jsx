import { Box, Flex, Grid, GridItem, Text } from "@chakra-ui/react";
import { BsMusicNoteBeamed, BsJoystick, BsYoutube, BsBrush } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import NavBar from "../../components/NavBar/NavBar";

const Fun = () => {
  const navigate = useNavigate();

  const handleIconClick = (path) => {
    navigate(path);
  };

  return (
    <Flex direction="row" minHeight="100vh">
      <Box flex="">
        <NavBar />
      </Box>
      <Box flex="1" mb="10%"display="flex" alignItems="center" justifyContent="center">
        <Grid templateColumns="repeat(2, 1fr)" gap={0} maxW="960px" p={4}>
          <GridItem>
            {/* Music Card */}
            <Box
              bg="#00B998"
              m="10px"

              color={"white"}
              p={4}
              borderRadius="md"
              boxShadow="md"
              display="flex"
              flexDirection="column"
              alignItems="center"
              justifyContent="center"
              height="100%"
              cursor="pointer"
              onClick={() => handleIconClick("/music")}
              _hover={{
                backgroundColor: "green.300",
                transform: "scale(1.05)",
              }}
            >
              <BsMusicNoteBeamed size={64} />
              <Text mt={2} fontWeight="bold">
                Listen to the hottest music
              </Text>
            </Box>

            {/* Chess Card */}
            <Box
              p={4}
              color={"white"}
              m="10px"

              bg="#00B998"
              borderRadius="md"
              boxShadow="md"
              display="flex"
              flexDirection="column"
              alignItems="center"
              justifyContent="center"
              height="100%"
              cursor="pointer"
              onClick={() => handleIconClick("/chess")}
              _hover={{
                backgroundColor: "green.300",
                transform: "scale(1.05)",
              }}
            >
              <BsJoystick size={64} />
              <Text mt={2} fontWeight="bold">
                Train your brain
              </Text>
            </Box>
          </GridItem>

          <GridItem>
            {/* YouTube Card */}
            <Box
              p={4}
              m="10px"

              color={"white"}
              bg="#00B998"
              borderRadius="md"
              boxShadow="md"
              display="flex"
              flexDirection="column"
              alignItems="center"
              justifyContent="center"
              height="100%"
              cursor="pointer"
              onClick={() => handleIconClick("/youtube")}
              _hover={{
                backgroundColor: "green.300",
                transform: "scale(1.05)",
              }}
            >
              <BsYoutube size={64} />
              <Text mt={2} fontWeight="bold">
                Watch your favorite creators while eating
              </Text>
            </Box>

            {/* Placeholder Card */}
            <Box
            m="10px"
              p={4}
              bg="#00B998"
              color={"white"}
              borderRadius="md"
              boxShadow="md"
              display="flex"
              flexDirection="column"
              alignItems="center"
              justifyContent="center"
              height="100%"
              cursor="pointer"
              onClick={() => handleIconClick("/draw")}
              _hover={{
                backgroundColor: "green.300",
                transform: "scale(1.05)",
              }}
            >
              <BsBrush size={64} />
              <Text mt={2} fontWeight="bold">
                Express yourself like Picasso
              </Text>
            </Box>
          </GridItem>
        </Grid>
      </Box>
    </Flex>
  );
};

export default Fun;




