/* eslint-disable react-hooks/rules-of-hooks */
import { useState, useEffect } from 'react';
import { Center, Box, Button, Text, Spinner, useToast, useColorModeValue } from '@chakra-ui/react';
import NavBar from '../../components/NavBar/NavBar';

const Quiz = () => {
    const [questions, setQuestions] = useState([]);
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
    const [score, setScore] = useState(0);
    const [answered, setAnswered] = useState(false);
    const [userAnswer, setUserAnswer] = useState(null);
    const [loading, setLoading] = useState(false);
    const [selectedAnswer, setSelectedAnswer] = useState(null);
    const [displayAnswers, setDisplayAnswers] = useState(false);
    const toast = useToast();

    let answerTimeout = null;


    useEffect(() => {
        fetchQuestions();
    }, []);

    const fetchQuestions = async () => {
        setLoading(true);
        try {
            const response = await fetch('https://opentdb.com/api.php?amount=50&type=multiple');
            const data = await response.json();
            setQuestions(data.results);
        } catch (error) {
            console.error(error);
        }
        setLoading(false);
    };

    const answerQuestion = (answer) => {
        setSelectedAnswer(answer);
        setAnswered(true);
        if (answerTimeout) {
            clearTimeout(answerTimeout);
        }
        answerTimeout = setTimeout(() => {
            setUserAnswer(answer);
            setDisplayAnswers(true);
            if (answer === questions[currentQuestionIndex].correct_answer) {
                setScore(score + 1);
                toast({
                    title: "Correct!",
                    status: "success",
                    duration: 1000,
                    isClosable: true,
                });
            } else {
                toast({
                    title: "Incorrect!",
                    status: "error",
                    duration: 1000,
                    isClosable: true,
                });
            }
        }, 3000);
    };

    const nextQuestion = () => {
        if (!answered) return;
        if (currentQuestionIndex < questions.length - 1) {
            setCurrentQuestionIndex(currentQuestionIndex + 1);
        }
        setAnswered(false);
        setUserAnswer(null);
        setSelectedAnswer(null);
        setDisplayAnswers(false);
    };


    const getColorScheme = (answer) => {
        if (displayAnswers) {
            if (answer === questions[currentQuestionIndex].correct_answer) {
                return 'green';
            } else if (answer === userAnswer) {
                return 'red';
            }
        } else if (answer === selectedAnswer) {
            return 'yellow';
        }
        return 'gray';
    };

    useEffect(() => {
        return () => {
            if (answerTimeout) {
                clearTimeout(answerTimeout);
            }
        };
    }, []);

    const getScoreMessage = () => {
        if (score > 30) {
            return "You are using ChatGPT, right?";
        } else if (score > 20) {
            return "Are you cheating?!";
        } else if (score > 10) {
            return "You're looking good!";
        } else if (score > 5) {
            return "Hmmm maybe you are not that bad.";
        } else {
            return "";
        }
    };

    const decodeHTML = text => {
        const textArea = document.createElement('textarea');
        textArea.innerHTML = text;
        return textArea.value;
    }

    return (
        <>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ flex: 1 }}>
                    <NavBar />
                </div>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center",
                        flex: 4,
                        marginTop: "2em",
                    }}
                >
                    <Center h="100vh">
                        <Box textAlign="center" color="white">
                            {loading ? (
                                <Spinner />
                            ) : questions.length > 0 && currentQuestionIndex < questions.length ? (
                                <Box>
                                    <Text mb={4} color={useColorModeValue("black", "white")} fontFamily="revert" fontWeight="extrabold">{decodeHTML(questions[currentQuestionIndex].question)}</Text>
                                    {[...questions[currentQuestionIndex].incorrect_answers, questions[currentQuestionIndex].correct_answer]
                                        .sort()
                                        .map((answer) => (
                                            <Button
                                            color={useColorModeValue("black", "white")}
                                                key={questions.question}
                                                onClick={() => !answered && answerQuestion(answer)}
                                                mr={2}
                                                mb={2}
                                                colorScheme={getColorScheme(answer)}
                                                isDisabled={answered}
                                            >
                                                {decodeHTML(answer)}
                                            </Button>
                                        ))}
                                    {answered && (
                                        <Button colorScheme="blue" onClick={nextQuestion}>
                                            Next question
                                        </Button>
                                    )}
                                </Box>
                            ) : (
                                <Text>This was the last question, come back again later.</Text>
                            )}
                            <Text mt={4} color="green.500" fontWeight="bold">Score: {score}</Text>
                            <Text mt={4} color="white">{getScoreMessage()}</Text>
                        </Box>
                    </Center>
                </div>
            </div>
        </>
    );

};

export default Quiz;
