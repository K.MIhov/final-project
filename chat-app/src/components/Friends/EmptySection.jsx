import { Box, Divider, Image, VStack, Heading } from "@chakra-ui/react";
import PropTypes from 'prop-types';

const EmptySection = ({ heading }) => {
    return (
        <>
            <Box mt={"2%"} display="flex" justifyContent={"center"}>
                <VStack>
                    <Heading size={"lg"} color="gray.600">
                        {heading}
                    </Heading>
                    <Divider mt="20px" orientation='horizontal' />
                </VStack>
            </Box>
            <Box display="flex" mt="10%" justifyContent={"center"}  >
                <Image w="30%" src="https://i.ibb.co/mHH5YjS/White-Chatting-Letter-Initial-Logo-7.png"></Image>
            </Box>
        </>
    )
};

EmptySection.propTypes = {
    heading: PropTypes.string.isRequired
}

export default EmptySection;