
import {
    Box,
    Divider,
    Heading,
    Image,
    VStack
} from "@chakra-ui/react";

export default function NoUsersOnline() {
    return (
        <>
            <Box mt={"2%"} display="flex" justifyContent={"center"}>
                <VStack>
                    <Heading size={"lg"} color="gray.600">
                        There are no online users in your friend list.
                    </Heading>
                    <Divider mt="20px" orientation='horizontal' />
                </VStack>
            </Box>
            <Box display="flex" mt="20" justifyContent={"center"} >
            <Image w="30%" src="https://i.ibb.co/mHH5YjS/White-Chatting-Letter-Initial-Logo-7.png"></Image>
            </Box>
        </>
    )
}

