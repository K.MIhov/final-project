/* eslint-disable react-hooks/rules-of-hooks */
import {
  MessageOutlined,
  MoreOutlined
} from "@ant-design/icons";
import {
  Avatar,
  AvatarBadge,
  Box,
  Button,
  Center,
  Divider,
  Flex,
  HStack,
  Heading,
  Input,
  InputGroup,
  InputRightElement,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
  Spacer,
  Text,
  VStack,
  useColorModeValue,
  useToast
} from "@chakra-ui/react";
import { MeetingProvider } from "@videosdk.live/react-sdk";
import {
  off,
  onValue,
  ref,
  remove
} from "firebase/database";
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from "react";
import {
  BsSearch
} from "react-icons/bs";
import { moreItems } from "../../common/constants";
import { db } from "../../config/firebase";
import AppContext from "../../context/AuthContext";
import { requestSingleUserCall } from "../../services/call.service";
import { addChannel } from "../../services/channels.service";
import { authToken, createMeeting } from "../../services/generateToken";
import { getUserByHandle, getUserFriendList } from "../../services/users.service";
import MeetingView from "../Meeting/MeetingView";
import UserProfilePreview from "../UserProfilePreview/UserProfilePreview";
import EmptySection from "./EmptySection";

export default function All({ handleSelectedChat, allSingleChannels }) {
  const [currentFriendList, setCurrentFriendList] = useState(null);
  const [meetingId, setMeetingId] = useState(null);
  const [user, setUser] = useState(null);
  const [filteredFriendList, setFilteredFriendList] = useState(null);
  const [showUserPreview, setUserPreview] = useState(null);

  const { userData, friendList } = useContext(AppContext);
  console.log(userData, friendList, AppContext);

  const toast = useToast();

  const handleCall = async (option, user) => {
    const id = await createMeeting({ token: authToken });
    requestSingleUserCall(userData.handle, user, id)
      .then(() => setMeetingId(id))
      .catch((err) => `Error ocurred while trying to reach the user: ${console.err(err)}`)
  }
  const onMeetingLeave = () => {

    setMeetingId(null);
  };
  const handleProfileView = (user) => {
    getUserByHandle(user)
      .then((snapshot) => setUserPreview(snapshot.val()))
      .catch((err) => console.err(`Error ocurred while getting the user data: ${err.message}`));
  }

  const handleRemoveFriend = (handle) => {
    const removeRequest = ref(
      db,
      `users/${userData.handle}/friendList/${handle}`
    );
    const removeFromOtherUserFrList = ref(
      db,
      `users/${handle}/friendList/${userData.handle}`
    );

    Promise.all([remove(removeRequest), remove(removeFromOtherUserFrList)])
      .then(() => toast({
        title: "User Successfully Removed",
        status: "success",
        duration: 5000,
        isClosable: true,
      }))
      .catch((err) => `Error ocurred while removing friend from friend list: ${console.err(err)}`)


  };
  const handleSearchChange = (event) => {
    const searchValue = event.target.value.toLowerCase();
    const filteredList = currentFriendList.filter((friend) =>
      friend.handle.toLowerCase().includes(searchValue)
    );
    setFilteredFriendList(filteredList);
  };
  useEffect(() => {
    if (currentFriendList) {
      setFilteredFriendList(currentFriendList);
    }
  }, [currentFriendList]);

  useEffect(() => {
    if (userData) {
      const friendsRef = ref(db, `users/${userData.handle}/friendList`);
      const onFriendsChange = onValue(friendsRef, (snapshot) => {
        if (snapshot.val() !== "none") {
          getUserFriendList(snapshot.val())
            .then((snapshot) => setCurrentFriendList(snapshot))
            .catch((err) => console.err(`Error ocurred while fetching friendList:${err.message}`))
        }
      });
      return () => {
        off(friendsRef, onFriendsChange);
      };
    }
  }, [userData, userData?.friendList]);

  const handleClosePreview = () => {
    setUserPreview(false)

  };
  const handleSingleChat = (e, user) => {
    // console.log('here');
    e.preventDefault();
    const team = {
      listOfParticipants: [user.uid, userData.uid],
    };
    // const checkIfTheChatExist = allSingleChannels.find(
    //   (channel) => channel.team.listOfParticipants[0] === user.uid
    //     || channel.team.listOfParticipants[0] === userData.uid
    // );
    const checkIfTheChatExist = allSingleChannels.find(
      (channel) => channel.team.listOfParticipants[0] === user.uid
        || channel.team.listOfParticipants[1] === user.uid
    );

    if (checkIfTheChatExist) {
      return handleSelectedChat(
        checkIfTheChatExist,
        Object.values(checkIfTheChatExist.chat)[0]
      );
    }

    return addChannel(team, userData.uid)
      .then((res) => {
        handleSelectedChat(res, Object.values(res.chat)[0])
      }
      )
      .catch((err) => console.error(err));
  };

  return (
    <>
      <Box>
        {currentFriendList && friendList !== 'none' ? (
          <Center>
            <Box w={{base: "80%",md:"50%"}} mt={"20px"}>
              <InputGroup>
                <Input
                  bg={useColorModeValue("gray.100", "gray.800")}
                  placeholder="Search"
                  onChange={handleSearchChange}
                />
                <InputRightElement color={useColorModeValue("gray.800", "gray.100")}>
                  <BsSearch />
                </InputRightElement>
              </InputGroup>
              <Heading display={"flex"} justifyContent={"center"} mt="10px" size={"md"} color={useColorModeValue("gray.700", "white")}>YOUR FRIEND LIST:</Heading>
              {filteredFriendList && filteredFriendList.map((request, index) => (
                <Box key={index} mt="20px" ml="10px" w="100%" textAlign={"center"}>
                  <HStack p="10px" bg="#00B998" borderRadius={"10px 10px 10px 10px"} >
                    {request.connections ? (
                      <Avatar
                      display={{ base: "none", sm: "block" }}
                        cursor={"pointer"}
                        onClick={() => handleProfileView(request.handle)}
                        _hover={{ border: '2px solid gray' }}
                        name={request.handle}
                        src={request.profileImageUrl}
                        bg="gray"
                        border="2px solid #49cce6"
                        style={{ width: "60px", height: "60px" }}
                        mr="15px"
                      ><AvatarBadge boxSize='1em' bg='green.500' /></Avatar>
                    ) : (
                      <Avatar
                      display={{ base: "none", sm: "block" }}
                        cursor={"pointer"}
                        onClick={() => handleProfileView(request.handle)}
                        _hover={{ border: '2px solid gray' }}
                        name={request.handle}
                        src={request.profileImageUrl}
                        bg="gray"
                        border="2px solid #49cce6"
                        style={{ width: "60px", height: "60px" }}
                        mr="15px"
                      />
                    )}
                    <VStack align={"flex-start"}>
                      <Text _hover={{ bg: 'gray.700' }} p="5px" borderRadius={"10px 10px 10px 10px"} cursor={"pointer"} onClick={() => handleProfileView(request.handle)} fontWeight={"bold"} mb="-10px" mt="5px">
                        {request.handle}
                      </Text >
                      {request.connections ? (
                        <Box pl="8px">
                          <Text>
                            Online
                          </Text>
                        </Box>
                      ) : (
                        <Box pl="8px">
                          <Text>
                            Offline
                          </Text>
                        </Box>
                      )}

                    </VStack>
                    <Spacer />
                    <Button fontSize={"20px"} bg="transparent" borderRadius={"50px 50px"} onClick={(e) => handleSingleChat(e, request)}>
                      {console.log(request)}
                      <MessageOutlined
                      />
                    </Button >

                    <Flex alignItems={'center'}>
                      <Menu >
                        <MenuButton>
                          <Box fontSize={"25px"} cursor="pointer" >

                            <MoreOutlined />
                          </Box>
                        </MenuButton>
                        <MenuList >
                          {moreItems.map((item) => (
                            <MenuItem key={item} color={useColorModeValue("gray.800", "white")} fontWeight="bold" onClick={() => handleCall(item, request.handle)}>{item}</MenuItem>
                          ))}
                          <MenuDivider />
                          <Button ml="5px" bg="red" onClick={() => handleRemoveFriend(request.handle)}>Remove Friend</Button>
                        </MenuList>
                      </Menu>
                    </Flex>
                  </HStack>
                  <Divider mt="20px" orientation='horizontal' />
                </Box>
              ))}
            </Box>
          </Center>
        ) : (
          <EmptySection heading={'Your friend list is empty'} />
        )}
        {authToken && meetingId && (
          <MeetingProvider
            config={{
              meetingId,
              micEnabled: true,
              webcamEnabled: true,
              name: userData.handle,
            }}
            token={authToken}
          >
            <MeetingView user={user} meetingId={meetingId} onMeetingLeave={onMeetingLeave} />
          </MeetingProvider>
        )}
        {showUserPreview && (
          <UserProfilePreview setProfilePreview={handleClosePreview} user={showUserPreview} location={'Friends'} />
        )}
        {/* {showSingleChat && singleChatUser && (
                <GroupChat isSingleChat={true} user={singleChatUser} />
            )} */}
        {/* {UserProfilePreview} */}
      </Box>
    </>
  )
}

All.propTypes = {
  handleSelectedChat: PropTypes.func.isRequired,
  allSingleChannels: PropTypes.any.isRequired,
}