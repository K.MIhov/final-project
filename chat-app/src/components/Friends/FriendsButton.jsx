import { HStack, Text, useColorModeValue } from "@chakra-ui/react";
import { faUserGroup } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ChevronRightIcon } from "@chakra-ui/icons";
const FriendsButton = () => {
    return (
        <HStack>
        <Text
            color={useColorModeValue('gray.800', 'white')}
            fontSize={'16px'}
            fontWeight={'bold'}
            p={'5px 10px'}
            w={'100%'}
            cursor={'pointer'}
            transition={'box-shadow 0.3s ease-out, transform 0.3s ease-out'}
            _hover={{
                backgroundColor: '#00B998',
                boxShadow: '0px 0px 20px rgba(0, 185, 152, 0.4)',
            }}
        >
            <FontAwesomeIcon
                icon={faUserGroup}
                size={'sm'}
                cursor={'pointer'}
                style={{ marginRight: '10px' }}
            />
            FRIENDS
            <ChevronRightIcon ml="10px" mb={"2px"}/>
        </Text>
        </HStack>
    );
};

export default FriendsButton;