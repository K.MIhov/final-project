import {
  Avatar,
  Box,
  Button,
  Divider,
  HStack,
  Heading,
  Image,
  Input,
  InputGroup,
  InputRightElement,
  Spacer,
  Text,
  VStack,
  Center,
  useColorModeValue
} from "@chakra-ui/react";
import { ref, update } from "firebase/database";
import { useContext, useState } from "react";
import { db } from "../../config/firebase";
import AppContext from "../../context/AuthContext";
import { getUserByHandle } from "../../services/users.service";
import UserProfilePreview from "../UserProfilePreview/UserProfilePreview";

export default function AddFriend() {
  const [searchResult, setSearchResult] = useState(null);
  const [searchValue, setSearchValue] = useState("");
  const [friendReqSent, setFriendReqSent] = useState(false);
  const { userData } = useContext(AppContext);
  const [showUserPreview, setUserPreview] = useState(null);

  async function searchByUsername(username) {
    const snapshot = await getUserByHandle(username);
    const userData = snapshot.val();
    console.log(userData);
    return snapshot.val();
  }
  const handleClosePreview = () => {
    setUserPreview(false);
  };
  const handleProfileView = (user) => {
    console.log(user);
    getUserByHandle(user).then((snapshot) => setUserPreview(snapshot.val()));
    // setUserPreview(true)
  };

  function handleSearchButtonClick() {
    if (searchValue) {
      searchByUsername(searchValue).then((result) => {
        if (result) {
          setSearchResult(result);
        } else {
          setSearchResult("No user found.");
        }
      });
    }
  }

  function handleSearchInputChange(event) {
    setSearchValue(event.target.value);
  }
  function handleFriendRequest() {
    const reference = ref(db, `users/${searchResult.handle}/requests`);
    // const notifications = ref(db, `users/${searchResult.handle}/unseenNotifications`);

    const user = {};
    user[userData.handle] = true;
    // console.log(user)
    // const notification = {};
    // notification[userData.handle] = {
    //     description: `${userData.handle} sent you a friend request.`,
    //     user: userData.handle
    // }
    // push(notifications, notification)
    // set(notifications, notification);
    update(reference, user)
      .then(() => console.log("success"))
      .catch((err) =>
        console.err(
          `Error ocurred while sending friend request: ${err.message}`
        )
      );
    setFriendReqSent(true);
  }

  return (
    <>
      <Box p="10px" display="flex" flexDirection="column" alignItems="center" mt={"20px"}>
        <Heading size="md" color={useColorModeValue("gray.700", "gray.100")}
        >ADD FRIEND</Heading>
        <Text color="gray">You can add friends by their Chatify username</Text>
        <br />
        <Box w="70%">
          <InputGroup>
            <Input
              value={searchValue}
              onChange={handleSearchInputChange}
              placeholder="You can add friends by their Chatify username"
              bg={useColorModeValue("gray.100", "gray.900")}
              focusBorderColor={"#00B998"}
              color={useColorModeValue("gray.700", "gray.100")}
            />
            <InputRightElement w="110px">
              <Button
                bg="#00B998"
                color="white"
                h="33px"
                mr="5px"
                onClick={handleSearchButtonClick}
              >
                Search user
              </Button>
            </InputRightElement>
          </InputGroup>
        </Box>
      </Box>
      <br />
      <Divider mb="20px" orientation="horizontal" />
      <Center>
        {searchResult && searchResult !== "No user found." ? (
          // <ListItem pt="20px" pl="5px">
          <Box m="0px 0px 0px 10px" w="50%">
            <HStack p="10px" bg="#00B998" borderRadius={"10px 10px 10px 10px"}>
              <Avatar
                cursor={"pointer"}
                onClick={() => handleProfileView(searchResult.handle)}
                _hover={{ border: "2px solid gray" }}
                name={searchResult.handle}
                src={searchResult.profileImageUrl}
                bg="gray"
                border="2px solid #49cce6"
                style={{ width: "60px", height: "60px" }}
                mr="15px"
              />
              <VStack align={"flex-start"}>
                <Text
                  _hover={{ bg: "gray.700" }}
                  p="0px 5px 0px 5px"
                  borderRadius={"10px 10px 10px 10px"}
                  cursor={"pointer"}
                  onClick={() => handleProfileView(searchResult.handle)}
                  fontWeight={"bold"}
                >
                  {searchResult.displayName}
                </Text>
                <Text
                  _hover={{ bg: "gray.700" }}
                  p="0px 5px 0px 5px"
                  borderRadius={"10px 10px 10px 10px"}
                  cursor={"pointer"}
                  onClick={() => handleProfileView(searchResult.handle)}
                  fontWeight={"bold"}
                >
                  {searchResult.handle}
                </Text>
              </VStack>
              <Spacer />
              {friendReqSent ? (
                <Button bg="gray.400" onClick={handleFriendRequest}>Sent</Button>
              ) : (
                <Button
                  onClick={handleFriendRequest}
                  boxShadow={"lg"}
                  bg="green.600"
                >
                  Send friend request
                </Button>
              )}
            </HStack>
            {/* <Divider mt="20px" orientation='horizontal' /> */}
          </Box>
        ) : (
          <Box ml="15px">
            <Text color={"gray.400"} fontWeight={"bold"}>
              {searchResult}
            </Text>
          </Box>
        )}
        <Box
          zIndex={-1}
          display="flex"
          mt="20"
          justifyContent={"center"}
          position={"absolute"}
          right="15%"
          top="30%"
        >
          <Image
            w="40%"
            src="https://i.ibb.co/mHH5YjS/White-Chatting-Letter-Initial-Logo-7.png"
          ></Image>

        </Box>
      </Center>
      {showUserPreview && (
        <UserProfilePreview
          setProfilePreview={handleClosePreview}
          user={showUserPreview}
          location={"Friends"}
        />
      )}
    </>
  );
}
