
import {
    Flex,
    useColorMode,
    Text,
    useColorModeValue,
    Grid,
    GridItem,
    Avatar,
    Center,
    Link,
    Menu,
    HStack,
    Box,
    MenuButton,
    Heading,
    Divider,
    Button,
    MenuItem,
    Spacer,
    Input,
    InputGroup,
    InputRightElement,
    ListItem,
    VStack,
    MenuList,
    MenuDivider,
    useToast,
    AvatarBadge,
    Image,
    MenuItemOption,
    MenuGroup,
    // Header, 
    // MenuItem,
    // Menu,
} from "@chakra-ui/react"
import { UsergroupAddOutlined, VideoCameraOutlined, InboxOutlined, MessageOutlined, MoreOutlined } from '@ant-design/icons';
import { useState, useContext, useEffect } from "react";
import AppContext from "../../context/AuthContext";
import { getUserByHandle } from "../../services/users.service";
import { update, ref, push, set, remove, get } from "firebase/database";
import { db } from "../../config/firebase";
import { MenuItems, moreItems } from "../../common/constants";
import Loader from "../Loader/Loader";
import NavBar from "../NavBar/NavBar";
import SidebarContacts from "../SidebarContacts/SidebarContacts";
import AddFriend from "./AddFriend";

export default function Blocked() { 
    return (
        <>
            <Box  mt={"2%"} display="flex" justifyContent={"center"}>
                <VStack>
                    <Heading size={"lg"} color="gray.600">
                        There are currently no blocked users.
                    </Heading>
                    <Divider mt="20px" orientation='horizontal' />
                </VStack>
            </Box>
            <Box display="flex" mt="10%" justifyContent={"center"} >
                <Image  w="40%" src="https://i.ibb.co/mHH5YjS/White-Chatting-Letter-Initial-Logo-7.png"></Image>
            </Box>
        </>
    )
}
