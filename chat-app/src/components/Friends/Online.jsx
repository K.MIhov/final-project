import { MessageOutlined, MoreOutlined } from '@ant-design/icons';
import {
    Avatar,
    AvatarBadge,
    Box,
    Button,
    Divider,
    Flex,
    HStack,
    Heading,
    Menu,
    MenuButton,
    MenuDivider,
    MenuItem,
    MenuList,
    Spacer,
    Text,
    VStack,
    useToast,
    Center,
    Input,
    InputRightElement,
    InputGroup,
    filter,
    useColorModeValue
} from "@chakra-ui/react";
import {
    BsSearch
} from "react-icons/bs";
import { ref, remove, update } from "firebase/database";
import { useContext, useEffect, useState } from "react";
import { moreItems } from "../../common/constants";
import { db } from "../../config/firebase";
import AppContext from "../../context/AuthContext";
import { getUserByHandle } from "../../services/users.service";
import UserProfilePreview from "../UserProfilePreview/UserProfilePreview";
import EmptySection from "./EmptySection";

export default function Online() {
    const [onlineFriends, setOnlineFriends] = useState([]);
    const [showUserPreview, setUserPreview] = useState(null);
    const [filteredFriendList, setFilteredFriendList] = useState(null);

    const toast = useToast();
    const { userData, friendList } = useContext(AppContext);

    const handleOption = (option, user) => {
        if (option === 'Start Video Call') {
            console.log(user);
            update(ref(db, `users/${user}/requestedCall`, { requestedCall: `${userData.handle}` }));
        }
    }
    const handleRemoveFriend = (handle) => {
        const removeRequest = ref(db, `users/${userData.handle}/friendList/${handle}`);
        const removeFromOtherUserFrList = ref(db, `users/${handle}/friendList/${userData.handle}`);
        Promise.all([remove(removeRequest), remove(removeFromOtherUserFrList)])
            .then(() => toast({
                title: "User Successfully Removed From Friend List",
                status: "success",
                duration: 5000,
                isClosable: true
            }))
            .catch((err) => console.err(`Error ocurred while removing user from friend list: ${err.message}`))


    }
    const handleProfileView = (user) => {
        console.log(user);
        getUserByHandle(user)
            .then((snapshot) => setUserPreview(snapshot.val()))
        // setUserPreview(true)
    }
    const handleClosePreview = () => {
        setUserPreview(false)

    };
    const handleSearchChange = (event) => {
        const searchValue = event.target.value.toLowerCase();
        const filteredList = onlineFriends.filter((friend) =>
            friend.handle.toLowerCase().includes(searchValue)
        );
        setFilteredFriendList(filteredList);
    };
    useEffect(() => {
        if (onlineFriends) {
            setFilteredFriendList(onlineFriends);
        }
    }, [onlineFriends]);

    useEffect(() => {
        if (userData && friendList && friendList !== 'none') {
            (async () => {
                const filteredFriendList = friendList.filter((userData) => userData && userData.connections);
                setOnlineFriends(filteredFriendList);
            })();
        }
    }, [userData.friendList]);

    return (
        <>
            <Box>
                {userData && onlineFriends.length > 0 ? (
                    <Center>
                        <Box w={{base: "80%",md:"50%"}} mt={"20px"}>
                            <InputGroup>
                                <Input
                                    bg={useColorModeValue("gray.100", "gray.800")}
                                    placeholder="Search"
                                    onChange={handleSearchChange}
                                />
                                <InputRightElement color={useColorModeValue("gray.800", "gray.100")}>
                                    <BsSearch />
                                </InputRightElement>
                            </InputGroup>
                            <Heading display={"flex"} mt="10px" justifyContent={"center"} size={"md"} color={useColorModeValue("gray.700", "white")}>ONLINE FRIENDS:</Heading>
                            {filteredFriendList && filteredFriendList.map((request, index) => (
                                <Box key={index} mt="20px" ml="10px" w="100%" textAlign={"center"}>
                                    <HStack p="10px" bg="#00B998" borderRadius={"10px 10px 10px 10px"}>
                                        {request.connections ? (
                                            <Avatar
                                                display={{ base: "none", sm: "block" }}
                                                cursor={"pointer"}
                                                onClick={() => handleProfileView(request.handle)}
                                                _hover={{ border: '2px solid gray' }}
                                                name={request.handle}
                                                src={request.profileImageUrl}
                                                bg="gray"
                                                border="2px solid #49cce6"
                                                style={{ width: "60px", height: "60px" }}
                                                mr="15px"
                                            ><AvatarBadge boxSize='1em' bg='green.500' /></Avatar>
                                        ) : (
                                            <Avatar
                                                display={{ base: "none", sm: "block" }}
                                                cursor={"pointer"}
                                                onClick={() => handleProfileView(request.handle)}
                                                _hover={{ border: '2px solid gray' }}
                                                name={request.handle}
                                                src={request.profileImageUrl}
                                                bg="gray"
                                                border="2px solid #49cce6"
                                                style={{ width: "60px", height: "60px" }}
                                                mr="15px"
                                            />
                                        )}

                                        <VStack
                                            align={"flex-start"}
                                        >
                                            <Text _hover={{ bg: 'gray.700' }} p="5px" borderRadius={"10px 10px 10px 10px"} cursor={"pointer"} onClick={() => handleProfileView(request.handle)} fontWeight={"bold"} mb="-10px" mt="5px">
                                                {request.handle}
                                            </Text >
                                            {request.connections ? (
                                                <Box pl="8px">
                                                    <Text>
                                                        Online
                                                    </Text>
                                                </Box>
                                            ) : (
                                                <Box pl="8px">
                                                    <Text>
                                                        Offline
                                                    </Text>
                                                </Box>
                                            )}

                                        </VStack>
                                        <Spacer />
                                        <Button fontSize={"20px"} bg="transparent" borderRadius={"50px 50px"} onClick={(e) => handleSingleChat(e, request)}>
                                            <MessageOutlined />
                                        </Button >

                                        <Flex alignItems={'center'}>
                                            <Menu >
                                                <MenuButton>
                                                    <Box fontSize={"25px"} cursor="pointer">
                                                        <MoreOutlined />
                                                    </Box>
                                                </MenuButton>
                                                <MenuList >
                                                    {moreItems.map((item) => (
                                                        <MenuItem key={item} color={useColorModeValue("gray.800", "white")} fontWeight="bold" onClick={() => handleCall(item, request.handle)}>{item}</MenuItem>
                                                    ))}
                                                    <MenuDivider />
                                                    <Button ml="5px" bg="red" onClick={() => handleRemoveFriend(request.handle)}>Remove Friend</Button>
                                                </MenuList>
                                            </Menu>
                                        </Flex>
                                    </HStack>
                                    <Divider mt="20px" orientation='horizontal' />
                                </Box>
                            ))}

                        </Box>
                    </Center>

                ) : (
                    <EmptySection heading={'There are no online users in your friend list.'} />
                )}

            </Box>
            {showUserPreview &&
                <UserProfilePreview setProfilePreview={handleClosePreview} user={showUserPreview} location={'Friends'} />
            }
        </>
    )
}
