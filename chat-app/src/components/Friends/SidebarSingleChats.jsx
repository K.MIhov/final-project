import { Avatar, Box, Container, Text, useColorModeValue } from "@chakra-ui/react";
import PropTypes from 'prop-types';
import { useContext } from "react";
import AppContext from "../../context/AuthContext";
import FriendsButton from "./FriendsButton";
const SidebarSingleChats = ({ handleChat, filterChannels, chats, users, handleFriends, switchDisplay, setSwitchDisplay }) => { //setDisplayAttribute, displayAttribute
  const { userData } = useContext(AppContext)
  const handleDisplayAttribute = () => {
    setSwitchDisplay('friends');
    handleFriends();
  }
  return (
    <>
      <Container
        display={switchDisplay === "DMs" ? { base: "block", xl: "block" } : { base: "none", xl: "block" }}
        maxW={"full"}
        h={"100vh"}
        bg={useColorModeValue('gray.100', "#1F232F")}
        p={"0px"}
        overflowY={"auto"}
        borderRight={useColorModeValue("1px solid #D9D9D9", "1px solid #292F3F")}
        sx={{
          "&::-webkit-scrollbar": {
            width: "0px",
          },
          "&::-webkit-scrollbar-thumb": {
            background: "#00B998",
            borderRadius: "10px",
          }
        }}
      >
        <Box
          borderBottom={"1px"}
          borderColor={useColorModeValue("gray.300", "#292F3F")}
        >
          <Text
            color={useColorModeValue('gray.800', "white")}
            fontSize={"16px"}
            fontWeight={"bold"}
            w={"100%"}
            p={"5px 10px"}
            mt={'2px'}
            borderRadius={"5px"}
          >
            DIRECT MESSAGES
          </Text>
          <Box onClick={handleFriends} display={{ base: 'none', xl: 'block' }}>
            <FriendsButton />
          </Box>
          <Box onClick={handleDisplayAttribute} display={{ base: 'block', xl: 'none' }}>
            <FriendsButton />
          </Box>
        </Box>
        {userData && users && chats && filterChannels.map((obj) => {
          console.log(obj.team.listOfParticipants[0])
          // const user = users.get(obj.team.listOfParticipants[0]);
          //TODO: да оправя логиката и тук
          const user = userData.uid === obj.owner ?
            users.get(obj.team.listOfParticipants[0]) : users.get(obj.owner);
          return (
            <Box
              key={obj.id}
              onClick={() => handleChat(obj, Object.values(obj.chat)[0])}
              cursor={"pointer"}
              display={"flex"}
              flexDirection={"row"}
              // bg="#00B998"
              m={"20px 10px"}
              p={"15px"}
              border={"1px"}
              borderColor={"#292F3F"}
              borderRadius={"10px"}
              transition={"box-shadow 0.3s ease-out, transform 0.3s ease-out"}
              _hover={{
                backgroundColor: '#00B998',
                boxShadow: "0px 0px 20px rgba(0, 185, 152, 0.4)",
                transform: "translateY(-2px)"
              }}
              alignItems={"center"}
            >
              <Avatar src={user.profileImageUrl} name={user.handle} size={"md"} />
              <Text
                fontSize={"16px"}
                ml={"10px"}
              >
                {user.handle}
              </Text>
            </Box>
          )
        })}
      </Container>
    </>
  );
}

SidebarSingleChats.propTypes = {
  handleChat: PropTypes.func.isRequired,
  filterChannels: PropTypes.arrayOf(PropTypes.object.isRequired),
  chats: PropTypes.arrayOf(PropTypes.object),
  users: PropTypes.any,
  handleFriends: PropTypes.func.isRequired

}

export default SidebarSingleChats;