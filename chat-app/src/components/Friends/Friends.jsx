import {
  Grid
} from "@chakra-ui/react";
import { off, onChildAdded, ref } from "firebase/database";
import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { FRIENDS_PAGE } from "../../common/constants";
import { db } from "../../config/firebase";
import AppContext from "../../context/AuthContext";
import { getAllUsers } from "../../services/users.service";
import Chat from "../Chat/Chat";
import Loader from "../Loader/Loader";
import NavBar from "../NavBar/NavBar";
import FriendsNavBar from "./FriendsNavBar";
import SidebarSingleChats from "./SidebarSingleChats";

const FriendsView = () => {
  const { userData } = useContext(AppContext);
  const [selectedChat, setSelectedChat] = useState(null);
  const [selectedChannel, setSelectedChannel] = useState(null);
  const [allSingleChannels, setAllSingleChannels] = useState(null);
  const [allChats, setAllChats] = useState(null);
  const [users, setUsers] = useState(null);
  const [isDrawerOpen, setIsDrawerOpen] = useState(true);
  const [allChannels, setAllChannels] = useState({});
  const [switchDisplay, setSwitchDisplay] = useState("DMs");
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    const channelRef = ref(db, "channels");
    let chatRef;
    let onChatAdded;

    const onChannelAdded = onChildAdded(channelRef, (snapshot) => {
      const channelId = snapshot.key;
      let newChannel = snapshot.val();
      newChannel = { id: channelId, ...newChannel };

      chatRef = ref(db, `channels/${channelId}/chat`);
      onChatAdded = onChildAdded(chatRef, (chatSnapshot) => {
        const chatId = chatSnapshot.key;
        const chat = chatSnapshot.val();
        newChannel = {
          ...newChannel,
          chat: {
            ...newChannel.chat,
            [chatId]: {
              ...chat,
              id: chatId,
            },
          },
        };

        setAllChannels((prevChannels) => ({
          ...prevChannels,
          [channelId]: newChannel,
        }));
      });
    });

    return () => {
      off(channelRef, "child_added", onChannelAdded);
      if (chatRef && onChatAdded) {
        off(chatRef, "child_added", onChatAdded);
      }
    };
  }, [userData?.uid]);

  useEffect(() => {
    if (userData) {
      getAllUsers()
        .then((snapshot) => {
          const allUsers = new Map();
          snapshot.forEach((doc) => {
            const userId = doc.uid ? doc.uid : "";
            if (userId) {
              allUsers.set(userId, doc);
            }
          });
          setUsers(allUsers);
        })
        .catch((err) => console.error(err));
    }
  }, [userData?.uid]);

  useEffect(() => {
    if (allChannels && userData) {
      const filterSingleChannels = Object.values(allChannels).filter((singleChannels) => {
        if (singleChannels.team.listOfParticipants.length === 2) {
          const check = new Set(singleChannels.team.listOfParticipants);
          if (check.has(userData.uid) || singleChannels.owner === userData.uid) {
            return singleChannels;
          }
        }
      });

      setAllSingleChannels(filterSingleChannels);

      const filterChats = filterSingleChannels
        .map((channel) => channel.chat)
      setAllChats(filterChats);
    }
  }, [allChannels, userData?.uid]);


  useEffect(() => {
    if (id && allSingleChannels) {
      const channel = allSingleChannels.find(channel => channel.id === id);
      if (channel && channel.chat) {
        setSelectedChannel(channel);
        setSelectedChat(Object.values(channel.chat)[0]);
      }
    }
  }, [id, allSingleChannels]);

  const handleSelectedChat = (channel, chat) => {
    setSelectedChannel(channel);
    setSelectedChat(chat);
    navigate(`/friends/${channel.id}`);
  };

  const handleSelectedChatFromSidebar = (channel, chat) => {
    setSelectedChannel(channel)
    setSelectedChat(chat)
    setSwitchDisplay("chat")
    navigate(`/friends/${channel.id}`)
  }

  const toggleDrawer = () => {
    setIsDrawerOpen(!isDrawerOpen);
  };

  const handleFriends = () => {
    setSelectedChat(null);
    setSelectedChannel(null);
    setSwitchDisplay('friends');
    navigate(`${FRIENDS_PAGE}`);
  };

  if (!userData) {
    return (
      <Loader></Loader>
    )
  }
  return (
    <Grid
      templateColumns={{ base: "1fr", xl: "7% 23% 70%", "2xl": "5% 25% 70%" }}
      gap={0}
    >
      <NavBar />
      {userData && allSingleChannels && (
        <SidebarSingleChats
          handleChat={handleSelectedChatFromSidebar}
          filterChannels={allSingleChannels}
          chats={allChats}
          users={users}
          handleFriends={handleFriends}
          switchDisplay={switchDisplay}
          setSwitchDisplay={setSwitchDisplay}
        />
      )}
      {userData && selectedChannel && users && selectedChat ? (
        <Chat
          toggle={toggleDrawer}
          selectedChat={selectedChat}
          selectedChannel={selectedChannel}
          users={users}
          switchDisplay={switchDisplay}
          setSwitchDisplay={setSwitchDisplay}
        />
      ) : (
        <FriendsNavBar
          handleSelectedChat={handleSelectedChat}
          allSingleChannels={allSingleChannels}
          switchDisplay={switchDisplay}
          setSwitchDisplay={setSwitchDisplay}
        />
      )}
    </Grid>
  );
};

export default FriendsView;