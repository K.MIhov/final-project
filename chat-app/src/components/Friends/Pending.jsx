
import {
    Avatar,
    Box,
    Button,
    Divider,
    HStack,
    Heading,
    Spacer,
    Text,
    VStack,
    Center,
    useColorModeValue
} from "@chakra-ui/react";
import { ref, remove, update } from "firebase/database";
import { useContext, useEffect, useState } from "react";
import { db } from "../../config/firebase";
import AppContext from "../../context/AuthContext";
import { getUserByHandle } from "../../services/users.service";
import UserProfilePreview from "../UserProfilePreview/UserProfilePreview";
import EmptySection from "./EmptySection";
import PropTypes from 'prop-types';

export default function Pending({ requests }) {
    const [pending, setPending] = useState(null);
    const [showUserPreview, setUserPreview] = useState(null);

    const { userData } = useContext(AppContext);

    const handleProfileView = (user) => {
        console.log(user);
        getUserByHandle(user)
            .then((snapshot) => setUserPreview(snapshot.val()))
        // setUserPreview(true)
    }
    const handleClosePreview = () => {
        setUserPreview(false)

    };

    const handleAcceptFriendRequest = (handle) => {
        const currentUserRef = ref(db, `users/${userData.handle}/friendList`);
        const otherUserRef = ref(db, `users/${handle}/friendList`);
        const requestRef = ref(db, `users/${userData.handle}/requests/${handle}`);
        Promise.all(
            [
                remove(requestRef),
                update(currentUserRef, { [handle]: true }),
                update(otherUserRef, { [userData.handle]: true })
            ])
            .catch((err) => `Problem ocurred while updating friendList ${err}`)

    };

    const handleDeclineFriendRequest = (handle) => {
        const removeRequest = ref(db, `users/${userData.handle}/requests/${handle}`);
        remove(removeRequest);

    }
    useEffect(() => {
        if (requests) {
            const fetchPendingRequests = async () => {
                const pendingRequests = await Promise.all(
                    Object.keys(requests).map((user) => {
                        return getUserByHandle(user).then((snapshot) => snapshot.val());
                    })
                );
                // console.log(pendingRequests);
                setPending(pendingRequests);
            };

            fetchPendingRequests();
        } else {
            setPending(null);
        }
    }, [requests]);

    return (
        <>
            {console.log(requests)}
            {console.log(pending)}
            {pending ? (
                <Center>
                    <Box w={"50%"} mt={"20px"}>
                        <Heading display={"flex"} mt="10px" justifyContent={"center"} size={"md"}color={useColorModeValue("gray.700", "gray.100")}>PENDING REQUESTS:</Heading>

                        {pending.map((request, index) => (
                                <Box key={index} mt="20px" ml="10px" w="100%" textAlign={"center"}>
                                <HStack p="15px" bg="#00B998" borderRadius={"10px 10px 10px 10px"}>
                                    <Avatar
                                        cursor={"pointer"}
                                        onClick={() => handleProfileView(request.handle)}
                                        _hover={{ border: '2px solid gray' }}
                                        name={request.handle}
                                        src={request.profileImageUrl}
                                        bg="gray"
                                        border="2px solid #49cce6"
                                        style={{ width: "60px", height: "60px" }}
                                        mr="15px"
                                    />
                                    <VStack align={"flex-start"}>
                                        <Text _hover={{ bg: 'gray.700' }} p="0px 5px 0px 5px" borderRadius={"10px 10px 10px 10px"} cursor={"pointer"} onClick={() => handleProfileView(request.handle)} fontWeight={"bold"} mb="-10px" >
                                            {request.displayName}
                                        </Text >
                                        <Text _hover={{ bg: 'gray.700' }} p="0px 5px 0px 5px" borderRadius={"10px 10px 10px 10px"} cursor={"pointer"} onClick={() => handleProfileView(request.handle)} fontWeight={"bold"} mb="-10px" >
                                            {request.handle}
                                        </Text >
                                    </VStack>
                                    <Spacer />
                                    <Box>
                                        <Button boxShadow={"lg"} onClick={() => handleAcceptFriendRequest(request.handle)} mr="5px" bg="green.600">
                                            Accept friend
                                        </Button>
                                        <Button boxShadow={"lg"} onClick={() => handleDeclineFriendRequest(request.handle)} bg="red.600">
                                            Decline
                                        </Button>
                                    </Box>
                                </HStack>
                                <Divider mt="20px" orientation='horizontal' />
                            </Box>
                        ))}
                    </Box>
                </Center>

            ) : (
                <EmptySection heading={'There are no pending friend requests.'} />
            )}
            {showUserPreview &&
                <UserProfilePreview setProfilePreview={handleClosePreview} user={showUserPreview} location={'Friends'} />
            }
        </>
    )
}

Pending.propTypes = {
    requests: PropTypes.any,
}
