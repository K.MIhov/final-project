
import { ChevronLeftIcon } from "@chakra-ui/icons";
import {
    Box,
    Button,
    Divider,
    Flex,
    HStack,
    Heading,
    Image,
    Menu,
    Spacer,
    Text,
    VStack,
    useColorModeValue
} from "@chakra-ui/react";
import { off, onValue, ref } from 'firebase/database';
import { useContext, useEffect, useState } from "react";
import { db } from '../../config/firebase';
import AppContext from "../../context/AuthContext";
import AddFriend from "./AddFriend";
import All from "./All";
import Blocked from "./Blocked";
import Online from "./Online";
import Pending from "./Pending";

export default function FriendsNavBar({ handleSelectedChat, allSingleChannels, switchDisplay, setSwitchDisplay }) {
    const [section, setSection] = useState(null);
    const [request, setRequest] = useState(null);
    const { userData } = useContext(AppContext);

    useEffect(() => {
        if (userData) {
            const requestRef = ref(db, `users/${userData.handle}/requests`);
            const onRequestsChange = onValue(requestRef, (snapshot) => {
                setRequest(snapshot.val());
            });

            return () => {
                off(requestRef, onRequestsChange);
            }
        }
    }, [userData, userData?.requests]);

    const handleSection = (sect) => {
        setSection(sect);
    }
    const handleDisplayAttribute = () => {
        setSwitchDisplay("DMs");
    }
    return (
        <>
            <Flex
                display={switchDisplay === 'friends' ? { base: "block", xl: "block" } : { base: "none", xl: "block" }}
                {...flexStyles}
                as="nav"
                flexDirection="column"
                h="100vh"
                bg={useColorModeValue('gray.50', "#292F3F")}
                alignItems="flex-start-right"
            >
                <Box
                    bg={'#00B998'}
                    px={4}
                    boxShadow="md"
                    color="white"
                >
                    <Flex h={"70px"}
                        alignItems={'center'}
                        overflowX="auto"
                        justifyContent={'space-between'}
                        {...flexStyles2}
                    >
                        <HStack>
                            <ChevronLeftIcon />
                            <Text
                                fontWeight={"bold"}
                                cursor={"pointer"}
                                onClick={handleDisplayAttribute}
                            >
                                DMs</Text>
                            <Box borderRight="1px solid white">
                                <Heading pl="20px" pr="30px" size={"md"} display={{ base: "none", md: "block" }}>FRIENDS</Heading>
                            </Box>
                        </HStack>
                        <HStack spacing={{ base: 2, sm: 5, md: 10 }} alignItems={'center'}>

                            <Menu>
                                <Button bg={'#34C7AD'} _focus={{ bg: "#00B998" }} onClick={() => handleSection('Online')} >Online</Button>
                                <Button bg={'#34C7AD'} _focus={{ bg: "#00B998" }} onClick={() => handleSection('All')}>All</Button>
                                <Button bg={'#34C7AD'} _focus={{ bg: "#00B998" }} position="relative" onClick={() => handleSection('Pending')}>
                                    {request && request !== 'none' && Object.values(request).length > 0 && (
                                        <Box
                                            {...boxStyles}
                                        >
                                            <Text color="white" fontSize="12px" fontWeight="bold">
                                                {Object.values(request).length}
                                            </Text>
                                        </Box>
                                    )}
                                    Pending
                                </Button>
                                {/* <Button _focus={{ bg: "#00B998" }} position="relative" onClick={() => handleSection('Blocked')}>Blocked</Button> */}
                                <Button bg={"green.300"} _focus={{ bg: "#00B998" }} onClick={() => handleSection('AddFriend')}  >Add Friend</Button>
                            </Menu>
                            <Spacer></Spacer>
                        </HStack>
                        <Box alignItems={"right"} cursor={"pointer"} onClick={() => handleSection('Inbox')} >
                            {/* <Inbox /> */}
                            {/* <InboxOutlined /> */}
                        </Box>
                    </Flex>
                </Box >
                <Box >
                    {section === 'All' && (
                        <All
                            handleSelectedChat={handleSelectedChat}
                            allSingleChannels={allSingleChannels}
                        />
                    )}
                    {section === 'AddFriend' && (
                        <AddFriend />
                    )}
                    {section === 'Pending' && (
                        <Pending requests={request} />
                    )}
                    {section === 'Online' && (
                        <Online />
                    )}
                    {section === 'Blocked' && (
                        <Blocked />
                    )}
                    {/* {section === 'Inbox' && (
                    <Inbox onClick  />
                )} */}
                    {!section && (
                        <>
                            <Box mt={"2%"} display="flex" justifyContent={"center"}>
                                <VStack>
                                    <Heading size={"lg"} color="gray.600">
                                        Hey There!
                                    </Heading>
                                    <Divider mt="20px" orientation='horizontal' />
                                </VStack>
                            </Box>
                            <Box display="flex" mt="10%" justifyContent={"center"} >
                                <Image w="30%" src={"https://i.ibb.co/mHH5YjS/White-Chatting-Letter-Initial-Logo-7.png"}></Image>
                            </Box>
                        </>
                    )}
                </Box>
            </Flex >
        </>
    )
}

// FriendsNavBar.propTypes = {
//     handleSelectedChat: PropTypes.func.isRequired,
//     allSingleChannels: PropTypes.any,
// }

const flexStyles = {
    maxW: "full",
    color: "white",
    maxH: "100vh",
    overflowY: "auto",
    bg: "#292F3F",
    p: "0px",
    sx: {
        "&::-webkit-scrollbar": {
            width: "5px",
        },
        "&::-webkit-scrollbar-thumb": {
            background: "gray.700",
            borderRadius: "10px",
        },
    },
};
const flexStyles2 = {
    sx: {
        "&::-webkit-scrollbar": {
            width: "1px",
            height: "7px"
        },
        "&::-webkit-scrollbar-thumb": {
            background: "gray.700",
            borderRadius: "10px",
        },
    },
};


const boxStyles = {
    position: "absolute",
    bottom: "-2px",
    right: "-2px",
    bg: "red",
    borderRadius: "full",
    width: "18px",
    height: "18px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
}