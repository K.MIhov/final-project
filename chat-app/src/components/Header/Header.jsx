import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { Flex, Link, Text, Button, Image } from '@chakra-ui/react';
import logo from '../../assets/White-Chatting-Letter-Initial-Logo-14.png';
import { ABOUT_US_PAGE, CAREERS_PAGE, FAQ_PAGE, HOME_PAGE, LOG_IN_PAGE } from '../../common/constants';

const Header = () => {
    const navigate = useNavigate();

    return (
        <Flex as="nav" p={0} bg={'gray.50'} alignItems="center" justifyContent="space-between">
            <Flex alignItems="center" m="5px">
                <Image src={logo} alt='Logo' w={"25%"} mr={2} onClick={() => navigate(`${HOME_PAGE}`)} cursor="pointer"/>
            <Link as={RouterLink} to={`${HOME_PAGE}`}onClick={() => navigate(`${HOME_PAGE}`)} color="white">
                <Text fontSize="2xl" fontWeight="bold" color="gray.700" fontFamily="Poppins, sans-serif">
                    Chatify
                </Text>
            </Link>
            </Flex>
            <Flex alignItems="center" justifyContent="center" flex={1}>
                <Link as={RouterLink} to={`${FAQ_PAGE}`} onClick={() => navigate(`${FAQ_PAGE}`)} fontSize="lg" fontWeight="bold" color="gray.700" mx={4} fontFamily="Poppins, sans-serif">
                    FAQ
                </Link>
                <Link as={RouterLink} to={`${ABOUT_US_PAGE}`} onClick={() => navigate(`${ABOUT_US_PAGE}`)} mx={4} fontSize="lg" fontWeight="bold" color="gray.700" fontFamily="Poppins, sans-serif">
                    About Us
                </Link>
                <Link as={RouterLink} to={`${CAREERS_PAGE}`} onClick={() => navigate(`${CAREERS_PAGE}`)} mx={4} fontSize="lg" fontWeight="bold" color="gray.700" fontFamily="Poppins, sans-serif">
                    Careers
                </Link>
            </Flex>
            <Button as={RouterLink} to={`${LOG_IN_PAGE}`} onClick={() => navigate(`${LOG_IN_PAGE}`)} colorScheme="white" variant="outline" mx={4} bgColor="#00B998" _hover={{ boxShadow: '0 0 10px rgba(0, 0, 0, 0.3)', transform: 'scale(1.05)' }} _active={{transform: 'scale(1)'}} transition = "all 0.3s" borderColor="#00B998" borderRadius="2xl">
                Log In
            </Button>
        </Flex>
    );
};

export default Header;
