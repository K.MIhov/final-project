import { Box } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Flex,
  Avatar,
  Stack,
  Heading,
  Collapse,
  Button,
  Text,
  useToast,
  AvatarBadge,
} from '@chakra-ui/react'
import { useState } from "react";
import { update, ref, push, set, remove, get } from "firebase/database";
import { db } from "../../config/firebase";
import { useContext, useEffect } from "react";
import AppContext from "../../context/AuthContext";
import PropTypes from 'prop-types';

// const buttonStyles = {}



const UserProfilePreview = ({ setProfilePreview, user, location }) => {
  const [show, setShow] = useState(false)
  const toast = useToast();
  const { userData } = useContext(AppContext);

  const handleToggle = () => setShow(!show)

  const handleCloseProfilePreview = () => {
    setProfilePreview(false);
  };

  function handleFriendRequest(user) {
    console.log(user);
    const reference = ref(db, `users/${user}/requests`);
    update(reference, { [userData.handle]: true })
      .then(() => {
        toast({
          title: "Friend Request Sent",
          status: "success",
          duration: 5000,
          isClosable: true,
          zIndex: 9999
        })
      })
      .catch((err) => `Error ocurred while sending friend request: ${err}`);

  }
  return (
    <Box
      position="fixed"
      top="0"
      left="0"
      right="0"
      bottom="0"
      zIndex="9999"
      bg="rgba(0, 0, 0, 0.1)"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Box
        maxW={'400px'}
        w={'full'}
        bg={'gray.800'}
        boxShadow={'2xl'}
        rounded={'md'}
        overflow={'hidden'}>
        <Box
          h={'120px'}
          w={'full'}
          bg="#00B998"
          objectFit={'cover'}
        />
        <Flex justify={'center'} mt={-12}>
          {user && (
            <Avatar
              size={'xl'}
              src={
                user.profileImageUrl
              }
              alt={'Author'}
              css={{
                border: '2px solid #00B998',
              }}
            >{user.connections ? <AvatarBadge boxSize='0.7em' bg='green.500' /> : null}</Avatar>
          )}
        </Flex>
        <Box p={6}>
          <Stack spacing={0} align={'center'} mb={5}>
            {user && (
              <Heading fontSize={'2xl'} fontWeight={500} fontFamily={'body'}>
                {user.displayName}
              </Heading>
            )}
            {user && (
              <Text color={'gray.500'}>{user.email}</Text>
            )}
            {user && user.connections ? (
              <Text color={'green.500'} fontWeight={"bold"}>Online</Text>
            ) : (
              <Text color={'gray.500'} fontWeight={"bold"}>Offline</Text>
            )}
          </Stack>
          <Stack>
            {user && user.about && (
              <>
                <Collapse startingHeight={30} in={show}>
                  {user.about}
                </Collapse>
                <Button size='sm' onClick={handleToggle} mt='1rem'>
                  Show {show ? 'Less' : 'More'}
                </Button>
              </>
            )}
          </Stack>
          <Stack direction={'row'} justify={'center'} spacing={6}>

          </Stack>
          {location !== 'Friends' && (
            <>
              {user.handle === userData.handle ? (
                <Button
                  bg="#00B998"
                  mt="4"
                  // onClick={() => handleFriendRequest(user.handle)}
                  {...buttonStyles}
                >
                  You
                </Button>
              ) : (
                <Button
                  bg="#00B998"
                  mt="4"
                  onClick={() => handleFriendRequest(user.handle)}
                  {...buttonStyles}
                >
                  Send Friend Request
                </Button>
              )}
            </>
          )}
          <Button
            {...buttonStyles}
            mt="5px"
            bg="red"
            onClick={handleCloseProfilePreview}
          >
            Close
          </Button>
        </Box>
      </Box >
    </Box>
  )
}

UserProfilePreview.propTypes = {
  setProfilePreview: PropTypes.func.isRequired,
  user: PropTypes.any.isRequired,
  location: PropTypes.string.isRequired
}

export default UserProfilePreview;






const buttonStyles = {
  w: 'full',
  color: 'white',
  rounded: 'md',
  _focus: {
    bg: 'gray',
  },
  _hover: {
    transform: 'translateY(-2px)',
    boxShadow: 'lg',
  },
};