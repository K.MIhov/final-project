import { Spinner } from "@chakra-ui/react";

const Loader = () => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "50vh",
        }}
      >
        <Spinner
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="#00B998"
          size="xl"
        />
      </div>
    );
  };
  
  export default Loader;