import { useState, useContext } from "react";
import { getUserDataByUserName } from "../../services/users.service";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { Input, InputRightElement, InputGroup, Button } from "@chakra-ui/react";
import AppContext from "../../context/AuthContext";


const Search = ({ handleAddUser }) => {
const [ query, setQuery ] = useState('')
const { userData } = useContext(AppContext);

const getUsers = (handle) => {
  getUserDataByUserName(handle)
  .then((snapshot) => {

    if (snapshot.exists()) {
      const key = Object.keys(snapshot.val())
      const user = snapshot.val()[key];

      if (userData.handle !== user.handle) {
        handleAddUser(user);
      } else {
        alert('You are already in the group')
        return
      }

    } else {
      alert(`This user does not exist: ${handle}`)
      return
    }

  })
  .catch(err => console.error(`The userDataByEmail in search: ${console.log(err)}`));
};

const addUser = (user) => {
    setQuery('');
    getUsers(user);
};

const onSearch = (e) => {
    e.preventDefault();
    setQuery(e.target.value);
};

// const handleKeyPress = (e, user) => {
//   if (e.key === "Enter") {
//     setQuery('');
//     getUsers(user);
//   }
// };
const handleKeyPress = (e, user) => {
  if (e.key === "Enter") {
    setQuery('');
    getUsers(user);
  }
};

return (
    <InputGroup
    mt={"20px"}
    >
      <Input
        placeholder='Search for a member'
        size="md"
        variant='outline'
        value={query}
        onChange={onSearch}
        focusBorderColor={"#00B998"}
        onKeyPress={(e) => handleKeyPress(e, query)}
      />
      <InputRightElement>
        <Button onClick={() => addUser(query)}>
          <FontAwesomeIcon icon={faPlus} size={'lg'} style={{ color: '#00B998', background:'rgba(0, 0, 0, 0)'}}/>
        </Button>
      </InputRightElement>
    </InputGroup>
  )
}

export default Search;