import { Box, Flex, Image, Text, Button, useBreakpointValue } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import backgroundImage from '../../assets/business-3d-young-women-standing-3182x3459.png';
import useRaindropAnimation from './useRaindropAnimation';
import './Landing.css'
import { useRef } from 'react';
import { HOME_PAGE } from '../../common/constants';

function Landing() {
  const navigate = useNavigate();
  const textRef1 = useRef(null);
  const textRef2 = useRef(null);
  const textRef3 = useRef(null);
  const shouldDisplayImage = useBreakpointValue({ base: false, lg: true});
  const fontSize1 = useBreakpointValue({ base: "4xl", md: "5xl" });
  const fontSize2 = useBreakpointValue({ base: "lg", md: "2xl" });
  const buttonPaddingX = useBreakpointValue({ base: 16, md: 32 });
  const buttonPaddingY = useBreakpointValue({ base: 4, md: 8 });


  useRaindropAnimation([
    { element: textRef1, content: "Discover a Place..." },
    { element: textRef2, content: "Where you can find your passion, connect with like-minded individuals, and share your creativity. A place where you and your closest friends can gather, collaborate, and build memories together. Join us in a space that makes communication effortless and socializing more enjoyable." },
    { element: textRef3, content: "Why are you still here!?" }
  ]);
  


  return (
    <Flex alignItems="stretch" justifyContent="space-between" p={0} minHeight="100vh" bg="gray.50">
      {shouldDisplayImage && (
        <Box w="50%" position="relative" borderRight="none" borderWidth={0}>
          <Box
            w="100%"
            h="100%"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <Image
              src={'https://i.ibb.co/88ffRX5/scrn3.jpg'}
              alt="Landing Image"
              // mb={4}
              position="static"
              maxW="100%"
              maxH="100%"
              objectFit="cover"
            />
          </Box>
        </Box>
      )}
      <Box w={shouldDisplayImage ? "50%" : "100%"} p={8} display="flex" alignItems="center" justifyContent="center" minHeight="100vh">
        <Box maxWidth="500px" textAlign="center">
          <Text ref={textRef1} color="gray.700" fontSize={fontSize1} mb={10} fontWeight="bold" textAlign="center" />
          <Text ref={textRef2} color="gray.700" fontSize={fontSize2} mb={10} />
          <Text ref={textRef3} color="gray.700" fontSize={fontSize2} mb={20} fontWeight="bold" textAlign="center" />
          <Box display="flex" justifyContent="center">
            <Button
              colorScheme="green"
              size="lg"
              px={buttonPaddingX}
              py={buttonPaddingY}
              borderRadius="9999px"
              fontWeight="bold"
              textTransform="uppercase"
              _hover={{
                transform: 'rotateZ(2deg) scale(1.25)' ,
                transition: "transform 0.3s ease-in-out"
              }}
              onClick={() => navigate(`${HOME_PAGE}`)}
            >
              Join Chatify Now
            </Button>
          </Box>
        </Box>
      </Box>
    </Flex>
  );
}


export default Landing;