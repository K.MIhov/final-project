import { useEffect } from 'react';

const useRaindropAnimation = (elements) => {
    useEffect(() => {
        elements.forEach(({ element, content }) => {
            const rect = element.current.getBoundingClientRect();
            const isVisible = rect.top <= window.innerHeight && rect.bottom >= 0;

            if (isVisible && !element.current.textContent) {
                content.split(' ').forEach((word, i) => {
                    const span = document.createElement("span");
                    // Include a non-breaking space after each word
                    span.innerText = `${word}\u00A0`;
                    span.style.animationDelay = `${i * 0.05}s`;
                    span.classList.add('raindrop-animation');
                    element.current.appendChild(span);
                });
            }
        });
    }, [elements]);
};

export default useRaindropAnimation;





