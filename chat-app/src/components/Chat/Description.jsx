import {
  Box,
  Button,
  Input,
  InputGroup,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import PropTypes from "prop-types";

const Description = ({
  updateDescription,
  value,
  onChangeDescription,
  selectedChannel,
}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Box>
      <Button
        onClick={onOpen}
        w={"88%"}
        bg={"#292F3F"}
        _hover={{ bg: "#00B998" }}
        m={"5px 20px"}
      >
        Update Description
      </Button>

      <Modal isOpen={isOpen} onClose={onClose} size={"xl"}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader bg={"#00B998"}>Update Description</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <InputGroup>
              <Input
                placeholder="Updated description..."
                size="md"
                variant="outline"
                value={value}
                onChange={onChangeDescription}
                focusBorderColor={"#00B998"}
                m={"20px 0px"}
              />
            </InputGroup>
            <Box
              display={"flex"}
              alignItems={"center"}
              justifyContent={"center"}
            >
              <Button
                type="submit"
                onClick={(e) => updateDescription(e, selectedChannel)}
                mb={"15px"}
                p={"10px 25px"}
                bg={"gray.700"}
                border={"1px"}
                borderColor={"#00B998"}
                _hover={{
                  bg: "#00B998",
                  boxShadow: "0 10px 20px rgba(0, 185, 152, 0.4)",
                  transform: "translateY(-2px)",
                }}
              >
                Update
              </Button>
            </Box>
          </ModalBody>
        </ModalContent>
      </Modal>
    </Box>
  );
};

Description.propTypes = {
  updateDescription: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  onChangeDescription: PropTypes.func.isRequired,
  selectedChannel: PropTypes.object.isRequired,
};

export default Description;
