import { Container, Grid, GridItem, useColorModeValue } from "@chakra-ui/react";
import ChatBar from "./ChatBar";
import Messages from "./Messages";
import InputMessages from "./InputMessage";
import { MeetingProvider } from "@videosdk.live/react-sdk";
import MeetingView from "../Meeting/MeetingView";
import { authToken } from "../../services/generateToken";
import { useState, useContext } from "react";
import { createMeeting } from "../../services/generateToken";
import {
  requestGroupCall,
  requestSingleUserCall,
} from "../../services/call.service";
import AppContext from "../../context/AuthContext";
import PropTypes from "prop-types";

const Chat = ({ toggle, selectedChat, selectedChannel, users, switchDisplay, setSwitchDisplay, toggleTeamInfo }) => {
  const [meetingId, setMeetingId] = useState(null);
  const { userData } = useContext(AppContext);

  // console.log(toggleTeamInfo);
  // console.log(toggleSidebar);

  const handleCall = async (participants) => {
    const id = await createMeeting({ token: authToken });

    if (participants?.listOfParticipants?.length > 1) {
      requestGroupCall(participants, userData, id)
        .then(() => setMeetingId(id))
        .catch((err) =>
          console.err(
            `Error ocurred while creating a group call: ${err.message}`
          )
        );
    } else {
      const string = participants.join("");
      requestSingleUserCall(userData.handle, string, id)
        .then(() => setMeetingId(id))
        .catch((err) =>
          console.err(
            `Error ocurred while trying to reach the user: ${err.message}`
          )
        );
    }
  };
  const onMeetingLeave = () => {
    setMeetingId(null);
  };

  return (
    <>
    {console.log(switchDisplay)}
      <Container display={switchDisplay === "chat" ? {base: "block", xl: "block"} : {base: "none", xl: "block"}}   maxW={"full"} bg={"#121634"} p={"0px"}>
        <Grid templateRows={"90% 10%"} h={"100vh"} padding={0}>
          <GridItem
            bg={useColorModeValue("gray.50", "#292F3F")}
            sx={{
              "&::-webkit-scrollbar": {
                width: "0px",
              },
              overflowY: "scroll",
              overflowX: "hidden",
            }}
          >
            <ChatBar handleCall={handleCall} toggle={toggle} chat={selectedChat} channel={selectedChannel} users={users} switchDisplay={switchDisplay} setSwitchDisplay={setSwitchDisplay} toggleTeamInfo={toggleTeamInfo}/>
            {selectedChannel && <Messages idChat={selectedChat?.id} channel={selectedChannel} users={users} />}
          </GridItem>
          <GridItem bg={useColorModeValue("gray.50", "#292F3F")}>
            <InputMessages
              chat={selectedChat}
              channel={selectedChannel}
              users={users}
            />
          </GridItem>
        </Grid>
      </Container>
      {authToken && meetingId && (
        <MeetingProvider
          config={{
            meetingId,
            micEnabled: true,
            webcamEnabled: true,
            name: userData.handle,
          }}
          token={authToken}
        >
          <MeetingView meetingId={meetingId} onMeetingLeave={onMeetingLeave} />
        </MeetingProvider>
      )}
    </>
  );
};

Chat.propTypes = {
  toggle: PropTypes.func.isRequired,
  selectedChat: PropTypes.object,
  selectedChannel: PropTypes.object,
  users: PropTypes.object,
};

export default Chat;
