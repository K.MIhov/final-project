import { Text, HStack, Grid, GridItem, Avatar, Center, Box, useColorModeValue, useBreakpointValue, Flex } from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhoneVolume, faInfo } from "@fortawesome/free-solid-svg-icons";
import { useContext } from "react";
import AppContext from "../../context/AuthContext";
import PropTypes from 'prop-types';
import { ChevronLeftIcon } from "@chakra-ui/icons";
import { useNavigate } from "react-router-dom";
const ChatBar = ({chat, channel, users, handleCall, setSwitchDisplay, toggleTeamInfo }) => {
  // console.log(toggleSidebar)
  console.log(toggleTeamInfo)
  const { userData } = useContext(AppContext)
  const navigate = useNavigate()
  const user = channel.team.listOfParticipants[0] === userData.uid ?
    users.get(channel.team.listOfParticipants[1]) :
    users.get(channel.team.listOfParticipants[0]);

  const isLargeScreen = useBreakpointValue({ base: false, md: false, lg: false, xl: true, '2xl': true });
  const breakpoint = useBreakpointValue({ base: "base", sm:"sm", md: "md", lg: "lg", xl:'xl', '2xl':'2xl' });

  const handleSwitchDisplay = () => {
    setSwitchDisplay('DMs');
    return navigate(-1)
  }

  // const gridTemplateColumns = useBreakpointValue({
  //   base: "1fr",
  //   lg: "1fr",
  //   xl: "3% 7% 80% 5% 5%",
  //   "2xl": "7% 21.5% 50% 21.5%",
  // });
  return (
    <Box
      bg={useColorModeValue("rgba(0, 0, 0, 0.2)", "rgba(31, 35, 47, 0.7)")}
      backdropFilter={"blur(2px)"}
      transition="background 0.3s ease-in-out, backdrop-filter 0.3s ease-in-out"
      borderLeft={"1px"}
      borderRight={"1px"}
      borderColor={"#292F3F"}
      position="sticky"
      top={0}
      zIndex={1}
    >
      <Flex align="center" justify="space-between" p="10px">
        <HStack>
          {!["xl", "2xl"].includes(breakpoint) && (
            <HStack mr="0px"  mt={{ base: "0px", md: "0px" }} display={{ base: "-webkit-inline-flex", xl: "none" }}>
              <ChevronLeftIcon />
              <Text fontWeight="bold" fontSize="15px" cursor="pointer" onClick={handleSwitchDisplay}>
                DMs
              </Text>
            </HStack>
          )}
          {user && channel.team.listOfParticipants.length === 2 ? (
            <Avatar display={{ base: "block", sm:"block", md: "block" }} name={user.handle} src={user.profileImageUrl} size="md" mr="10px" />
          ) : (
            <Avatar name={channel?.team?.teamName} src={channel?.team?.teamPhoto} display={{ base: "block", sm:"block", md: "block" }} size="md" mr="10px" />
          )}
          <Box>
            {chat && channel.team.listOfParticipants.length > 2 ? (
              <>
                <Text fontWeight="bold">{chat.name}</Text>
                <Text fontSize="sm" color="gray.500">
                  {channel.team.listOfParticipants.length} participants
                </Text>
              </>
            ) : (
              <Text fontWeight="bold">{user.handle}</Text>
            )}
          </Box>
        </HStack>
        <HStack>
          {user && channel.team.listOfParticipants.length === 2 ? (
            <FontAwesomeIcon icon={faPhoneVolume} size="lg" cursor="pointer" style={{ color: '#00B998', marginRight: '10px' }} onClick={() => handleCall([user.handle])} />
          ) : (
            <FontAwesomeIcon icon={faPhoneVolume} size="lg" cursor="pointer" style={{ color: '#00B998', marginRight: '10px' }} onClick={() => handleCall(channel.team)} />
          )}
          {isLargeScreen && (
            <FontAwesomeIcon
              icon={faInfo}
              size="lg"
              cursor="pointer"
              onClick={toggleTeamInfo}
              style={{ color: "#00B998" }}
            />
          )}
        </HStack>
      </Flex>
    </Box>
  );  
};

ChatBar.propTypes = {
  toggle: PropTypes.func.isRequired,
  chat: PropTypes.object,
  channel: PropTypes.object,
  users: PropTypes.object,
  handleCall: PropTypes.func.isRequired,
};

export default ChatBar