import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPaperPlane,
  faImage,
  faGift,
  faSmile,
} from "@fortawesome/free-solid-svg-icons";
import { useContext, useState, useRef } from "react";
import { addMessage, addFileMessage } from "../../services/channels.service";
import AppContext from "../../context/AuthContext";
import {
  Input,
  Box,
  Flex,
  Button,
  Avatar,
  InputGroup,
  InputRightElement,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalHeader,
  ModalCloseButton,
  Grid,
  Image,
  ModalBody,
  ModalContent,
} from "@chakra-ui/react";
import { GIPHY_API_KEY } from "../../common/constants";
import axios from "axios";
import Picker from "emoji-picker-react";
import PropTypes from "prop-types";

const InputMessages = ({ chat, channel, users }) => {
  const { userData } = useContext(AppContext);
  const [query, setQuery] = useState("");
  const [chosenEmoji, setChosenEmoji] = useState(null);
  const [isEmojiPickerOpen, setEmojiPickerOpen] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [gifs, setGifs] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [selectedFile, setSelectedFile] = useState(null);
  const fileInputRef = useRef(null);
  // console.log
  const user = users.get(userData.uid);
  // console.log(chat);
  // console.log(channel);

  const onEmojiClick = (emojiObject, event) => {
    console.log(emojiObject);
    setChosenEmoji(emojiObject);
    setQuery((prevQuery) => prevQuery + emojiObject.emoji);
    setEmojiPickerOpen(false);
  };

  const handleGifs = async () => {
    const gifs = await fetchGifs();
    console.log(gifs);
    setGifs(gifs);
    onOpen();
  };

  const searchGifs = async (e) => {
    e.preventDefault();
    const gifs = await fetchGifs(searchQuery);
    setGifs(gifs);
  };

  const fetchGifs = async (query) => {
    const url = query
      ? `https://api.giphy.com/v1/gifs/search?api_key=${GIPHY_API_KEY}&q=${query}&limit=25&offset=0&rating=g&lang=en`
      : `https://api.giphy.com/v1/gifs/trending?api_key=${GIPHY_API_KEY}&limit=25&rating=g`;

    try {
      const response = await axios.get(url);
      return response.data.data;
    } catch (error) {
      console.error("Error fetching gifs", error);
      return [];
    }
  };

  const onGifClick = (gifUrl, e) => {
    // console.log(gif)
    setQuery(gifUrl);
    // sendMessage(e, 'gif')
    // const test = new File([gif.type], "gifFile.gif", {type : gif.type})
    // console.log(test)
    // setSelectedFile(test)
    onClose();
  };

  // const onGifClick = (gifUrl, gif) => {
  //   fetch(gifUrl)
  //     .then((response) => {
  //       if (!response.ok) {
  //         throw new Error("Failed to fetch GIF");
  //       }
  //       return response.blob();
  //     })
  //     .then((blob) => {
  //       console.log(blob);
  //       // const file = new File([blob], "gifFile.gif", { type: blob.type });
  //       // setSelectedFile(() => file);
  //       // console.log(file.name)
  //       setQuery(gifUrl);
  //     })
  //     .catch((error) => {
  //       console.error(error);
  //     });

  //   onClose();
  // };

  const sendMessage = (e, type) => {
    e.preventDefault();
    console.log("tuk");

    if (query.trim() === "") {
      throw new Error("The input message is empty");
    }

    if (query.includes("gif")) {
      console.log("hvanah go");
      type = "gif";
      console.log(type);
    }

    let seenByUsers = null;

    if (channel.team.listOfParticipants.length === 1) {
      seenByUsers =
        userData.uid === channel.owner
          ? { [channel.team.listOfParticipants[0]]: false }
          : { [channel.owner]: false };
    } else {
      seenByUsers = channel.team.listOfParticipants.reduce((acc, value) => {
        if (value !== userData.uid) {
          acc[value] = false;
        } else {
          acc[userData.uid] = true;
        }
        return acc;
      }, {});
    }

    // console.log(test)
    const comment = {
      from: userData.handle,
      created_at: new Date().toISOString(),
      uid: userData.uid,
      message: query,
      type: type,
      seenBy: seenByUsers,
    };

    if (selectedFile) {
      addFileMessage(chat.id, channel.id, selectedFile, userData, seenByUsers);
    } else {
      addMessage(chat.id, channel.id, comment);
    }

    setQuery("");
    setEmojiPickerOpen(false);
    setSelectedFile("");
  };

  const onTyping = (e) => {
    e.preventDefault();
    setQuery(e.target.value);
    console.log(query);
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      e.preventDefault();
      console.log(e.target.value);
      setQuery(e.target.value);
      sendMessage(e, "text");
    }
  };

  const handleFileMessage = (e) => {
    const file = e.target.files[0];
    setSelectedFile(file);
    setQuery(file.name);
    e.target.value = "";
  };

  return (
    <>
      <Flex
        minWidth={"max-content"}
        alignItems={"center"}
        flexDirection={"row"}
        justifyContent={"center"}
        height={"100%"}
      >
        {userData && (
          <Avatar
            src={user.profileImageUrl}
            name={user.handle}
            size={"sm"}
            m={"0px 10px"}
          />
        )}
        <InputGroup width={"90%"} mr={"15px"}>
          <Input
            border={"none"}
            placeholder={"Type here..."}
            value={query}
            onChange={onTyping}
            bg={"#1F232F"}
            pr={"125px"}
            onKeyPress={handleKeyPress}
            focusBorderColor={"#00B998"}
          />
          <InputRightElement mr={"45px"}>
            <Box position="relative">
              {isEmojiPickerOpen && (
                <Box
                  position="absolute"
                  top="-480px" // Adjust the position as needed
                  right="-10"
                  bg="#FFFFFF"
                  // color={"#292F3F"}
                  border="1px solid #E2E8F0"
                  borderRadius="4px"
                  padding="10px"
                  boxShadow="0px 2px 4px rgba(0, 0, 0, 0.1)"
                >
                  <Picker style={{ backGround: "#292F3F" }} onEmojiClick={onEmojiClick} />
                </Box>
              )}
              <Input
                type="file"
                ref={fileInputRef}
                style={{ display: "none" }}
                onChange={handleFileMessage}
              />
              <FontAwesomeIcon
                icon={faImage}
                size="lg"
                style={{
                  color: "#00B998",
                  margin: "0px 10px",
                  cursor: "pointer",
                }}
                onClick={() => fileInputRef.current.click()}
              />
            </Box>
            <FontAwesomeIcon
              icon={faGift}
              size="lg"
              style={{
                color: "#00B998",
                marginRight: "10px",
                cursor: "pointer",
              }}
              onClick={handleGifs}
            />
            <FontAwesomeIcon
              icon={faSmile}
              size="lg"
              style={{
                color: "#00B998",
                marginRight: "10px",
                cursor: "pointer",
              }}
              onClick={() => setEmojiPickerOpen(!isEmojiPickerOpen)}
            />
            <FontAwesomeIcon
              icon={faPaperPlane}
              size="lg"
              style={{
                color: "#00B998",
                marginRight: "10px",
                cursor: "pointer",
              }}
              onClick={(e) => sendMessage(e, "text")}
            />
          </InputRightElement>
        </InputGroup>
      </Flex>
      <Modal isOpen={isOpen} onClose={onClose} size={"xl"}>
        <ModalOverlay />
        <ModalContent h={"70vh"}>
          <ModalHeader bg={"#00B998"}> Browse Gifs</ModalHeader>
          <ModalCloseButton />
          <Box
            h="calc(100vh - 120px)"
            overflowY="auto"
            display={"flex"}
            alignContent={"center"}
            justifyContent={"center"}
            sx={{
              "&::-webkit-scrollbar": {
                width: "0px",
              },
              "&::-webkit-scrollbar-thumb": {
                background: "#00B998",
                borderRadius: "10px",
              },
            }}
          >
            <ModalBody>
              <Flex alignItems="center" m="20px 0">
                <Input
                  placeholder="Search for Gifs..."
                  value={searchQuery}
                  onChange={(e) => setSearchQuery(e.target.value)}
                  mr={2}
                  pr={"80px"}
                  width={"81%"}
                  focusBorderColor={"#00B998"}
                />
                <Button
                  type="submit"
                  onClick={searchGifs}
                  // mt={"25px"}
                  p={"10px 15px"}
                  bg={"gray.700"}
                  border={"1px"}
                  borderColor={"#00B998"}
                  _hover={{
                    bg: "#00B998",
                    boxShadow: "0 10px 20px rgba(0, 185, 152, 0.4)",
                  }}
                >
                  Search
                </Button>
              </Flex>
              <Grid templateColumns="repeat (3, 1fr)" gap={6}>
                {gifs.map((gif, index) => (
                  <Image
                    key={index}
                    src={gif.images.original.url}
                    alt={gif.title}
                    onClick={(e) => onGifClick(gif.images.original.url, e)}
                    cursor="pointer"
                    w={"515px"}
                    h={"500px"}
                  />
                ))}
              </Grid>
            </ModalBody>
          </Box>
        </ModalContent>
      </Modal>
    </>
  );
};

InputMessages.propTypes = {
  chat: PropTypes.object.isRequired,
  channel: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
};

export default InputMessages;