import {
  Button,
  Box,
  Input,
  Avatar,
  Text,
  Container,
  FormLabel,
} from "@chakra-ui/react";
import { useContext, useEffect, useState } from "react";
import AppContext from "../../context/AuthContext";
import { useNavigate } from "react-router-dom";
import {
  getUserByHandle,
  getUserData,
  getUserDataByUserName,
} from "../../services/users.service";
import {
  addTeamMember,
  changePhoto,
  removeMember,
  updateDescriptionText,
  leaveChat,
} from "../../services/channels.service";
import RemoveView from "./RemoveView";
import AddMemberView from "./AddMemberView";
import Description from "./Description";
import { db } from "../../config/firebase";
import { ref, onValue, off, onChildAdded, onChildRemoved} from "firebase/database";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import UserProfilePreview from "../UserProfilePreview/UserProfilePreview";
import { deleteChannel } from "../../services/channels.service";

const TeamInfo = ({ selectedChannel }) => {
  const [selectedUsers, setSelectedUsers] = useState(new Map());
  const [file, setFile] = useState(null);
  const [description, setDescription] = useState("");
  const [query, setQuery] = useState("");
  const [valueDescription, setValueDescription] = useState("");
  const { userData } = useContext(AppContext);
  const [users, setUsers] = useState("");
  const [showUserPreview, setUserPreview] = useState(null);

  const navigate = useNavigate();

  // console.log(selectedChannel);
  useEffect(() => {
    if (selectedChannel) {
      const teamPhotoRef = ref(db, `channels/${selectedChannel.id}/team`);
      const onPhotoChanged = onValue(teamPhotoRef, (snapshot) => {
        const updatedPic = snapshot.val();
        setFile(updatedPic.teamPhoto);
      });
  
      const descriptionRef = ref(
        db,
        `channels/${selectedChannel.id}/team/description`
      );
      const onDescriptionChanged = onValue(descriptionRef, (snapshot) => {
        const updatedDescription = snapshot.val();
        setDescription(updatedDescription);
      });
  
      const membersRefAdded = ref(
        db,
        `channels/${selectedChannel.id}/team/listOfParticipants`
      );
      const onMemberAdded = onChildAdded(membersRefAdded, (snapshot) => {
        const addedMember = snapshot.val();
        fetchUsersData(addedMember);
      });

      const membersRefRemoved = ref(
        db,
        `channels/${selectedChannel.id}/team/listOfParticipants`
      );
      const onMemberRemoved = onChildRemoved(membersRefRemoved, (snapshot) => {
        const removedMember = snapshot.val();
        removeFromDisplayedUsers(removedMember);
      });
  
      return () => {
        off(teamPhotoRef, onPhotoChanged);
        off(descriptionRef, onDescriptionChanged);
        off(membersRefAdded, 'child_added', onMemberAdded);
        off(membersRefRemoved, 'child_removed', onMemberRemoved);
      };
    }
  }, [selectedChannel]);


  useEffect(() => {
    if (selectedChannel && selectedChannel.team.listOfParticipants) {
      fetchUsersData();
    }
  }, [selectedChannel, selectedChannel?.team?.listOfParticipants]);
  
  const fetchUsersData = (newMember) => {
    const participants = selectedChannel.team.listOfParticipants;
    const promises = participants.map((participantId) =>
      getUserData(participantId).then((snapshot) => {
        if (snapshot.exists()) {
          const key = Object.keys(snapshot.val());
          return { ...snapshot.val()[key] };
        }
        return null;
      })
    );
    
    const isNewMember = !participants.includes(newMember);
    if (newMember && isNewMember) {
      promises.push(getUserData(newMember).then((snapshot) => {
        if (snapshot.exists()) {
          const key = Object.keys(snapshot.val());
          return { ...snapshot.val()[key] };
        }
        return null;
      }));
    }
  
    Promise.all(promises)
      .then((userObjects) => {
        const filteredUsers = userObjects.filter((user) => user !== null);
        console.log(filteredUsers);
        setUsers(filteredUsers);
      })
      .catch((error) => {
        console.error("Error fetching user data:", error);
      });
  };

  const removeFromDisplayedUsers = (removedMember) => {
    setUsers((prevUsers) =>
      prevUsers.filter((user) => user.uid !== removedMember)
    );
  };

  const getUsers = (handle) => {
    getUserDataByUserName(handle)
      .then((snapshot) => {
        if (snapshot.exists()) {
          const key = Object.keys(snapshot.val());
          const userInfo = snapshot.val()[key];
          const userId = snapshot.val()[key].uid;

          //TODO: Да помисля какво може да се прецака
          setSelectedUsers((prevSelectedUsers) =>
            new Map(prevSelectedUsers).set(userId, userInfo)
          );
        } else {
          alert("This user doesn`t exist");
          return;
        }
      })
      .catch((err) =>
        console.error(`The userDataByEmail in search: ${console.log(err)}`)
      );
  };

  const removeTeamMember = (e, selectedChannel, member) => {
    e.preventDefault();
    const memberId = member.values().next().value.uid;
    removeMember(
      selectedChannel.id,
      memberId,
      selectedChannel.team.listOfParticipants
    );
    setSelectedUsers(new Map());
  };

  const addMember = (e, selectedChannel, member) => {
    e.preventDefault();
    const memberId = member.values().next().value.uid;
    addTeamMember(
      selectedChannel.id,
      memberId,
      selectedChannel.team.listOfParticipants
    );
    setSelectedUsers(new Map());
  };

  const leaveChatFunc = (e) => {
    e.preventDefault();
    leaveChat(
      selectedChannel.id,
      userData.uid,
      selectedChannel.team.listOfParticipants
    );
    navigate(`/channel`);
  };

  const removeUser = (uid) => {
    const updatedSelectedUsers = new Map(selectedUsers);
    updatedSelectedUsers.delete(uid);
    setSelectedUsers(updatedSelectedUsers);
  };

  const addUser = (user) => {
    setQuery("");
    getUsers(user);
  };

  const onSearch = (e) => {
    e.preventDefault();
    setQuery(e.target.value);
  };

  const changeTeamPhoto = (e) => {
    e.preventDefault();
    const pic = e.target.files[0];
    changePhoto(selectedChannel.team.teamName, selectedChannel.id, pic);
    e.target.value = "";
  };

  const onChangeDescription = (e) => {
    e.preventDefault();
    setValueDescription(e.target.value);
  };

  const updateDescription = (e) => {
    e.preventDefault();
    updateDescriptionText(selectedChannel.id, valueDescription);
    setValueDescription("");
  };

  const handleProfileView = (user) => {
    console.log(user);
    getUserByHandle(user)
    .then((snapshot) => setUserPreview(snapshot.val()))
    // setUserPreview(true)
  }
  const handleClosePreview = () => {
    setUserPreview(false)
  };

  const handleKeyPress = (e, user) => {
    if (e.key === "Enter") {
      setQuery('');
      getUsers(user);
    }
  };

  const deleteTheChannel = (id) => {
    deleteChannel(id)
    navigate('/channel')
  }

  return userData && selectedChannel ? (
    <Container
      maxW={"full"}
      maxH={"100vh"}
      overflowY={"auto"}
      bg={"#1F232F"}
      p={"0px"}
      sx={{
        "&::-webkit-scrollbar": {
          width: "0px",
        },
        "&::-webkit-scrollbar-thumb": {
          // background: "#00B998",
          borderRadius: "10px",
        }
      }}
    >
      <Box>
        <Text
          fontSize={"18px"}
          fontWeight={"bold"}
          borderBottom={"1px"}
          borderColor={"#292F3F"}
          p={"21.5px 20px"}
          // m={"0px 20px"}
        >
          Group info
        </Text>
      </Box>
      <Box
        display={"flex"}
        flexDirection={"column"}
        alignItems={"center"}
        m={"20px 0"}
        // p={"0px 20px"}
      >
        <Avatar name={selectedChannel.team.teamName} size={"2xl"} src={file} />
        <Text mt={"20px"} fontSize={"20px"} fontWeight={"bold"}>
          {selectedChannel.team.teamName}
        </Text>
        <Text fontSize={"14px"} color={"gray.100"} mb={"20px"}>
          {selectedChannel.team.listOfParticipants.length} members
        </Text>
        {userData.uid === selectedChannel.owner ? (
          <>
            <FormLabel
              htmlFor="upload-input"
              p={"10px 15px"}
              cursor={"pointer"}
              bg={"#292F3F"}
              borderRadius={"5px"}
              fontSize={"16px"}
              _hover={{ bg: "#00B998" }}
              m={"0px"}
            >
              Upload Picture
              <Input
                id="upload-input"
                type="file"
                onChange={changeTeamPhoto}
                style={{ display: "none" }}
              />
            </FormLabel>
          </>
        ) : null}
      </Box>
      {userData.uid === selectedChannel.owner ? (
        <>
          <Box p={"0px"} ml={"20px"}>
            <Text fontSize={"20px"} fontWeight={"bold"}>
              Description
            </Text>
            <Text>{description}</Text>
          </Box>
          <Description
            updateDescription={updateDescription}
            value={valueDescription}
            onChangeDescription={onChangeDescription}
            selectedChannel={selectedChannel}
          />
        </>
      ) : (
        <Box ml={"20px"}>
          <Text fontSize={"20px"} fontWeight={"bold"}>
            Description
          </Text>
          <Text>{selectedChannel.team.description}</Text>
        </Box>
      )}
      <Box
        m={"20px 20px"}
        borderBottom={"1px"}
        display={"flex"}
        flexDirection={"row"}
        alignItems={"center"}
      >
        <FontAwesomeIcon
          icon={faUser}
          size={"xl"}
          style={{
            //  background: '#00B998',
            padding: "10px 5px",
            borderRadius: "25px",
          }}
        />
        <Text ml={"5px"} fontSize={"18px"} fontWeight={"bold"}>
          Members
        </Text>
        <Text ml={"5px"} fontSize={"14px"}>
          ({selectedChannel.team.listOfParticipants.length})
        </Text>
      </Box>
      <Box m={"20px 20px"}>
        {users &&
          users.map((user) => (
            <Box
              display={"flex"}
              flexDirection={"row"}
              alignItems={"center"}
              overflow="hidden"
              key={user.uid}
            >
              {console.log(user.handle)}
              <Avatar
                size={"sm"}
                name={user.handle}
                src={user.profileImageUrl}
                cursor={"pointer"}
                onClick={() => handleProfileView(user.handle)}
                _hover={{ border: '2px solid gray'}}
              />

              <Text ml={"10px"} py="2" _hover={{ bg: 'gray.700'}} p="8px" borderRadius={"10px 10px 10px 10px"} cursor={"pointer"} onClick={() => handleProfileView(user.handle)}>
                {user.handle}
              </Text>
            </Box>
          ))}
      </Box>
      {userData.uid === selectedChannel.owner ? (
        <>
          <AddMemberView
            onSearch={onSearch}
            query={query}
            addUser={addUser}
            addMember={addMember}
            removeUser={removeUser}
            selectedChannel={selectedChannel}
            selectedUsers={selectedUsers}
            handleKeyPress={handleKeyPress}
          />
          <RemoveView
            onSearch={onSearch}
            query={query}
            addUser={addUser}
            removeTeamMember={removeTeamMember}
            removeUser={removeUser}
            selectedChannel={selectedChannel}
            selectedUsers={selectedUsers}
            handleKeyPress={handleKeyPress}
          />
          <Button
          w={"88%"}
          m={"1px 20px"}
          bg={"#292F3F"}
          _hover={{background: 'red'}}
          onClick={() => deleteTheChannel(selectedChannel.id)}
          >
            Delete channel
          </Button>
        </>
      ) : (
        <Button
          onClick={(e) => leaveChatFunc(e)}
          w={"88%"}
          bg={"#292F3F"}
          m={"0px 20px"}
          _hover={{ bg: "red" }}
        >
          Leave chat
        </Button>
      )}
      {showUserPreview && 
         <UserProfilePreview setProfilePreview={handleClosePreview} user={showUserPreview} location={'Chats'}/>
      }
    </Container>
  ) : null;
};

export default TeamInfo;
