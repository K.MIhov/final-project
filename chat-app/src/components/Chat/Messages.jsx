import {
  Text,
  Avatar,
  Box,
  Container,
  Image,
  Link,
  HStack,
} from "@chakra-ui/react";
import { useEffect, useState, useContext, useRef } from "react";
import {
  ref,
  onChildAdded,
  off,
  update,
  remove,
  onValue,
} from "firebase/database";
import { db } from "../../config/firebase";
import { sendNotification } from "../../services/notifications.service";
import AppContext from "../../context/AuthContext";
import { addOrRemoveReaction } from "../../services/channels.service";
import PropTypes from "prop-types";

const Messages = ({ idChat, channel, users }) => {
  const [removeNotification, setRemoveNotification] = useState(null);
  const [allMessages, setAllMessages] = useState({});
  const [notifications, setNotifications] = useState({});
  const { userData } = useContext(AppContext);
  const [emojiPickerVisible, setEmojiPickerVisible] = useState(false);
  const emojiOptions = ["👍", "👎", "❤️", "😂", "😢"];
  const handleReaction = async (event, messageId, emoji) => {
    event.stopPropagation();
    console.log("tuk");
    const reaction = {
      from: userData.handle,
      emoji: emoji,
    };

    try {
      await addOrRemoveReaction(channel.id, idChat, messageId, reaction);
    } catch (error) {
      console.error("Error updating reaction: ", error);
    }
  };

  const messagesEndRef = useRef(null);
  console.log(idChat)

  useEffect(() => {
    setAllMessages({});
    const cleanupFunctions = [];

    const messagesRef = ref(
      db,
      `channels/${channel.id}/chat/${idChat}/messages`
    );

    const onMessageAdded = onChildAdded(messagesRef, (snapshot) => {
      const messageId = snapshot.key;
      const updatedMessage = snapshot.val();
      console.log(messageId)
      console.log(updatedMessage)
      const reactionsRef = ref(
        db,
        `channels/${channel.id}/chat/${idChat}/messages/${messageId}/reactions`
      );

      const onReactionsChanged = onValue(reactionsRef, (snapshot) => {
        const updatedReactions = snapshot.val() || [];

        setAllMessages((prevMessages) => ({
          ...prevMessages,
          [messageId]: {
            ...prevMessages[messageId],
            reactions: updatedReactions,
          },
        }));
      });

      cleanupFunctions.push(() => off(reactionsRef, onReactionsChanged));

      const unseenUsers = Object.entries(updatedMessage.seenBy || {}).filter(
        ([userId, seen]) => !seen && userId === userData?.uid
      );

      if (unseenUsers.length > 0) {
        const seenByRef = ref(
          db,
          `channels/${channel.id}/chat/${idChat}/messages/${messageId}/seenBy`
        );
        // console.log(unseenUsers)
        const seenByUpdate = unseenUsers.reduce((update, [userId]) => {
          update[userId] = true;
          return update;
        }, {});

        update(seenByRef, seenByUpdate);
        setRemoveNotification(messageId);
      } else {
        setNotifications((prevNotifications) => ({
          ...prevNotifications,
          [messageId]: updatedMessage,
        }));
      }

      setAllMessages((prevMessages) => ({
        ...prevMessages,
        [messageId]: {
          ...updatedMessage,
          reactions: updatedMessage.reactions
            ? [...updatedMessage.reactions]
            : [],
        },
      }));
    });

    cleanupFunctions.push(() => {
      off(messagesRef, "child_added", onMessageAdded);
    });

    return () => {
      cleanupFunctions.forEach((cleanupFunction) => cleanupFunction());
    };
  }, [idChat, channel.id, userData]);

  useEffect(() => {
    // console.log(notifications);
    Object.entries(notifications).forEach(([messageId, message]) => {
      const unseenUsers = Object.entries(message.seenBy || {})
        .filter(([userId, seen]) => !seen && userId !== userData?.uid)
        .map(([userId]) => userId);

      // console.log(unseenUsers);
      unseenUsers.forEach((userId) => {
        console.log(
          `Send notification to user ${userId} for message ${messageId}`
        );
        sendNotification(
          userId,
          channel.id,
          message.from,
          message.created_at,
          message.message,
          message.type,
          channel.team,
          messageId
        );
      });
    });

    if (removeNotification) {
      // console.log(removeNotification);
      const notificationsRef = ref(db, "notifications");
      onChildAdded(notificationsRef, (snapshot) => {
        const notification = snapshot.val();
        if (
          notification.idMessage === removeNotification &&
          notification.idUser === userData.uid
        ) {
          console.log("Removing notification");
          const notificationId = snapshot.key;
          const notificationRefToRemove = ref(
            db,
            `notifications/${notificationId}`
          );
          remove(notificationRefToRemove);
        }
      });
    }

    setRemoveNotification(null);
    setNotifications({});
  }, [allMessages, removeNotification, userData?.uid, channel.id]);

  useEffect(() => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [allMessages]);

  const getTimeText = (timestamp) => {
    const now = new Date();
    const messageTime = new Date(timestamp);

    const diffInSeconds = Math.floor((now - messageTime) / 1000);

    if (diffInSeconds < 5) {
      return "Just now";
    } else if (diffInSeconds < 60) {
      return `${diffInSeconds} seconds ago`;
    } else if (diffInSeconds < 3600) {
      const minutes = Math.floor(diffInSeconds / 60);
      return `${minutes} minutes ago`;
    } else if (diffInSeconds < 86400) {
      const hours = Math.floor(diffInSeconds / 3600);
      return `${hours} hours ago`;
    } else {
      const options = { year: "numeric", month: "long", day: "numeric" };
      return messageTime.toLocaleDateString(undefined, options);
    }
  };

  return (
    <>
      {Object.entries(allMessages).map(([_, message]) => {
        const isOwner = userData.uid === message.uid;
        const getUser = isOwner
          ? users.get(userData.uid)
          : users.get(message.uid);
        const timeText = getTimeText(message.created_at);
        const isGif = message.type.includes("gif");
        const isImage = message.type.includes("image");
        const isFile = message.type.includes("application");
        return (
          <Container
            key={message.id}
            display="flex"
            flexDirection={isOwner ? "column" : "row"}
            alignItems={isOwner ? "flex-end" : "flex-start"}
            mb={2}
            maxW="100%"
            pt={4}
            position="relative"
            onClick={() => {
              console.log("test");
              setEmojiPickerVisible(message.id);
            }}
            onMouseLeave={() => {
              if (emojiPickerVisible === message.id) {
                setEmojiPickerVisible(false);
              }
              // { console.log('you are there') }
            }}
          >
            {isOwner ? (
              <>
                <Box display="flex" alignItems="center" mb={1}>
                  <Text fontSize="xs" color="gray.500" mr={"5px"}>
                    {timeText}
                  </Text>
                  <Text fontSize="sm" fontWeight="bold" mr={"5px"}>
                    You
                  </Text>
                  <Avatar
                    src={getUser.profileImageUrl}
                    name={getUser.handle}
                    size="sm"
                  />
                </Box>
                {isGif && (
                  <Image
                    src={message.message}
                    boxSize="200px"
                    objectFit="cover"
                    alignSelf={isOwner ? "flex-end" : "flex-start"}
                  />
                )}
                {isImage && (
                  <Image
                    src={message.message.url}
                    // boxSize="200px"
                    // objectFit="cover"
                    alignSelf={isOwner ? "flex-end" : "flex-start"}
                  />
                )}
                {isFile && (
                  <Link
                    href={message.message.url}
                    isExternal
                    download={message.message.name}
                  >
                    {message.message.name}
                  </Link>
                )}
                {!isImage && !isFile && !isGif && (
                  <Text
                    bg={isOwner ? "#00B998" : "gray.200"}
                    color={isOwner ? "white" : "black"}
                    py={1}
                    px={2}
                    borderRadius={
                      isOwner ? "10px 0px 10px 10px" : "0px 10px 10px 10px"
                    }
                    width="auto"
                    alignSelf={isOwner ? "flex-end" : "flex-start"}
                    cursor={"pointer"}
                    sx={{
                      wordBreak: "break-word",
                    }}
                  >
                    {message.message}
                  </Text>
                )}
                {emojiPickerVisible === message.id && (
                  <Box
                  position="absolute"
                  bottom="50px"
                    zIndex="1"
                    bgColor="white"
                    borderRadius={"10px 10px 10px 10px"}
                  >
                    <HStack>
                      {/* {console.log(emojiOptions)} */}
                      {emojiOptions.map((emoji) => (
                        <Box
                          p="3px"
                          key={emoji}
                          cursor={"pointer"}
                          onClick={(event) => {
                            handleReaction(event, message.id, emoji);
                          }}
                        >
                          {emoji}
                        </Box>
                      ))}
                    </HStack>
                  </Box>
                )}
                <HStack>
                  {message.reactions &&
                    Object.values(message.reactions)
                      .map((reaction) => (
                        <Text key={reaction.emoji}> {reaction.emoji}</Text>
                      ))}
                </HStack>
              </>
            ) : (
              <>
                <Avatar
                  src={getUser.profileImageUrl}
                  name={getUser.handle}
                  size="md"
                  ml={"5px"}
                />
                <Box>
                  <Box display="flex" alignItems="center" mb={1}>
                    <Text fontSize="sm" fontWeight="bold" ml={"5px"}>
                      {message.from}
                    </Text>
                    <Text fontSize="xs" color="gray.500" ml={"5px"}>
                      {timeText}
                    </Text>
                  </Box>
                  {isGif && (
                    <Image
                      src={message.message}
                      boxSize="200px"
                      objectFit="cover"
                      alignSelf={isOwner ? "flex-end" : "flex-start"}
                    />
                  )}
                  {isImage && (
                    <Image
                      src={message.message.url}
                      // boxSize="200px"
                      // objectFit="cover"
                      alignSelf={isOwner ? "flex-end" : "flex-start"}
                    />
                  )}
                  {isFile && (
                    <Link
                      href={message.message.url}
                      isExternal
                      download={message.message.name}
                    >
                      {message.message.name}
                    </Link>
                  )}
                  {!isImage && !isFile && !isGif && (
                    <Text
                      bg={isOwner ? "#00B998" : "gray.200"}
                      color={isOwner ? "white" : "black"}
                      py={1}
                      px={2}
                      ml={"5px"}
                      borderRadius={
                        isOwner ? "10px 0px 10px 10px" : "0px 10px 10px 10px"
                      }
                      width="auto"
                      alignSelf={isOwner ? "flex-end" : "flex-start"}
                      cursor={"pointer"}
                      sx={{
                        wordBreak: "break-word",
                      }}
                    >
                      {message.message}
                    </Text>
                  )}
                  {emojiPickerVisible === message.id && (
                    <Box
                      zIndex="1"
                      bgColor="white"
                      borderRadius={"10px 10px 10px 10px"}
                    >
                      <HStack>
                        {emojiOptions.map((emoji) => (
                          <Box
                            p="3px"
                            key={emoji}
                            cursor={"pointer"}
                            onClick={(event) => {
                              handleReaction(event, message.id, emoji);
                            }}
                          >
                            {emoji}
                          </Box>
                        ))}
                      </HStack>
                    </Box>
                  )}
                  <HStack>
                    {message.reactions &&
                      Object.values(message.reactions)
                        .map((reaction) => (
                          <Text key={reaction.emoji}> {reaction.emoji}</Text>
                        ))}
                  </HStack>
                </Box>
              </>
            )}
          </Container>
        );
      })}
      <Box ref={messagesEndRef} />
    </>
  );
};

Messages.propTypes = {
  idChat: PropTypes.string.isRequired,
  channel: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
};

export default Messages;
