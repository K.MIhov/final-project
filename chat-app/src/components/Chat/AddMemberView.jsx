import {
  Avatar,
  Box,
  Button,
  Card,
  CardBody,
  Input,
  InputGroup,
  InputRightElement,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { faDeleteLeft, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";

const AddMemberView = ({
  onSearch,
  query,
  addUser,
  addMember,
  removeUser,
  selectedChannel,
  selectedUsers,
  handleKeyPress,
}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Box>
      <Button
        onClick={onOpen}
        w={"88%"}
        bg={"#292F3F"}
        _hover={{ bg: "#00B998" }}
        m={"1px 20px"}
      >
        Add Member
        <FontAwesomeIcon
          icon={faPlus}
          size={"lg"}
          cursor={"pointer"}
          style={{ marginLeft: "10px" }}
        />
      </Button>

      <Modal isOpen={isOpen} onClose={onClose} size={"xl"}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader bg={"#00B998"}>Add members</ModalHeader>
          <ModalCloseButton />
          <ModalBody m={"20px 0px"}>
            <InputGroup>
              <Input
                placeholder="Search for the member"
                size="md"
                variant="outline"
                value={query}
                onChange={onSearch}
                focusBorderColor={"#00B998"}
                onKeyPress={(e) => handleKeyPress(e, query)}
              />
              <InputRightElement>
                <Button onClick={() => addUser(query)}>
                  <FontAwesomeIcon
                    icon={faPlus}
                    size={"lg"}
                    style={{ color: "#00B998", background: "rgba(0, 0, 0, 0)" }}
                  />
                </Button>
                {/* <Button onClick={() => addUser(query)}>Add</Button> */}
              </InputRightElement>
            </InputGroup>
            {Array.from(selectedUsers.values()).map((obj) => {
              return (
                <Card
                  direction={{ base: "column", sm: "row" }}
                  overflow="hidden"
                  variant="outline"
                  key={obj.uid}
                  m={"10px 0px"}
                  p={"10px"}
                >
                  <Link to={`/profile/${obj.uid}`}>
                    <Avatar name={obj.handle} size={"sm"} mt={"5px"} />
                  </Link>

                  <CardBody p={"5px 5px"}>
                    <Text mt={"5px"}>{obj.handle}</Text>
                  </CardBody>
                  <Button onClick={() => removeUser(obj.uid)}>
                    <FontAwesomeIcon icon={faDeleteLeft} size={"md"} />
                  </Button>
                </Card>
              );
            })}
            <Box
              display={"flex"}
              alignItems={"center"}
              justifyContent={"center"}
            >
              <Button
                type="submit"
                onClick={(e) => addMember(e, selectedChannel, selectedUsers)}
                mt={"25px"}
                p={"10px 25px"}
                bg={"gray.700"}
                border={"1px"}
                borderColor={"#00B998"}
                _hover={{
                  bg: "#00B998",
                  boxShadow: "0 10px 20px rgba(0, 185, 152, 0.4)",
                  transform: "translateY(-2px)",
                }}
              >
                Add member
              </Button>
            </Box>
          </ModalBody>
        </ModalContent>
      </Modal>
    </Box>
  );
};

export default AddMemberView;
