import { Box, Button, Tooltip } from '@chakra-ui/react';
import { BsMoon, BsSun } from 'react-icons/bs';
import {
  BsCalendar2,
  BsCameraVideo,
  BsChatDots,
  BsDoorOpen,
  BsGear,
  BsJoystick,
  BsPeople,
//   BsSun,
  BsPlusCircle,
  BsTelephone,
//   BsMoon
} from "react-icons/bs";
const NavBarLinks = ({  handleIconClick, toggleColorMode, colorMode, onOpen, displayAttribute }) => {
    const navBarLinks = [
        // {
        //     id: "Notifications",
        //     icon: notificationCount ? (
        //         <Tooltip label="Notifications">
        //             <VscBellDot style={{ fontSize: "20px", color: 'red' }} />
        //         </Tooltip>
        //     ) : (
        //         <Tooltip label="Notifications">
        //             <VscBell style={{ fontSize: "20px", color: 'white' }} />
        //         </Tooltip>
        //     ),
        // },
        { id: "Chats", icon: <BsChatDots style={{ fontSize: "20px", color: 'white' }} /> },
        { id: "Team", icon: <BsPeople style={{ fontSize: "20px", color: 'white' }} /> },
        { id: "Calls", icon: <BsTelephone style={{ fontSize: "20px", color: 'white', backGround: '#34C7AD' }} /> },
        // { id: 'Friends', icon: <BsPersonPlus style={{ fontSize: "20px", color: 'white', backGround: '#34C7AD' }} /> },
        { id: 'Fun', icon: <BsJoystick style={{ fontSize: "20px", color: 'white', backGround: '#34C7AD' }} /> },
        { id: 'Calendar', icon: <BsCalendar2 style={{ fontSize: "20px", color: 'white', backGround: '#34C7AD' }} /> },
        { id: 'Create Group Chat', icon: <BsPlusCircle style={{ fontSize: "20px", color: 'white', backGround: '#34C7AD' }} onClick={onOpen} /> },
      ];
  return (
    <>
      {navBarLinks.map((link, index) => (
        <Box display={displayAttribute} key={link.id}>
          <Tooltip label={link.id} placement="right">
            <Button
            //   mt={{ base: '5px', xl: '0px', '2xl': '20px' }}
              p="10px"
              borderRadius="25px"
              display="flex"
              alignItems="center"
              color="gray.500"
              bg="#34C7AD"
              onClick={() => handleIconClick(link.id)}
            >
              {link.icon}
            </Button>
          </Tooltip>
        </Box>
      ))}
      {/* <Box display={displayAttribute} pr={{ sm: '20px', md: '0px' }}>
        <Tooltip label="Who uses light mode!?" placement="right">
          <Button
            p="10px"
            borderRadius="25px"
            bg="#34C7AD"
            color="white"
            onClick={toggleColorMode}
          >
            {colorMode === 'light' ? <BsMoon /> : <BsSun />}
          </Button>
        </Tooltip>
      </Box> */}
    </>
  );
};

export default NavBarLinks;
