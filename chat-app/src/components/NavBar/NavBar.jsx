import {
    Box
} from "@chakra-ui/react";
import {
    MeetingProvider,
} from "@videosdk.live/react-sdk";
import { off, onChildAdded, onChildRemoved, onValue, ref } from 'firebase/database';
import { useContext, useEffect, useState } from "react";
import { db } from "../../config/firebase";
import AppContext from "../../context/AuthContext";
import { authToken } from "../../services/generateToken";
import IncomingCallModal from '../IncomingCall/IncomingCall';
import MeetingView from '../Meeting/MeetingView';
import NavbarContent from "./NavBarContent";

export default function NavBar() {
    const [requestedCall, setRequestedCall] = useState(null);
    const [meetingId, setMeetingId] = useState(null);
    const [callRequested, setCallRequested] = useState(false);
    const { userData } = useContext(AppContext);
    const [notificationCount, setNotificationCount] = useState(0);

    const onMeetingLeave = () => {
        setMeetingId(null);
    };

    const setMeetingIdCallback = (id) => {
        setMeetingId(id);
    };

    const closeModal = () => {
        setCallRequested(false);
    };

    useEffect(() => {
        if (userData) {
            const dbRef = ref(db, `users/${userData.handle}/requestedCall`);
            const unsubscribe = onValue(dbRef, (snapshot) => {
                const requestedCall = snapshot.val();
                setRequestedCall(requestedCall);
                setCallRequested(true);
            });

            return () => off(dbRef, unsubscribe);
        }


    }, [userData]);

    return (
        <Box position="sticky" top="0" zIndex="sticky">
            <NavbarContent />
            {callRequested && requestedCall && requestedCall !== 'none' && (
                <IncomingCallModal
                    setCallRequested={closeModal}
                    setMeetingId={setMeetingIdCallback}
                    data={Object.values(requestedCall)}
                />
            )}
            {meetingId && authToken && (
                <MeetingProvider
                    config={{
                        meetingId,
                        micEnabled: true,
                        webcamEnabled: true,
                        name: userData.handle,
                    }}
                    token={authToken}
                >
                    <MeetingView meetingId={meetingId} onMeetingLeave={onMeetingLeave} />
                </MeetingProvider>
            )}
        </Box>
    );
}


