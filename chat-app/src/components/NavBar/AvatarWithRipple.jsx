import {
    Box,
    Flex,
    Avatar,
    AvatarBadge,
} from '@chakra-ui/react';
import { LoadingOutlined } from '@ant-design/icons';
import { keyframes } from '@chakra-ui/react';
import { useContext } from 'react';
import AppContext from '../../context/AuthContext';

function AvatarWithRipple() {
    const { userData } = useContext(AppContext);

    // const size = '50px';
    const color = 'white';

    const pulseRing = keyframes`
    0% {
      transform: scale(0.33);
    }
    40%,
    50% {
      opacity: 0;
    }
    100% {
      opacity: 0;
    }
  `;
    if (!userData) {
        return (
            <div>
                <LoadingOutlined />
            </div>
        )
    }
    return (
        <Flex
            position="relative"
            alignItems="center"
            justifyContent="center"
            h={{base: "30px", sm:"35px", md: "40px", lg:"50px"}}
            w={{base: "30px", sm:"35px", md: "40px", lg:"50px"}}
        >
            <Box
                ml="5px"
                as="div"
                position="absolute"
                top={0}
                w="100%"
                h="100%"
                _before={{
                    content: "''",
                    position: 'absolute',
                    display: 'block',
                    width: '300%',
                    height: '300%',
                    boxSizing: 'border-box',
                    marginLeft: '-100%',
                    marginTop: '-100%',
                    borderRadius: '50%',
                    bgColor: color,
                    animation: `2.25s ${pulseRing} cubic-bezier(0.455, 0.03, 0.515, 0.955) -0.4s infinite`,
                }}
            />
            <Avatar border={"1.5px solid white"} ml="5px" src={userData.profileImageUrl} size="full" >
            <AvatarBadge borderColor={"green.200"} boxSize='2.25em' bg='green.500' />
            </Avatar>
        </Flex>
    );
}
export default AvatarWithRipple;