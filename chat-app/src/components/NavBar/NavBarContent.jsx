import {
  MoreOutlined
} from "@ant-design/icons";
import {
  Box,
  Button,
  Center,
  Divider,
  Flex,
  HStack,
  Image,
  Link,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Portal,
  Spacer,
  Stack,
  Tooltip,
  useBreakpointValue,
  useColorMode,
  useDisclosure
} from "@chakra-ui/react";
import { signOut } from "firebase/auth";
import { off, onChildAdded, onChildRemoved, ref } from 'firebase/database';
import { useContext, useEffect, useState } from "react";
import {
  BsDoorOpen,
  BsGear,
  BsMoon,
  BsSun
} from "react-icons/bs";
import { VscBell, VscBellDot } from "react-icons/vsc";
import { useNavigate } from "react-router-dom";
import { CALENDAR_PAGE, CALLS_PAGE, CHANNEL_PAGE, FRIENDS_PAGE, FUN_PAGE, HOME_PAGE, LOG_IN_PAGE, PROFILE_PAGE } from "../../common/constants";
import { auth, db } from "../../config/firebase";
import AppContext from "../../context/AuthContext";
import GroupChat from "../GroupChat/GroupChat";
import Notifications from "../Notifications/Notifications";
import AvatarWithRipple from "./AvatarWithRipple";
import NavBarLinks from "./NavBarLinks";

const NavbarContent = () => {
  const [notificationCount, setNotificationCount] = useState(0);
  const [isGroupChatOpen, setGroupChatOpen] = useState(false);
  const [isNotificationOpen, setNotificationOpen] = useState(false);
  const { userData } = useContext(AppContext);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { colorMode, toggleColorMode } = useColorMode();
  const navigate = useNavigate();

  useEffect(() => {
    if (userData) {
      const notificationsRef = ref(db, "notifications");
      const onNotificationAdded = onChildAdded(notificationsRef, (snapshot) => {
        const notification = snapshot.val();
        if (notification.idUser === userData.uid) {
          setNotificationCount((prevCount) => prevCount + 1);
        }
      });
      const onNotificationRemoved = onChildRemoved(
        notificationsRef,
        (snapshot) => {
          const notification = snapshot.val();
          if (notification.idUser === userData.uid) {
            setNotificationCount((prevCount) => prevCount - 1);
          }
        }
      );
      return () => {
        off(notificationsRef, "child_added", onNotificationAdded);
        off(notificationsRef, "child_removed", onNotificationRemoved);
      };
    }
    if (userData) {
      const notificationsRef = ref(db, "notifications");
      const onNotificationAdded = onChildAdded(notificationsRef, (snapshot) => {
        const notification = snapshot.val();
        console.log(notification);
        if (notification.idUser === userData.uid) {
          setNotificationCount((prevCount) => prevCount + 1);
        }
      });
      const onNotificationRemoved = onChildRemoved(
        notificationsRef,
        (snapshot) => {
          const notification = snapshot.val();
          console.log(notification);
          if (notification.idUser === userData.uid) {
            setNotificationCount((prevCount) => prevCount - 1);
          }
        }
      );

      return () => {
        off(notificationsRef, "child_added", onNotificationAdded);
        off(notificationsRef, "child_removed", onNotificationRemoved);
      };
    }
  }, [userData]);

  const handleIconClick = (icon) => {
    //console.log(icon);
    if (icon === "Calls") {
      return navigate(`${CALLS_PAGE}`);
    }
    if (icon === "Chats") {
      return navigate(`${FRIENDS_PAGE}`);
    }
    if (icon === "Team") {
      return navigate(`${CHANNEL_PAGE}`);
    }
    if (icon === 'Create Group Chat') {
      setGroupChatOpen(true);
      onOpen();
    }
    if (icon === "Notifications") {
      setNotificationOpen(true);
      onOpen();
    }
    if (icon === 'Fun') {
      return navigate(`${FUN_PAGE}`);
    }

    if (icon === 'Calendar') {
      return navigate(`${CALENDAR_PAGE}`)
    }
  }

  const closeModals = () => {
    setGroupChatOpen(false);
    setNotificationOpen(false);
    onClose();
  };

  const logoutUser = () => {
    navigate(`${LOG_IN_PAGE}`)
    return signOut(auth);
  }
  const handleClick = () => {
    onOpen();
    console.log('click');
  }
  return (
    <Flex direction={{ base: 'column', sm: "row", md: "row", lg: "row", xl: 'row' }}>

      <Stack
        direction={useBreakpointValue({ base: 'row', xl: 'column' })}
        w={{ base: '100%', xl: '100px' }}
        bg="#00B998"
        {...stackStyles}
        // borderRadius={{ base: '0 0 10px 10px', xl: '0px 10px 10px 0px' }}
        p={{ base: 0, xl: 6 }}
        h={{ base: "70px", xl: '100vh' }}
        alignItems="center"
        spacing={4}
        boxShadow="0 10px 15px rgba(0, 185, 152, 0.4)"
        overflowY={{base: "hidden", xl:"auto"}}
        overflowX="hidden"
      >
        {useBreakpointValue({
          base:
            <Box >
              <Popover zIndex={"999"}>
                <PopoverTrigger>
                  <Button ml={"5px"} borderRadius={"25px"} fontSize={"20px"} bg={"transparent"} onClick={handleClick}><PopoverTrigger><MoreOutlined /></PopoverTrigger> </Button>
                </PopoverTrigger>
                <Portal >
                  <PopoverContent zIndex={"999"} w="full" mt="15px">
                    <PopoverArrow />
                    <PopoverHeader>Menu</PopoverHeader>
                    <PopoverCloseButton />
                    <PopoverBody >
                      <HStack zIndex={"999"}>
                        <NavBarLinks
                          handleIconClick={handleIconClick}
                          toggleColorMode={toggleColorMode}
                          colorMode={colorMode}
                          onOpen={onOpen}
                          displayAttribute={{ base: 'block', md: 'none' }}
                        />
                      </HStack>
                    </PopoverBody>
                  </PopoverContent>
                </Portal>
              </Popover>
            </Box>, md: null
        })}
        <Box display={{ base: "none", md: "block" }} w={{ base: "none", md: "5%", lg: "4%", xl: "85%" }} className="Logo" ml={"7px"} onClick={() => navigate(`${HOME_PAGE}`)} cursor={"pointer"}>
          <Center>
            <Image src='https://i.ibb.co/bv5NWRr/White-Chatting-Letter-Initial-Logo-13.png' />
          </Center>
        </Box>
        <Flex
          display={{ base: "none", sm: "block" }}
          alignItems={"center"} cursor={"pointer"} onClick={() => navigate(`${PROFILE_PAGE}`)}>
          <AvatarWithRipple />
        </Flex>
        <Stack direction={useBreakpointValue({ base: 'row', xl: 'column' })}
          spacing={4} as={"nav"}>
          <Divider orientation="horizontal" />
          {notificationCount ? (
            <Tooltip label="Notifications">
              <Button
                // size={{base:"sm"}}
                mt={{ base: "5pxr", xl: "0px", "2xl": "20px" }}
                // h={{base: "30px", sm:"35px", md: "40px", lg:"50px"}}
                // w={{base: "30px", sm:"35px", md: "40px", lg:"50px"}}
                p="10px"
                borderRadius={"25px"}
                display="flex"
                alignItems="center"
                color={"gray.500"}
                bg={'#34C7AD'}
                // _hover={{ color: "red" }}
                onClick={() => handleIconClick('Notifications')}
              >
                <VscBellDot style={{ fontSize: "20px", color: 'red' }} />
              </Button>
            </Tooltip>
          ) : (
            <Tooltip label="Notifications">
              <Button
                mt={{ base: "5pxr", xl: "0px", "2xl": "20px" }}
                p={{ base: "0", md: "10px" }}
                borderRadius={"25px"}
                display="flex"
                alignItems="center"
                color={"gray.500"}
                bg={'#34C7AD'}
                // _hover={{ color: "red" }}
                onClick={() => handleIconClick('Notifications')}
              >
                <VscBell style={{ fontSize: "20px", color: 'white' }} />
              </Button>
            </Tooltip>
          )}
          <NavBarLinks
            handleIconClick={handleIconClick}
            toggleColorMode={toggleColorMode}
            colorMode={colorMode}
            onOpen={onOpen}
            displayAttribute={{ base: 'none', md: 'block' }}
          />
        </Stack>
        <Spacer />
        <Stack pr={{ base: "20px", md: "10px", xl: "0px" }} direction={useBreakpointValue({ base: 'row', xl: 'column' })}
          spacing={4}>
          <Tooltip label="Profile" placement="right">

            <Button
              p="10px"
              borderRadius={"25px"}
              color={"gray.500"}
              bg={'#34C7AD'}
              onClick={() => navigate(`${PROFILE_PAGE}`)}
            >
              <BsGear
                _hover={{ color: "#49cce6" }}
                style={{ fontSize: "20px", color: 'white', borderRadius: "25px", backGround: '#34C7AD' }}
              />
            </Button>
          </Tooltip>
          <Box>
            <Tooltip label="Log out" placement="right">
              <Link
                p="10px"
                borderRadius={"25px"}
                display="flex"
                alignItems="center"
                _hover={{ color: "white" }}
                color="red"
                bg={'#34C7AD'}
              >
                <BsDoorOpen onClick={logoutUser} style={{ fontSize: "20px" }} />
              </Link>
            </Tooltip>
          </Box>
          <Box display={{ base: "none", md: "block" }} pr={{ sm: "20px", md: "20px", lg:"20px", xl:"0px"}}>
            <Tooltip label="Who uses light mode !?" placement="right">

              <Button
                p="10px"
                borderRadius={"25px"}
                bg={'#34C7AD'}
                color={"white"}
                onClick={toggleColorMode}>
                {colorMode === 'light' ? <BsMoon /> : <BsSun />}
              </Button>
            </Tooltip>
          </Box>
          {isNotificationOpen && (
            <Notifications isOpen={isOpen} onClose={closeModals} />
          )}
          {isGroupChatOpen && (
            <GroupChat isOpen={isOpen} onClose={closeModals} />
          )}
        </Stack>
      </Stack>
    </Flex>
  )
}

const stackStyles = {
  sx: {
    "&::-webkit-scrollbar": {
      width: "0px",
    },
    "&::-webkit-scrollbar-thumb": {
      background: "gray.700",
      borderRadius: "10px",
    },
    "&:hover::-webkit-scrollbar": {
      width: "5px",
    },
  },
}
export default NavbarContent;