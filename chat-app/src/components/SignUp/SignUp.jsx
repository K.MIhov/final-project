import {
    Box,
    Button,
    FormControl,
    FormLabel,
    Input,
    Spacer,
    Heading,
    Select,
    Link,
    Tooltip,
} from "@chakra-ui/react";
import { ArrowBackIcon } from "@chakra-ui/icons";

import AppContext from "../../context/AuthContext";
import { getUserByHandle, createUserHandle } from "../../services/users.service";
import { registerUser } from "../../services/auth.service";
import { update, ref } from "firebase/database";
import { auth, db } from "../../config/firebase";


import { useNavigate } from "react-router-dom";
import { useState, useContext } from "react";
import { useToast } from "@chakra-ui/react";
import { toasted, formControl, initialForm, LOG_IN_PAGE } from "../../common/constants";

const SignUp = () => {

    return (
        <Box
            backgroundImage="url('https://i.ibb.co/gTnDpf0/Untitled-design-3.png')"
            backgroundPosition="center"
            backgroundSize="cover"
            backgroundRepeat="no-repeat"
            minHeight="100vh"
            display="flex"
            justifyContent="center"
            alignItems="center"
        >
            <SignUpForm />
        </Box>
    )
}



const SignUpForm = () => {
    const [form, setForm] = useState(initialForm);
    const { setUser } = useContext(AppContext);
    const navigate = useNavigate();
    const toast = useToast();

    const onRegister = () => {
        const emailRegex = /\S+@\S+\.\S+/;
        if (!emailRegex.test(form.email) || !form.email) {
            toast(toasted[0]);
            return;
        }
        if (form.handle < 4 || form.handle > 32) {
            toast(toasted[1]);
            return;
        }
        if (!form.displayName) {
            toast(toasted[2]);
            return;
        }
        if (form.password.length < 7 || form.password.length > 64) {
            toast(toasted[3]);
            return;
        }

        getUserByHandle(form.handle)
            .then(snapshot => {
                if (snapshot.exists()) {
                    toast(toasted[4])
                    throw new Error('username taken')
                }
                return registerUser(form.email, form.password);
            })
            .then(credential => {
                return createUserHandle(form.handle, credential.user.uid, form.displayName, form.phoneNumber, credential.user.email, form.isAdmin, form.isBlocked, form.profileImageUrl, form.userStatus, 'none', 'none')
                    .then(() => {
                        setUser({
                            user: credential.user,
                        });
                    });
            })
            .then(() => {
                navigate(`/log-in`);
            })
            .catch(e => console.log(e));
    }

    const updateForm = prop => e => {
        setForm({
            ...form,
            [prop]: e.target.value,
        });
    };

    return (
        <Box
            bg="gray.800"
            boxShadow="md"
            p="4"
            borderRadius="md"
            h="780px"
            w="30%"
        >
            <Tooltip label="Back">
                <ArrowBackIcon onClick={() => navigate(`${LOG_IN_PAGE}`)} boxSize={6} cursor="pointer" />
            </Tooltip>
            <Heading textAlign="center">Sign Up</Heading>
            <Spacer />
            <br />
            <Box
                bg="gray.900"
                boxShadow="md"
                p="0px 40px 0px 40px"
                borderRadius="md"
                h="72%"
                w="100%"
            >
                <Box w="100%" p={8} mt={0} textAlign="center">
                    {formControl.map((forms) => (
                        <FormControl pb="5px" key={forms.id} isRequired id={forms.id}>
                            <FormLabel>{forms.input}</FormLabel>
                            <Input value={form[forms.id]} type={forms.id} name={forms.id} onChange={updateForm(forms.id)} />
                        </FormControl>
                    ))}
                    <FormControl id="country" >
                        <FormLabel>Country</FormLabel>
                        <Select placeholder='Select country' name="country">
                            <option value="us">United States</option>
                            <option value="uk">United Kingdom</option>
                            <option value="bg">Bulgaria</option>
                            <option value="ca">Canada</option>
                        </Select>
                    </FormControl>
                    <Button mt="80px" mb="15px" bg="blue.400" onClick={onRegister}>
                        Sign Up
                    </Button>
                    <Link
                        display="flex"
                        justifyContent="center"
                    >
                        Terms of Use and Privacy Policy
                    </Link>
                </Box>
            </Box>
        </Box>
    );
}
export default SignUp;