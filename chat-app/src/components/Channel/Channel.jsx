import {
  Grid, GridItem
} from "@chakra-ui/layout";
import {
  useColorModeValue,
} from '@chakra-ui/react';
import NavBar from "../NavBar/NavBar";
import ChannelView from "./ChannelView";
import { ref, onChildAdded, off } from "firebase/database";
import { db } from "../../config/firebase";

import { useContext, useEffect, useState } from "react";
import AppContext from "../../context/AuthContext";

const Channel = () => {
  const [allChannels, setAllChannels] = useState([])
  const { userData } = useContext(AppContext)

  useEffect(() => {
    if (userData) {
      const channelsRef = ref(db, 'channels');

      const onChannelAdded = onChildAdded(channelsRef, (snapshot) => {
        const channel = snapshot.val();
        const key = snapshot.key;

        const transformedChannel = {
          ...channel,
          id: key,
          createdOn: new Date(channel.createdOn),
        };

        setAllChannels((prevChannels) => {
          const channelExists = prevChannels.some((c) => c.id === transformedChannel.id);

          if (!channelExists) {
            return [...prevChannels, transformedChannel];
          }
          return prevChannels;
        });
      });

      return () => {
        off(channelsRef, 'child_added', onChannelAdded);
      };
    }
  }, [userData?.uid]);

  return (
    <>
      <Grid
      templateColumns={{ base: "1fr", xl: "5% 95%", "2xl": "5% 95%" }}
      bg={useColorModeValue("gray.50", "#292F3F")} 
      // maxH={'auto'}
      >
        <GridItem>
          <NavBar />
        </GridItem>
        <GridItem
        p={"0px 50px"}
        bg={useColorModeValue("gray.50", "#292F3F")}
        >
          <ChannelView allChannels={allChannels} />
        </GridItem>
      </Grid>
    </>
  )
}

export default Channel;