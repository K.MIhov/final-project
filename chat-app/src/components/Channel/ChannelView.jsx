import { Grid, Container, Text } from "@chakra-ui/layout";
import GridChild from "../GridChild/GridChild";
import { useState, useEffect, useContext, useMemo } from "react";
import { useNavigate } from 'react-router-dom';
import AppContext from "../../context/AuthContext";
import PropTypes from 'prop-types';
import { useBreakpointValue } from "@chakra-ui/react";

const ChannelView = ({ allChannels }) => {
const [channels, setChannels] = useState([]);
const navigate = useNavigate();
const { userData } = useContext(AppContext);


const filteredChannels = useMemo(() => {
  if (userData) {
    return allChannels.filter(
      (channel) => Object.values(channel.team.listOfParticipants).includes(userData?.uid) &&
        Object.values(channel.team.listOfParticipants).length > 2
    );
  }
  return [];
}, [allChannels, userData]);

useEffect(() => {
  setChannels(filteredChannels);
}, [filteredChannels]);

const gridTemplateColumns = useBreakpointValue({
  base: "1fr",
  sm: "repeat(2, 1fr)",
  md: "repeat(3, 1fr)",
  xl: "repeat(4, 1fr)",
  '2xl': "repeat(4, 1fr)",
});


const specificRoom = (id) => {
  navigate(`/channel/${id}`)
}

  return (
    <Container
     maxW={"full"} h={"100vh"} p={"0"} m={"0"}
    >
      <Text
      fontSize={"20px"}
      fontWeight={"bold"}
      m={"20px 0px"}
      >
        CHANNELS
      </Text>
      <Grid
        templateColumns={channels.length ? gridTemplateColumns : '1fr'}
        // templateColumns={channels.length ? 'repeat(4, 1fr)' : '1fr'}
        gap={6}
        // maxH={"100vh"}
        // overflowY={"auto"}
        // sx={{
        //   "&::-webkit-scrollbar": {
        //     width: "2px",
        //   },
        //   "&::-webkit-scrollbar-thumb": {
        //     background: "#00B998",
        //     borderRadius: "10px",
        //   }
        // }}
        >
        {userData && <GridChild 
        channels={channels} 
        specificRoom={specificRoom} 
        />}
      </Grid>
    </Container>
  )
}

ChannelView.propTypes = {
  allChannels: PropTypes.array.isRequired,
};

export default ChannelView;