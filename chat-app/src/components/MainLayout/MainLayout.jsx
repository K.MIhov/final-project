// import { useState, useEffect, useContext, useMemo, useCallback } from "react";
// import Chat from "../Chat/Chat";
// import SidebarContacts from "../SidebarContacts/SidebarContacts";
// import NavBar from "../NavBar/NavBar";
// import TeamInfo from "../Chat/TeamInfo";
// import { Grid, useBreakpointValue } from "@chakra-ui/react";
// import { useParams } from "react-router";
// import { getAllChannels } from "../../services/channels.service";
// import AppContext from "../../context/AuthContext";
// import { getAllUsers } from "../../services/users.service";

// const MainLayout = () => {
//   const [isDrawerOpen, setIsDrawerOpen] = useState(true);
//   const [selectedChat, setSelectedChat] = useState(null);
//   const [selectedChannel, setSelectedChannel] = useState(null);
//   const [users, setUsers] = useState(null);
//   const { userData } = useContext(AppContext);
//   const { id } = useParams();

//   const filteredChannels = useMemo(() => {
//     return getAllChannels().then((res) => res.filter((channel) => channel.id === id));
//   }, [id]);

//   useEffect(() => {
//     if (userData) {
//       filteredChannels.then((filterChannels) => {
//         setSelectedChannel(Object.values(filterChannels)[0]);
//         setSelectedChat(Object.values(filterChannels[0]?.chat)[0]);
//       });
//     }
//   }, [userData, filteredChannels]);

//   useEffect(() => {
//     getAllUsers()
//       .then((snapshot) => {
//         const allUsers = new Map();
//         snapshot.forEach((doc) => {
//           const userId = doc.uid ? doc.uid : '';
//           if (userId) {
//             allUsers.set(userId, doc);
//           }
//         });
//         setUsers(allUsers);
//       })
//       .catch((err) => console.error(err));
//   }, [userData?.uid]);

//   const memoizedUsers = useMemo(() => users, [users]);

//   const breakpoint = useBreakpointValue({ base: "base", md: "md", lg: "lg" });

//   useEffect(() => {
//     setIsDrawerOpen(breakpoint !== "base" && breakpoint !== "md" && breakpoint !== "lg");
//   }, [breakpoint]);

//   const toggleDrawer = useCallback(() => {
//     setIsDrawerOpen((prevState) => !prevState);
//   }, []);

//   const handleChatClick = useCallback((chat, channel) => {
//     setSelectedChannel(channel);
//     setSelectedChat(chat);
//   }, []);

//   const gridTemplateColumns = useBreakpointValue({
//     base: "1fr",
//     lg: "7% 23% 70%",
//     xl: isDrawerOpen ? "7% 21.5% 50% 21.5%" : "7% 23% 70%",
//     "2xl": isDrawerOpen ? "7% 21.5% 50% 21.5%" : "7% 23% 70%",
//   });

//   return (
//     <Grid templateColumns={gridTemplateColumns}>
//       <NavBar />
//       {selectedChat && (
//         <SidebarContacts onChatClick={handleChatClick} id={id} selectedChat={selectedChat} />
//       )}
//       {memoizedUsers && selectedChat && selectedChannel && (
//         <Chat toggle={toggleDrawer} selectedChat={selectedChat} selectedChannel={selectedChannel} users={memoizedUsers} />
//       )}
//       {isDrawerOpen && selectedChannel && <TeamInfo selectedChannel={selectedChannel} />}
//     </Grid>
//   );
// };

// export default MainLayout;

import { useState, useEffect, useContext, useMemo, useCallback } from "react";
import Chat from "../Chat/Chat";
import SidebarContacts from "../SidebarContacts/SidebarContacts";
import NavBar from "../NavBar/NavBar";
import TeamInfo from "../Chat/TeamInfo";
import { Grid, useBreakpointValue, Collapse} from "@chakra-ui/react";
import { useParams, useNavigate } from "react-router";
import { getAllChannels } from "../../services/channels.service";
import AppContext from "../../context/AuthContext";
import { getAllUsers } from "../../services/users.service";

const MainLayout = () => {
  const [switchDisplay, setSwitchDisplay] = useState("DMs");
  const [isTeamInfoOpen, setIsTeamInfoOpen] = useState(false);
  const [selectedChat, setSelectedChat] = useState(null);
  const [selectedChannel, setSelectedChannel] = useState(null);
  const [users, setUsers] = useState(null);
  const { userData } = useContext(AppContext);
  const { id, chatId } = useParams();
  console.log(id)
  console.log(chatId)
  const navigate = useNavigate();

  const filteredChannels = useMemo(() => {
    return getAllChannels().then((res) => res.filter((channel) => channel.id === id));
  }, [id]);

  useEffect(() => {
    if (userData) {
      filteredChannels.then((filterChannels) => {
        setSelectedChannel(Object.values(filterChannels)[0]);
        setSelectedChat(Object.values(filterChannels[0]?.chat)[0]);
        if (Object.values(filterChannels[0]?.chat)[0]?.id) {
          navigate(`/channel/${id}/${Object.values(filterChannels[0]?.chat)[0]?.id}`);
        }
      });
    }
  }, [userData, filteredChannels]);

  useEffect(() => {
    getAllUsers()
      .then((snapshot) => {
        const allUsers = new Map();
        snapshot.forEach((doc) => {
          const userId = doc.uid ? doc.uid : "";
          if (userId) {
            allUsers.set(userId, doc);
          }
        });
        setUsers(allUsers);
      })
      .catch((err) => console.error(err));
  }, [userData?.uid]);

  const memoizedUsers = useMemo(() => users, [users]);

  const breakpoint = useBreakpointValue({ base: "base", sm:"sm", md: "md", lg: "lg", xl:'xl', '2xl':'2xl' });

  // const toggleSidebar = useCallback(() => {
  //   setIsSidebarOpen((prevState) => !prevState);
  //   onOpen()
  // }, []);

  const toggleTeamInfo = useCallback(() => {
    console.log('hello')
    setIsTeamInfoOpen((prevState) => !prevState);
  }, []);

  const handleChatClick = useCallback((chat, channel) => {
    setSelectedChannel(channel);
    setSelectedChat(chat);
    setSwitchDisplay('chat');
    console.log(selectedChat)
    console.log(selectedChannel)
    if (breakpoint === "lg" || breakpoint === "base") {
      setIsTeamInfoOpen((prevState) => !prevState);
    }
    if (chat.id) {
      navigate(`/channel/${id}/${chat.id}`);
    }
  }, [breakpoint, id]);

  const gridTemplateColumns = useBreakpointValue({
    base: "1fr",
    lg: "1fr",
    xl: isTeamInfoOpen ? "7% 21.5% 50% 21.5%" : "7% 23% 70%",
    "2xl": isTeamInfoOpen ? "7% 21.5% 50% 21.5%" : "7% 23% 70%",
  });

  return (
    <Grid templateColumns={gridTemplateColumns}>
      <NavBar />
      {console.log(breakpoint)}
      {memoizedUsers && selectedChat && selectedChannel && (<SidebarContacts
      onChatClick={handleChatClick}
      id={id}
      chatId={chatId}
      selectedChat={selectedChat}
      switchDisplay={switchDisplay}
      setSwitchDisplay={setSwitchDisplay}
      />)}
      {memoizedUsers && selectedChat && selectedChannel && (
        <Chat
          toggleTeamInfo={toggleTeamInfo}
          // toggleSidebar={toggleSidebar}
          selectedChat={selectedChat}
          selectedChannel={selectedChannel}
          users={memoizedUsers}
          switchDisplay={switchDisplay}
          setSwitchDisplay={setSwitchDisplay}
        />
      )}
      <Collapse in={isTeamInfoOpen} animateOpacity>
        {isTeamInfoOpen && selectedChannel &&  (breakpoint === 'xl' || breakpoint === '2xl' ) && (
          <TeamInfo selectedChannel={selectedChannel} toggleTeamInfo={toggleTeamInfo}/>
        )}
      </Collapse>
    </Grid>
  );
};

export default MainLayout;