import {
  Avatar,
  Box,
  Flex,
  Text,
  Stack,
  Button,
  useColorModeValue,
  Spacer,
  HStack,
} from '@chakra-ui/react';
import { db } from '../../config/firebase';
import { useContext, useState, useEffect } from 'react';
import AppContext from '../../context/AuthContext';
import { ref as ref2, remove } from 'firebase/database';
import { useNavigate } from 'react-router-dom';
import EditData from './EditData';
import { ref, onValue, off, update } from 'firebase/database';
import { LOG_IN_PAGE } from '../../common/constants';
import AccountRemoval from './AccountRemoval';
import PropTypes from 'prop-types';

const MyAccountCard = ({ setCurrentSettings }) => {
  const [showDataChange, setShowDataChange] = useState(false);
  const [currentChange, setCurrentChange] = useState([]);
  const [profilePhoto, setProfilePhoto] = useState(null);
  const [displayName, setDisplayName] = useState(null);
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [email, setEmail] = useState(null);
  const { userData } = useContext(AppContext)
  const navigate = useNavigate();

  const handleDeleteProfile = () => {
    remove(ref2(db, `users/${userData.handle}`))
      .then(() => alert('account removed'))
      .then(() => navigate(`${LOG_IN_PAGE}`))
      .catch((err) => console.error(err));
    setShowDataChange(false);
  };

  const handleDisableProfile = () => {
    update(ref2(db, `disabledUsers/`), userData)
      .then(() => console.log('moved'))
      .then(() => handleDeleteProfile())
      .catch((err) => console.log(err))
  }
  const handleDataChange = (value, label) => {
    setCurrentChange([value, label]);
    setShowDataChange(true);
  }
  useEffect(() => {
    if (userData) {
      const emailRef = ref(db, `users/${userData.handle}/email`);
      const onEmailChange = onValue(emailRef, (snapshot) => {
        setEmail(snapshot.val());
      });

      const displayNameRef = ref(db, `users/${userData.handle}/displayName`);
      const onDisplayNameChange = onValue(displayNameRef, (snapshot) => {
        setDisplayName(snapshot.val())
      });
      const profileImageUrlRef = ref(db, `users/${userData.handle}/profileImageUrl`);
      const onProfileImageUrlChange = onValue(profileImageUrlRef, (snapshot) => {
        setProfilePhoto(snapshot.val())
      });
      const phoneNumberRef = ref(db, `users/${userData.handle}/phoneNumber`);
      const onPhoneNumberChange = onValue(phoneNumberRef, (snapshot) => {
        setPhoneNumber(snapshot.val())
      });

      return () => {
        off(emailRef, onEmailChange);
        off(displayNameRef, onDisplayNameChange);
        off(profileImageUrlRef, onProfileImageUrlChange);
        off(phoneNumberRef, onPhoneNumberChange);

      }
    }
  }, [userData?.phoneNumber, userData?.displayName, userData?.profileImageUrl, userData?.email]);

  return (
    <>
      <Box {...boxStyles} bg={useColorModeValue('white', 'gray.800')}>
        <Box h={'120px'} w={'full'} bg="#00B998" objectFit={'cover'} />
        <Flex justify={'center'} mt={-12}>
          {userData && (
            <Avatar
              size={'xl'}
              src={profilePhoto}
              alt={'Author'}
              css={{
                border: '2px solid #00B998',
              }}
            />
          )}
        </Flex>
        <Stack m="5px">
          <Button onClick={() => setCurrentSettings('Profile')} {...buttonStyles}>
            Edit Profile
          </Button>
        </Stack>
        <Box p={6}>
          {userData && (
            <>
              <Text {...textStyles}>Display Name</Text>
              <HStack>
                <Text {...hStackStyles}>{displayName}</Text>
                <Spacer />
                <Button onClick={() => handleDataChange('displayName', 'Display Name')} {...buttonStyles}>
                  Edit
                </Button>
              </HStack>
              <Text {...textStyles}>Email</Text>
              <HStack>
                <Text {...hStackStyles}>{email}</Text>
                <Spacer />
                <Button onClick={() => handleDataChange('email', 'Email')} {...buttonStyles}>
                  Edit
                </Button>
              </HStack>
              <Text {...textStyles}>Phone Number</Text>
              <HStack>
                <Text {...hStackStyles}>{phoneNumber}</Text>
                <Spacer />
                <Button onClick={() => handleDataChange('phoneNumber', 'Phone Number')} {...buttonStyles}>
                  Edit
                </Button>
              </HStack>
            </>
          )}
          <Button onClick={() => handleDataChange('password', 'Password')} {...buttonStyles}>
            Change Password
          </Button>
        </Box>
      </Box>
      <AccountRemoval setDelete={handleDeleteProfile} setDisable={handleDisableProfile} />
      {showDataChange && <EditData currentChange={currentChange} setShowDataChange={setShowDataChange} />}
    </>
  )
}

MyAccountCard.propTypes = {
  setCurrentSettings: PropTypes.func.isRequired
}
export default MyAccountCard;

const boxStyles = {
  mt: '20px',
  maxW: '400px',
  w: 'full',
  boxShadow: '2xl',
  rounded: 'md',
  overflow: 'hidden',
};

const buttonStyles = {
  mt: '20px',
  bg: '#00B998',
  color: 'white',
};

const textStyles = {
  fontWeight: 'bold',
  color: 'gray.500',
};

const hStackStyles = {
  fontWeight: 'bold',
};



