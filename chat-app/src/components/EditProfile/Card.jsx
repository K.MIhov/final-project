import {
  Avatar,
  Box,
  Button,
  Collapse,
  Flex,
  Heading,
  Stack,
  Text,
  useColorModeValue
} from '@chakra-ui/react';
import { off, onValue, ref } from 'firebase/database';
import { useContext, useEffect, useState } from 'react';
import { db } from '../../config/firebase';
import AppContext from '../../context/AuthContext';
import PropTypes from 'prop-types';

export default function SocialProfileWithImage({ setCurrentSettings }) {
  const { userData } = useContext(AppContext)
  const [show, setShow] = useState(false)
  const [about, setAbout] = useState(null);
  const [displayName, setDisplayName] = useState(null)
  const [profilePhoto, setProfilePhoto] = useState(null)
  const handleToggle = () => setShow(!show)

  useEffect(() => {
    if (userData) {
      const aboutRef = ref(db, `users/${userData.handle}/about`);
      const onAboutChanged = onValue(aboutRef, (snapshot) => {
        setAbout(snapshot.val());
      });

      const displayNameRef = ref(db, `users/${userData.handle}/displayName`);
      const onDisplayNameChanged = onValue(displayNameRef, (snapshot) => {
        setDisplayName(snapshot.val())
      });
      const profileImageUrlRef = ref(db, `users/${userData.handle}/profileImageUrl`);
      const onProfileImageUrlChange = onValue(profileImageUrlRef, (snapshot) => {
        setProfilePhoto(snapshot.val())
      });

      return () => {
        off(aboutRef, onAboutChanged);
        off(displayNameRef, onDisplayNameChanged);
        off(profileImageUrlRef, onProfileImageUrlChange);
      }
    }
  }, [userData?.about, userData?.displayName, userData?.profileImageUrl]);

  return (
    <Box {...boxStyles} bg={useColorModeValue('white', 'gray.800')}>
      <Box {...imageStyles} />
      <Flex justify='center' mt={-12}>
        {userData && (
          <Avatar {...avatarStyles} src={profilePhoto} />
        )}
      </Flex>
      <Box p={6}>
        <Stack spacing={0} align='center' mb={5}>
          {userData && (
            <Heading {...headingStyles}>
              {displayName}
            </Heading>
          )}
          {userData && (
            <Text color='gray.500'>{userData.email}</Text>
          )}
        </Stack>
        <Stack>
          {userData && userData.about && (
            <Box textAlign={"center"}>
              <Collapse startingHeight={30} in={show}>
                {about}
              </Collapse>
              <Button onClick={handleToggle} {...buttonStyles}>
                Show {show ? 'Less' : 'More'}
              </Button>
            </Box>
          )}
        </Stack>
        <Stack direction='row' justify='center' spacing={6}></Stack>
        <Button
          onClick={() => setCurrentSettings('My Account')}
          w='full'
          mt={8}
          bg="#00B998"
          color='white'
          rounded='md'
          _hover={{
            transform: 'translateY(-2px)',
            boxShadow: 'lg',
          }}
        >
          Edit Account
        </Button>
      </Box>
    </Box>
  )
}


SocialProfileWithImage.propTypes = {
  setCurrentSettings: PropTypes.func.isRequired,
};

const boxStyles = {
  maxW: '400px',
  w: 'full',

  boxShadow: '2xl',
  rounded: 'md',
  overflow: 'hidden',
};

const imageStyles = {
  h: '120px',
  w: 'full',
  bg: "#00B998",
  objectFit: 'cover',
};

const avatarStyles = {
  size: 'xl',

  alt: 'Author',
  css: {
    border: '2px solid #00B998',
  },
};

const headingStyles = {
  fontSize: '2xl',
  fontWeight: 500,
  fontFamily: 'body',
};

const buttonStyles = {
  size: 'sm',
  mt: '1rem',
};

