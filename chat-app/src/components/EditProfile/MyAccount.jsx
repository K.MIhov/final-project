import {
  Box,
  Heading,
  Divider,
} from '@chakra-ui/react';
import MyAccountCard from './MyAccountCard';
import PropTypes from 'prop-types';

const MyAccount = ({ setCurrentSettings }) => {
  return (
    <Box>
      <Box>
        <Box ml={{ base: 0, md: 60 }} p="4">
          <Box>
            <Heading>My Account</Heading>
            <Divider orientation="horizontal" mt="20px" />
            <MyAccountCard setCurrentSettings={setCurrentSettings}  />
           
          </Box>
        </Box>
      </Box>
    </Box>
  )
};

MyAccount.propTypes = {
  setCurrentSettings: PropTypes.func.isRequired,
}
export default MyAccount;

