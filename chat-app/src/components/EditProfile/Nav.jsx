import {
    IconButton,
    Avatar,
    Box,
    Flex,
    HStack,
    VStack,
    Text,
    Menu,
    MenuButton,
    Button,
    useBreakpointValue,
} from '@chakra-ui/react';
import { useContext, } from 'react';
import AppContext from '../../context/AuthContext';
import { useEffect, useState } from 'react';
import { off, onValue, ref } from 'firebase/database';
import { db } from '../../config/firebase';
import PropTypes from 'prop-types';
import { HamburgerIcon } from '@chakra-ui/icons'


const Nav = ({ onOpen }) => {
    const [profilePhoto, setProfilePhoto] = useState(null)
    const { userData } = useContext(AppContext);

    useEffect(() => {
        if (userData) {
          const profileImageUrlRef = ref(db, `users/${userData.handle}/profileImageUrl`);
          const onProfileImageUrlChange = onValue(profileImageUrlRef, (snapshot) => {
            setProfilePhoto(snapshot.val())
          });
    
          return () => {
            off(profileImageUrlRef, onProfileImageUrlChange);
          }
        }
      }, [userData, userData?.profileImageUrl]);

      return (
        <Box>
          <Flex {...boxStyles}>
          {/* {useBreakpointValue({ base: null, md: <SocialProfileWithImage setCurrentSettings={setCurrentSettings} /> })}</HStack> */}
           {useBreakpointValue({base: <Button fontSize={"20px"} bg={"transparent"} onClick={onOpen}><HamburgerIcon/> </Button>, md:null})} 
            
            <HStack spacing={{ base: '0', md: '6' }}>
              <Flex alignItems={'center'}>
                <Menu>
                  <MenuButton py={2} transition="all 0.3s" _focus={{ boxShadow: 'none' }}>
                    <HStack>
                      {userData && (
                        <Avatar size={'sm'} src={profilePhoto} />
                      )}
                      <VStack display={{ base: 'none', md: 'flex' }} alignItems="flex-start" spacing="1px" ml="2">
                        {userData && (
                          <Box>
                            <Text {...textStyles}>{userData.handle}</Text>
                            <Text display={"flex"} justifyContent={"left"} fontSize="xs" color="white">
                              {userData.isAdmin ? 'Admin' : 'User'}
                            </Text>
                          </Box>
                        )}
                      </VStack>
                    </HStack>
                  </MenuButton>
                </Menu>
              </Flex>
            </HStack>
          </Flex>
        </Box>
      );
};

Nav.propTypes = {
    onOpen: PropTypes.func.isRequired
}

export default Nav;


const boxStyles = {
    ml: { base: 0, md: 60 },
    px: { base: 4, md: 4 },
    height: '20',
    alignItems: 'center',
    bg: '#00B998',
    borderBottomWidth: '1px',
    borderRadius: '10px 10px 10px 10px',
    boxShadow: '0 2px 2px rgba(0, 185, 152, 0.4)',
    borderBottomColor: '#00B998',
    justifyContent: { base: 'space-between', md: 'flex-end' },
  };

  // {...iconButtonStyles} 
  // const iconButtonStyles = {
  //   display: { base: 'flex', md: 'none' },
  //   // variant: 'outline',
  //   // color: "red",
  //   // color: 'white',
  //   'aria-label': 'open menu',
  // };

  const textStyles = {
    fontSize: 'sm',
    fontWeight: 'bold',
    color: 'white',
  };

