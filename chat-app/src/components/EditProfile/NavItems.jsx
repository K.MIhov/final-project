import {
  Flex,
  Icon,
  Link,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';

const NavItem = ({ icon, children, ...rest }) => {
  return (
    <Link href="#" style={{ textDecoration: 'none' }} _focus={{ boxShadow: 'none' }}>
      <Flex
        align="center"
        p="4"
        mx="4"
        borderRadius="lg"
        role="group"
        cursor="pointer"
        _hover={{
          bg: '#00B998',
          color: 'white',
        }}
        {...rest}>
        {icon && (
          <Icon
            mr="4"
            fontSize="16"
            _groupHover={{
              color: 'white',
            }}
            as={icon}
          />
        )}
        {children}
      </Flex>
    </Link>
  );
};

NavItem.propTypes = {
  icon: PropTypes.any.isRequired,
  children: PropTypes.string.isRequired,
}

export default NavItem;