import {
  Box,
  Heading,
  Divider,
} from '@chakra-ui/react';
import ProfileContent from './ProfileContent';
import PropTypes from 'prop-types';

const ProfileSettings = ({ setCurrentSettings }) => {
  return (
    <Box>
      <Box ml={{ base: 0, md: 60 }} p="4">
        <Box>
          <Heading>Profile</Heading>
          <Divider orientation="horizontal" mt="20px" />
          <ProfileContent setCurrentSettings={setCurrentSettings} />
        </Box>
      </Box>
    </Box>
  )
}

ProfileSettings.propTypes = {
  setCurrentSettings: PropTypes.func.isRequired
}
export default ProfileSettings;