import {
    Avatar,
    Box,
    Button,
    Divider,
    Input,
    Text,
    VStack,
    useColorModeValue,
    useToast
} from '@chakra-ui/react';
import { ref as ref2, update } from 'firebase/database';
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { useContext, useState } from 'react';
import { toasted } from '../../common/constants';
import { auth, db, storage } from '../../config/firebase';
import AppContext from '../../context/AuthContext';
import PropTypes from 'prop-types';

const EditAvatar = ({ setShowAvatarUpload }) => {
    const [selectedImage, setSelectedImage] = useState(null);
    const [imageUpload, setImageUpload] = useState(null);
    const { userData } = useContext(AppContext);
    const toast = useToast();

    const handleUpload = (event) => {
        const file = event.target.files[0];
        setSelectedImage(file);
        setImageUpload(event.target.files[0])
    };
    const handleBlurBackground = () => {
        setShowAvatarUpload(false);
    };
    const uploadImage = () => {
        if (imageUpload === null) {
            return;
        }
        const imageRef = ref(storage, `profileImages/${auth.currentUser.email}`);
        uploadBytes(imageRef, imageUpload)
            .then((snapshot) => getDownloadURL(snapshot.ref))
            .then((url) => update(ref2(db, `users/${userData.handle}`), { profileImageUrl: url }))
            .then(() => setShowAvatarUpload(false))
            .then(() => toast(toasted[7]))
            .catch((err) => console.err(`Error ocurred while uploading a photo: ${err.message}`));

    }
    return (
        <Box {...containerStyles}>
            <Box {...boxStyles} bg={useColorModeValue("gray.50", "gray.800")}>
                <Text {...textStyles}>Select an Image</Text>
                <VStack {...vStackStyles}>
                    <Box {...innerBoxStyles} bg={useColorModeValue("gray.50", "gray.800")}>
                        {selectedImage ? (
                            <Avatar {...avatarStyles} src={URL.createObjectURL(selectedImage)} />
                        ) : (
                            <Avatar {...avatarStyles} />
                        )}
                        <Input type="file" {...inputStyles} onChange={handleUpload} />
                    </Box>
                </VStack>
                <Divider orientation='horizontal' />
                <Box {...buttonContainerStyles}>
                    <Button {...cancelButtonStyles} onClick={handleBlurBackground}>
                        Cancel
                    </Button>
                    <Button {...uploadButtonStyles} onClick={uploadImage}>
                        Upload Image
                    </Button>
                </Box>
            </Box>
        </Box>
    );

}

export default EditAvatar;

EditAvatar.propTypes = {
    setShowAvatarUpload: PropTypes.func.isRequired
}

const containerStyles = {
    position: "fixed",
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    zIndex: "9999",
    bg: "rgba(0, 0, 0, 0.3)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
};

const boxStyles = {
    // bg: "gray.800",
    p: "4",
    borderRadius: "md",
    boxShadow: "md",
    maxWidth: "400px",
};

const textStyles = {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: "xl",
};

const vStackStyles = {
    display: "flex",
};

const innerBoxStyles = {
    // bg: "gray.800",
    p: "4",
    borderRadius: "md",
    h: "50%",
    w: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
};

const avatarStyles = {
    boxSize: "xs",
};

const inputStyles = {
    border: "none",
};

const buttonContainerStyles = {
    display: "flex",
    justifyContent: "center",
    m: "10px",
};

const cancelButtonStyles = {
    mr: "10px",
    bg: "red",
    color:"white"
};

const uploadButtonStyles = {
    bg: "#00B998",
    color:"white"
};