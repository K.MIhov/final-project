import {
  Avatar,
  Box,
  Button,
  Collapse,
  Flex,
  Heading,
  Stack,
  Text,
  useColorModeValue
} from '@chakra-ui/react';
import { off, onValue, ref } from 'firebase/database';
import { useContext, useEffect, useState } from 'react';
import { db } from '../../config/firebase';
import AppContext from '../../context/AuthContext';
import PropTypes from 'prop-types';

export default function SocialProfileWithImage({ setCurrentSettings }) {
  const { userData } = useContext(AppContext)
  const [show, setShow] = useState(false)
  const [about, setAbout] = useState(null);
  const [displayName, setDisplayName] = useState(null)
  const [profilePhoto, setProfilePhoto] = useState(null)
  const handleToggle = () => setShow(!show)

  useEffect(() => {
    if (userData) {
      const aboutRef = ref(db, `users/${userData.handle}/about`);
      const onAboutChanged = onValue(aboutRef, (snapshot) => {
        setAbout(snapshot.val());
      });

      const displayNameRef = ref(db, `users/${userData.handle}/displayName`);
      const onDisplayNameChanged = onValue(displayNameRef, (snapshot) => {
        setDisplayName(snapshot.val())
      });
      const profileImageUrlRef = ref(db, `users/${userData.handle}/profileImageUrl`);
      const onProfileImageUrlChange = onValue(profileImageUrlRef, (snapshot) => {
        setProfilePhoto(snapshot.val())
      });

      return () => {
        off(aboutRef, onAboutChanged);
        off(displayNameRef, onDisplayNameChanged);
        off(profileImageUrlRef, onProfileImageUrlChange);
      }
    }
  }, [userData, userData?.about, userData?.displayName, userData?.profileImageUrl]);

  return (
    <>
      <Box {...boxStyles} />
      <Flex {...flexStyles}>
        {userData && <Avatar  src={profilePhoto} {...avatarStyles} />}
      </Flex>
      <Box p={6}>
        <Stack spacing={0} align={'center'} mb={5}>
          {userData && <Heading {...headingStyles}>{displayName}</Heading>}
          {userData && <Text {...textStyles}>{userData.email}</Text>}
        </Stack>
        <Stack>
          {userData && userData.about && (
            <Collapse startingHeight={30} in={show}>
              {about}
            </Collapse>
          )}
          <Button {...buttonStyles} onClick={handleToggle}>
            Show {show ? 'Less' : 'More'}
          </Button>
        </Stack>
        <Stack direction={'row'} justify={'center'} spacing={6}></Stack>
        <Button
          onClick={() => setCurrentSettings('My Account')}
          w={'full'}
          mt={8}
          {...buttonStyles}
        >
          Edit Account
        </Button>
      </Box>
    </>
  );
}

SocialProfileWithImage.propTypes = {
  setCurrentSettings: PropTypes.func.isRequired,
}

const boxStyles = {
  h: '120px',
  w: 'full',
  bg: '#00B998',
  objectFit: 'cover',
};

const flexStyles = {
  justify: 'center',
  mt: -12,
};

const avatarStyles = {
  size: 'xl',
  alt: 'Author',
  css: {
    border: '2px solid #00B998',
  },
};

const headingStyles = {
  fontSize: '2xl',
  fontWeight: 500,
  fontFamily: 'body',
};

const textStyles = {
  color: 'gray.500',
};

const buttonStyles = {
  size: 'sm',
  mt: '1rem',
  bg: '#00B998',
  boxShadow: '0 3px 3px rgba(0, 185, 152, 0.4)',
  color: 'white',
  rounded: 'md',
  _hover: {
    transform: 'translateY(-2px)',
    boxShadow: 'lg',
  },
};