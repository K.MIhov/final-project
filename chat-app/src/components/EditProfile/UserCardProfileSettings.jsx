
import {
  Avatar,
  Box,
  Text,
  Button,
} from '@chakra-ui/react';
import { useContext } from 'react';
import AppContext from '../../context/AuthContext';

const UserCardProfileSettings = () => {
  const { userData } = useContext(AppContext);
  return (
    <Box {...boxStyles}>
      {userData && <Avatar {...avatarStyles} src={userData.profileImageUrl}></Avatar>}
      <Box {...innerBoxStyles}>
        {userData && (
          <Text {...textStyles}>{userData.displayName}</Text>
        )}
        <Button {...buttonStyles}>Edit Account</Button>
      </Box>
    </Box>
  )
}

export default UserCardProfileSettings;


const boxStyles = {
  position: 'absolute',
  top: '53%',
  right: '20%',
  transform: 'translateY(-50%)',
  bg: 'gray.800',
  boxShadow: 'md',
  p: '4',
  borderRadius: 'md',
  h: '30%',
  w: '20%',
};

const avatarStyles = {
  ml: '10px',
  size: 'xl',
};

const innerBoxStyles = {
  position: 'absolute',
  top: '70%',
  transform: 'translateY(-50%)',
  bg: 'gray.900',
  boxShadow: 'md',
  p: '4',
  borderRadius: 'md',
  h: '50%',
  w: '90%',
};

const textStyles = {
  p: '8px',
  borderBottom: '1px solid #49cce6',
  fontWeight: 'bold',
};

const buttonStyles = {
  mt: '20px',
  bg: 'blue.600',
};

