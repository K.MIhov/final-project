import {
    Box,
    HStack,
    VStack,
    Text,
    Button,
    Input,
    Spacer,
    useToast,
    useColorModeValue
} from '@chakra-ui/react';
import { auth, db } from '../../config/firebase';
import { useContext, useState } from 'react';
import AppContext from '../../context/AuthContext';
import { update, ref as ref2 } from 'firebase/database';
import { updatePassword } from 'firebase/auth';
import { toasted } from '../../common/constants';
import { getUserDataByEmail } from '../../services/users.service';
import PropTypes from 'prop-types';

const EditData = ({ currentChange, setShowDataChange }) => {
    const [input, setInput] = useState([]);
    const { userData } = useContext(AppContext)
    const toast = useToast();
    const handleBlurBackground = () => {
        setShowDataChange(false);
    };
    function handleInputChange(event) {
        console.log(event.target.value);
        setInput(event.target.value);
    }

    const handleDone = () => {
        if (currentChange[0] === 'password') {
            updatePassword(auth.currentUser, input)
                .then(() => toast(toasted[7])
                    .catch((err) => console.error(`Error ocurred while changing password: ${err.message}`))
                )
        } else if (currentChange[0] === 'email') {
            getUserDataByEmail(input)
                .then((snapshot) => {
                    if (snapshot.exists()) {
                        toast(toasted[0])
                    } else {
                        update(ref2(db, `users/${userData.handle}`), { [currentChange[0]]: input })
                            .then(() => toast(toasted[7]));
                    }
                }).catch((err) => console.error(`Error ocurred while changing email address: ${err.message}`));
        } else {
            update(ref2(db, `users/${userData.handle}`), { [currentChange[0]]: input })
                .then(() => toast(toasted[7]))
                .catch((err) => console.error(`Error ocurred while changing data: ${err.message}`))

        }
        setShowDataChange(false);
    }

    return (
        <Box {...containerStyles}>
            <Box {...boxStyles} bg={useColorModeValue("gray.50", "gray.800")}>
                <HStack {...hStackStyles}>
                    <Text {...textStyles}>{`Change your ${currentChange[1]}`}</Text>
                    <Spacer />
                </HStack>
                <Text display="flex" justifyContent="center" m="0px 0px 10px 10px" fontSize="l">{`Enter a new ${currentChange[1]}`}</Text>
                <VStack {...vStackStyles}>
                    <Box  bg={useColorModeValue("gray.50", "gray.800")}{...inputStyles}>
                        {currentChange[0] === 'password' ? (
                            <Input type="password" onChange={handleInputChange} />
                        ) : (
                            <Input type="text" onChange={handleInputChange} />
                        )}
                    </Box>
                </VStack>
                <HStack color={"white"} display="flex" justifyContent="center" {...buttonStyles}>
                    <Button bg="red" onClick={handleBlurBackground}>Cancel</Button>
                    <Button bg="#00B998" onClick={handleDone}>Done</Button>
                </HStack>
            </Box>
        </Box>
    );
}

export default EditData;

EditData.propTypes = {
    currentChange: PropTypes.arrayOf(PropTypes.string),
    setShowDataChange: PropTypes.func.isRequired,
}

const containerStyles = {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 9999,
    bg: 'rgba(0, 0, 0, 0.3)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
};

const boxStyles = {
    // bg: 'gray.800',
    p: 4,
    borderRadius: 'md',
    maxWidth: '400px',
};

const hStackStyles = {
    m: '0px 10px 10px 10px',
};

const textStyles = {
    fontWeight: 'bold',
    fontSize: 'xl',
};

const vStackStyles = {
    display: 'flex',
};

const inputStyles = {
    // bg: 'gray.800',
    p: 4,
    borderRadius: 'md',
    h: '50%',
    w: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
};

const buttonStyles = {
    mt: '10px',
};