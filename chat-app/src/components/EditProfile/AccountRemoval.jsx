import {
    Box,
    Text,
    Button,
    Divider,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';

const AccountRemoval = ({ setDelete, setDisable }) => {

    const handleDeleteProfile = () => {
        setDelete();
    }
    const handleDisableProfile = () => {
        setDisable()
    }
    return (
        <>
            <Divider pt="1%" orientation="horizontal" />
            <Text p="10px" fontWeight="bold" color={"gray.500"}>Account Removal</Text>
            <Box className='delete-buttons'>
                <Button bg="red" onClick={handleDisableProfile} color={"white"} >Disable Account</Button>
                <Button ml="5px" bg="none" border={"2px solid red"} onClick={handleDeleteProfile}>Delete Account</Button>
            </Box>
        </>
    )
}

AccountRemoval.propTypes = {
    setDelete: PropTypes.func.isRequired,
    setDisable: PropTypes.func.isRequired
}
export default AccountRemoval;