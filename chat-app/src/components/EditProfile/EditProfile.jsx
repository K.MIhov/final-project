import {
  Box,
  useColorModeValue,
  Drawer,
  DrawerContent,
  useDisclosure,
} from '@chakra-ui/react';
import ProfileSettings from './ProfileSettings';
import Nav from './Nav';
import SidebarContent from './SideBarContent';
import MyAccount from './MyAccount';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { signOut } from 'firebase/auth';
import { auth } from '../../config/firebase';
import { CHANNEL_PAGE, LOG_IN_PAGE } from '../../common/constants';

function SidebarWithHeader() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [currentSettings, setCurrentSettings] = useState('Profile');
  const navigate = useNavigate();
  const handleLogout = () => {
    navigate(`${LOG_IN_PAGE}`)
    return signOut(auth);
  }

  const handleNavItemClick = (name) => {
    console.log('here');
    switch (name) {
      case 'Back':
        navigate(`${CHANNEL_PAGE}`);
        break;
      case 'Profile':
        setCurrentSettings('Profile');
        break;
      case 'My Account':
        setCurrentSettings('My Account');
        break;
      case 'Log out':
        handleLogout()
        break;
    }
  }

  return (
    <Box minH="100vh" bg={useColorModeValue('gray.50', 'gray.900')} position="relative">
      <SidebarContent handleNavItemClick={handleNavItemClick} onClose={() => onClose} display={{ base: 'none', md: 'block' }} />
      <Drawer
        autoFocus={false}
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        returnFocusOnClose={false}
        onOverlayClick={onClose}
        size="full"
      >
        <DrawerContent>
          <SidebarContent onClose={onClose} handleNavItemClick={handleNavItemClick} />
        </DrawerContent>
      </Drawer>
      <Nav onOpen={onOpen} />
      {currentSettings === 'Profile' && <ProfileSettings setCurrentSettings={handleNavItemClick} />}
      {currentSettings === 'My Account' && <MyAccount setCurrentSettings={handleNavItemClick} />}
    </Box>
  );
}

export default SidebarWithHeader;
