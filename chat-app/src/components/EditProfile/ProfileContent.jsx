import {
    Box,
    Button,
    Divider,
    HStack,
    Input,
    Text,
    Textarea,
    VStack,
    useToast,
    useBreakpointValue
} from '@chakra-ui/react';
import { ref as ref2, update } from 'firebase/database';
import { deleteObject, ref } from 'firebase/storage';
import { useContext, useState } from 'react';
import { toasted } from '../../common/constants';
import { db, storage } from '../../config/firebase';
import AppContext from '../../context/AuthContext';
import EditAvatar from './EditAvatar';
import SocialProfileWithImage from './Card';
import PropTypes from 'prop-types';

const ProfileContent = ({ setCurrentSettings }) => {
    const { userData } = useContext(AppContext);
    const [input, setInputChange] = useState('');
    const [showAvatarUpload, setShowAvatarUpload] = useState(false);
    const toast = useToast();

    const handleInputChange = (event) => {
        setInputChange(event.target.value);
    }
    const handleAboutChange = () => {
        console.log(input.length);
        if (input.length < 5 || input.length > 80) {
            toast(toasted[8])
        } else {
            update(ref2(db, `users/${userData.handle}`), { about: input })
                .then(() => toast(toasted[7]))
                .catch((err) => console.error(err))
        }

    }
    const handleAvatarChange = () => {
        setShowAvatarUpload(true);
    };

    const handleAvatarUploadClose = () => {
        setShowAvatarUpload(false);

    };

    const handleDisplayNameChange = () => {
        update(ref2(db, `users/${userData.handle}`), { displayName: input })
            .then(() => toast(toasted[7]))
            .catch((err) => console.error(err))
    }
    const handleRemoveAvatar = () => {
        const reference = ref(storage, `profileImages/${userData.email}`)
        deleteObject(reference)
            .then(() => console.log('Successfully removed'))
            .catch((err) => console.error(err))
    }
    return (
        <>
            <Box fontWeight="bold" pt="10px">
                <HStack spacing={20} ml="10px">
                    <VStack align="self-start" spacing={20}>
                        <Box >
                            <Text>Display Name</Text>
                            {userData && (<Input m="10px 0px 10px 0px" onChange={handleInputChange} placeholder={"Enter a Display Name"} />)}
                            <Button
                                color={"white"}
                                bg="#00B998"
                                onClick={handleDisplayNameChange}>
                                Change</Button>
                        </Box>
                        <Divider orientation="horizontal" />
                        <Box >
                            <Text position="relative">Avatar</Text>
                            <Box fontWeight="bold" mt="10px" >
                                <Button
                                    color={"white"}
                                    onClick={handleAvatarChange}
                                    bg="#00B998"
                                    mr="5px">
                                    Change avatar
                                </Button>
                                <Button color={"white"} onClick={handleRemoveAvatar} bg="red" >Remove avatar</Button>
                            </Box>
                        </Box>
                    </VStack>
                    {useBreakpointValue({ base: null, md: <SocialProfileWithImage setCurrentSettings={setCurrentSettings} /> })}</HStack>
                <Divider orientation="horizontal" mt="40px" />
                {showAvatarUpload && (
                    <EditAvatar setShowAvatarUpload={handleAvatarUploadClose} />
                )}
                <Box m="10px">
                    <Text
                        mb="10px"
                    >About</Text>
                    <Textarea onChange={handleInputChange} placeholder={'Share something about you'} h="150px" w="30%" />
                    <br />
                    <Button
                        color={"white"}
                        onClick={handleAboutChange}
                        mt="10px"
                        bg="#00B998">
                        Change</Button>
                </Box>
            </Box>
        </>
    );
}

ProfileContent.propTypes = {
    setCurrentSettings: PropTypes.func.isRequired
}
export default ProfileContent;