import {
    Box,
    CloseButton,
    Flex,
    Image,
    useColorModeValue,
} from '@chakra-ui/react';
import NavItem from './NavItems';
import { LinkItems } from '../../common/constants';
import PropTypes from 'prop-types';

const SidebarContent = ({ onClose, handleNavItemClick, ...rest }) => {

    return (
        <Box
            transition="3s ease"
            bg={useColorModeValue("#F6F6F6",'#17171b')}
            borderRight="1px"
            borderRightColor='rgba(0, 185, 152, 0.4)'
            w={{ base: 'full', md: 60 }}
            pos="fixed"
            h="full"
            boxShadow="0 2px 2px rgba(0, 185, 152, 0.4)"

            {...rest}>
            <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
                <Box display={"flex"} justifyContent={"center"} mt="40px" w={"30%"} className='Logo'>
                    <Image src='https://i.ibb.co/k9KGhBC/White-Chatting-Letter-Initial-Logo-14.png' />
                </Box>
                <CloseButton display={{ base: 'flex', md: 'none' }} onClick={onClose} />
            </Flex>
            <br />
            {LinkItems.map((link) => (
                <NavItem fontWeight="bold" key={link.name} onClick={() => handleNavItemClick(link.name)} icon={link.icon}>
                    {link.name}
                </NavItem>
            ))}
        </Box>
    );
};

SidebarContent.propTypes = {
    onClose: PropTypes.func.isRequired,
    handleNavItemClick: PropTypes.func.isRequired
}

export default SidebarContent;