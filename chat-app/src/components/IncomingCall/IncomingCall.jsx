import {
  Box,
  Button,
  VStack,
  Divider,
  ModalOverlay,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  useDisclosure,
  Avatar
} from '@chakra-ui/react';

import AppContext from "../../context/AuthContext";
import { useContext, useEffect } from 'react';
import { getUserByHandle } from '../../services/users.service';
import { useState } from 'react';
import { ref, update } from 'firebase/database';
import { db } from '../../config/firebase';
import { BsTelephoneInbound } from "react-icons/bs";
import PropTypes from 'prop-types';

export default function IncomingCallModal({ setCallRequested, setMeetingId, data }) {
  const [userCalling, setUserCalling] = useState(null);
  const [audioElement, setAudioElement] = useState(null);
  const { onOpen, onClose } = useDisclosure();
  const { userData } = useContext(AppContext);

  useEffect(() => {
    if (data[0] ==='Group') {
      setUserCalling(data[1].user.teamName)
      onOpen();
    } else {
      console.log(data)
      getUserByHandle(data[1].user)
        .then((snapshot) => setUserCalling(snapshot.val()))
        .then(() => onOpen())
        .catch((err) => console.err(err));
    }
  }, [data]);

  useEffect(() => {
    // Create audio element on component mount
    const audio = new Audio("https://2u039f-a.akamaihd.net/downloads/ringtones/files/mp3/7120-download-iphone-6-original-ringtone-42676.mp3");
    audio.loop = true;
    setAudioElement(audio);

    return () => {
      // Cleanup function to stop and remove audio element
      if (audioElement) {
        audioElement.pause();
        audioElement.currentTime = 0;
        setAudioElement(null);
      }
    };
  }, []);

  useEffect(() => {
    // Start playing audio when the modal opens
    if (userCalling) {
      audioElement.play();
    }
  }, [userCalling]);

  const handleAcceptedCall = () => {
    onClose();
    setMeetingId(data[1].meetingId);
    setCallRequested(false);
    if (audioElement) {
      audioElement.pause();
      audioElement.currentTime = 0;
      setAudioElement(null);
    }
  };

  const handleCancelCall = () => {
    update(ref(db,`users/${userData.handle}`), {requestedCall: 'none'})
    .then(() => {
      onClose();
      setCallRequested(false);

      // Stop and remove audio element when call is canceled
      if (audioElement) {
        audioElement.pause();
        audioElement.currentTime = 0;
        setAudioElement(null);
      }
    })
    .catch((err) => `Error ocurred while canceling incoming call: ${err}`);
  };

  return (
    <>
      {userCalling && (
        <Modal blockScrollOnMount={false} isOpen={onOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            {console.log(userCalling)}
            {data[0] !== 'Group' ? (
              <>
              <ModalHeader display="flex" justifyContent="center">{`You have an incoming call from`}</ModalHeader>
              <ModalHeader display="flex" justifyContent="center">{userCalling.displayName}</ModalHeader>
              </>
            ) : (
              <>
                <ModalHeader display="flex" justifyContent="center">{`You have an incoming call from`}</ModalHeader>
                <ModalHeader display="flex" justifyContent="center">{userCalling}</ModalHeader>
              </>
            )}
            <VStack>
              <ModalBody>
                {userCalling && data[0] !== 'Group' && (
                  <Avatar size="2xl" src={userCalling.profileImageUrl}></Avatar>
                  // ) : (
                  //   <Avatar size="2xl" src={userCalling.profileImageUrl}></Avatar>
                )}
              </ModalBody>
              <Box fontSize="4xl" p="5px">
                <BsTelephoneInbound />
              </Box>
              <Divider orientation="horizontal" mt="20px" />
              <ModalFooter>
                <Button colorScheme="red" onClick={handleCancelCall}>
                  Cancel
                </Button>
                <Button colorScheme="green" ml={3} onClick={handleAcceptedCall}>
                  Accept
                </Button>
              </ModalFooter>
            </VStack>
          </ModalContent>
        </Modal>
      )}
    </>
  );
}

