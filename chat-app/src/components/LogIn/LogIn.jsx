import {
    Box,
    Button,
    FormControl,
    FormLabel,
    Image,
    Input,
    Flex,
    Text,
    Tooltip,
    InputGroup,
    IconButton,
    InputRightElement,
    Icon,
    useToast,
    Avatar,
    HStack,
    Divider,
    VStack,
    Spacer,
    Heading,
    Link,
} from "@chakra-ui/react";
import { ArrowBackIcon, ArrowForwardIcon } from "@chakra-ui/icons";
import { useState, useContext } from "react";
import AppContext from "../../context/AuthContext";
import { FiEye, FiEyeOff } from 'react-icons/fi';
import { auth, db } from "../../config/firebase";
import { setPersistence, browserLocalPersistence } from 'firebase/auth';
import { useNavigate } from "react-router-dom";
import { update, ref } from "firebase/database";
import { loginUser, logoutUser } from "../../services/auth.service";
// import { useToast } from "@chakra-ui/react";
import { CHANNEL_PAGE, HOME_PAGE, SIGN_UP_PAGE, TERMS_PAGE, toasted } from "../../common/constants";
const LogIn = () => {
    return (
        <Box
            backgroundImage="url('https://i.ibb.co/gTnDpf0/Untitled-design-3.png')"
            backgroundPosition="center"
            backgroundSize="cover"
            backgroundRepeat="no-repeat"
            minHeight="100vh"
            display="flex"
            justifyContent="center"
            alignItems="center"
        >
            {console.log(`auth:`, auth.currentUser)}
            {auth.currentUser ?
                (<UserExists />
                ) : (
                    <LogInForm />
                )}
            {/* <LogInForm /> */}
            {/* <UserExists/> */}
        </Box>
    );
};

// const RegisterForm = () => {

// }

const LogInForm = () => {
    const [showPassword, setShowPassword] = useState(false);
    const [form, setForm] = useState({
        email: '',
        password: '',
    });
    const { setUser } = useContext(AppContext);
    const navigate = useNavigate();
    const toast = useToast();
    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    }

    const updateForm = prop => e => {
        setForm({
            ...form,
            [prop]: e.target.value,
        });
    };

    const onLogin = () => {
        if(!form.email || !form.password) {
            toast(toasted[5]);
            return;
        }
        setPersistence(auth, browserLocalPersistence)
            .then(() => {
                return loginUser(form.email, form.password);
            })
            .then((userCredential) => {
                setUser({
                    user: userCredential.user,
                });
                // console.log(auth.currentUser.handle);
                // update(ref(db, `users/${auth.currentUser.handle}/status`), {status: 'Online'})
                // .then(() => alert('status updated'))
                // .catch((err) => console.error(err))
                toast(toasted[6]);
                navigate(`/`);
            })
            .catch((error) => {
                console.error(error);
            });
    };
    return (
        <Box
            bg="gray.800"
            boxShadow="md"
            p="4"
            borderRadius="md"
            h="520px"
            w="30%"
        >
            <HStack>
                <Tooltip label="Back">
                    <ArrowBackIcon onClick={() => navigate(`${HOME_PAGE}`)} boxSize={6} cursor="pointer" />
                </Tooltip>
                <Text fontWeight="bold" cursor="pointer" onClick={() => navigate(`${HOME_PAGE}`)}>Home</Text>
            </HStack>
            <Heading textAlign={"center"}>Log in</Heading>
            <Spacer></Spacer>
            <br />
            <Box
                // top="50%
                bg="gray.900"
                boxShadow="md"
                p="4"
                borderRadius="md"
                h="65%"
                w="100%"
            >
                <Box w="100%" p={8} mt={0} textAlign="center">
                    <FormControl isRequired id="email">
                        <FormLabel>Email</FormLabel>
                        <Input
                            type="text"
                            name="email"
                            value={form.email}
                            onChange={updateForm('email')}
                        />
                    </FormControl>
                    <FormControl isRequired id="password" mt={4}>
                        <FormLabel>Password</FormLabel>

                        <InputGroup>
                            <Input
                                name="password"
                                value={form.password}
                                onChange={updateForm('password')}
                                type={showPassword ? "text" : "password"}

                            />
                            <InputRightElement>
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={togglePasswordVisibility}
                                    icon={showPassword ? <FiEye /> : <FiEyeOff />}
                                    size="md"
                                />
                            </InputRightElement>
                        </InputGroup>
                        <Link pt="3px" display="flex" justifyContent="left" color="blue.200" fontWeight="bold">forgot password?</Link>
                    </FormControl>
                    <Button
                        m="15px"
                        bg="blue.400"
                        w="25%"
                        onClick={onLogin}
                    >
                        Log in
                    </Button>
                    <Spacer></Spacer>
                </Box>
                <Link
                    onClick={() => navigate(`${SIGN_UP_PAGE}`)}
                    display="flex"
                    justifyContent="center"
                    fontWeight="bold"
                >
                    Create account for free
                </Link>
                <Link
                    display="flex"
                    justifyContent="center" mt={2}
                    onClick={()=> navigate(`${TERMS_PAGE}`)}
                >
                    Terms of Use and Privacy Policy
                </Link>
            </Box>
        </Box>
    )
}

// const LogIn = () => {

// }

const UserExists = () => {
    const { userData } = useContext(AppContext)
    const navigate = useNavigate();
    const handleContinue = () => {
        // console.log('here');
        // console.log(auth.currentUser);
        // update(ref(db, `users/${userData.handle}/`), {userStatus: 'Online'})
        // .then(() => alert('status updated'))
        // .catch((err) => console.error(err))
        // console.log('here2');
        return navigate(`${CHANNEL_PAGE}`);
    }
    return (
        <Box
            bg="gray.800"
            boxShadow="md"
            p="4"
            borderRadius="md"
            h="200px"
            w="40%"
        >
            <Box
                bg="gray.900"
                boxShadow="md"
                p="4"
                borderRadius="md"
                h="100%"
                w="100%"
            >
                <HStack spacing={6} pt="15px">
                    {userData && (
                        <Avatar border="2px solid #49cce6" ml="10px" size="xl" src={userData.profileImageUrl} />)}
                    <VStack>
                        {userData && (
                            <Text p="10px" fontWeight="bold">
                                {userData.email}
                                <Text fontWeight="hairline">
                                    your account
                                </Text>
                            </Text>
                        )}
                    </VStack>
                    <Spacer></Spacer>
                    <Button onClick={handleContinue} mt="40px" bg="blue.600">
                        Continue
                    </Button>
                    <Button onClick={logoutUser} mt="20px" bg="blue.600">
                        Log out
                    </Button>
                </HStack>
                <Divider orientation="horizontal" mt="20px" />
            </Box>
        </Box>
    )

}



export default LogIn;