import {
    useMeeting
} from "@videosdk.live/react-sdk";
import { Button, Box } from "@chakra-ui/react";
import { ref } from "firebase/database";
import { db } from "../../config/firebase";
import { update } from "firebase/database";
function Controls({ user }) {
    // console.log(user);
    const { leave, toggleMic, toggleWebcam } = useMeeting();
    const handleLeave = () => {
        update(ref(db, `users/${user}`), { requestedCall: 'none' })
            .then(() => leave())
            .catch((err) => console.err(err))
    }
    return (
        <Box display={"flex"} justifyContent={"center"} color="white">
            <Button bg="red" onClick={handleLeave} m="10px">Leave</Button>
            <Button bg="#00B998" onClick={() => toggleMic()} m="10px">toggleMic</Button>
            <Button bg="#00B998" onClick={() => toggleWebcam()} m="10px">toggleWebcam</Button>
        </Box>
    );
}

export default Controls;