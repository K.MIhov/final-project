import { useEffect, useMemo, useRef } from "react";
import {
    useParticipant,
} from "@videosdk.live/react-sdk";
import ReactPlayer from "react-player";
import { Button, Text, Box } from "@chakra-ui/react";

function ParticipantView(props) {
    const micRef = useRef(null);
    const { webcamStream, micStream, webcamOn, micOn, isLocal, displayName } =
        useParticipant(props.participantId);

    const videoStream = useMemo(() => {
        if (webcamOn && webcamStream) {
            const mediaStream = new MediaStream();
            mediaStream.addTrack(webcamStream.track);
            return mediaStream;
        }
    }, [webcamStream, webcamOn]);

    useEffect(() => {
        if (micRef.current) {
            if (micOn && micStream) {
                const mediaStream = new MediaStream();
                mediaStream.addTrack(micStream.track);

                micRef.current.srcObject = mediaStream;
                micRef.current
                    .play()
                    .catch((error) =>
                        console.error("videoElem.current.play() failed", error)
                    );
            } else {
                micRef.current.srcObject = null;
            }
        }
    }, [micStream, micOn]);

    return (
        // <Box
        // position="fixed"
        // top="0"
        // left="0"
        // right="0"
        // bottom="0"
        // zIndex="9999"
        // bg="rgba(0, 0, 0, 0.5)"
        // display="flex"
        // justifyContent="center"
        // alignItems="center"
        // >
        //     <Box
        //         bg="gray.700"
        //         p="4"
        //         borderRadius="md"
        //         boxShadow="md"
        //     // maxWidth="400px"4
        //     >
        <div>
            <Text textAlign={"center"} fontWeight={"bold"}>
                Participant: {displayName}
            </Text>
            <Text textAlign={"center"} fontWeight={"bold"} pt="5px">
            Webcam: {webcamOn ? "ON" : "OFF"} | Mic:{" "}
                {micOn ? "ON" : "OFF"}
            </Text>
            <audio ref={micRef} autoPlay playsInline muted={isLocal} />
            {webcamOn && (
                <ReactPlayer
                    //
                    playsinline // very very imp prop
                    pip={false}
                    light={false}
                    controls={false}
                    muted={true}
                    playing={true}
                    //
                    url={videoStream}
                    //
                    // border="1dpx solid #00B998"
                    // border: '1px solid #00B998' , 
                    style={{borderRadius:"10px 10px 10px 10px" , background:"black"}}
                    // borderRadius="10px 10px 10px 10px"
                    height={"500px"}
                    width={"600px"}
                    onError={(err) => {
                        console.log(err, "participant video error");
                    }}
                />
            )}
            </div>
        // </Box>
        // </Box>
        
    );
}

export default ParticipantView;