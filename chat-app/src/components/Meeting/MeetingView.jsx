import { useState } from "react";
import {
    useMeeting,
} from "@videosdk.live/react-sdk";
import ParticipantView from "./ParticipantView";
import Controls from "./Controls";
import { Button, Text, useColorModeValue, Box, Heading, SimpleGrid } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import { ref, onChildAdded, off } from "firebase/database";
import { db } from "../../config/firebase";
import { update } from "firebase/database";
import AppContext from "../../context/AuthContext";
import { useEffect, useContext } from "react";


function MeetingView(props) {
    const [joined, setJoined] = useState(null);
    const { userData } = useContext(AppContext);
    //Get the method which will be used to join the meeting.
    //We will also get the participants list to display all participants
    const { join, participants } = useMeeting({
        //callback for when meeting is joined successfully
        onMeetingJoined: () => {
            setJoined("JOINED");
        },
        //callback for when meeting is left
        onMeetingLeft: () => {
            props.onMeetingLeave();
            update(ref(db, `users/${userData.handle}`), { requestedCall: 'none' })
                .then(() => console.log('success'))
                .catch((err) => console.err(err))
        },
    });
    const joinMeeting = () => {
        setJoined("JOINING");
        // console.log(props.user);
        // // console.log(user);
        // update(ref(db, `users/${props.user}`), {requestedCall: {[userData.handle]: props.meetingId}})
        // // .then(() => setMeetingId(id))
        // .catch((err) => console.err(err));
        join();
    };
    

    return (
        <Box
            position="fixed"
            top="0"
            left="0"
            right="0"
            bottom="0"
            zIndex="999"
            bg="rgba(0, 0, 0, 0.3)"
            display="flex"
            justifyContent="center"
            alignItems="center">
            <Box
                // bg="gray.700"
                p="4"
                borderRadius="md"
                boxShadow="md"
                bg={useColorModeValue( "gray.100",'gray.700')}

            // maxWidth="400px"4
            >

                {/* <CloseIcon pb="5px" onClick={() => props.setActiveCall(false)} cursor={"pointer"} /> */}
                {/* <h3>Meeting Id: {props.meetingId}</h3> */}
                {joined && joined == "JOINED" ? (
                    <Box>

                        <SimpleGrid  justifyContent={"center"} columns={{base:1, md:1, lg:2, xl:2}} spacing={2}>

                        {/* //For rendering all the participants in the meeting */}
                        {[...participants.keys()].map((participantId, index) => (
                            <Box  key={index}>
                            <ParticipantView
                                participantId={participantId}
                                key={participantId}
                            />
                            </Box>
                        ))}
                        </SimpleGrid>
                        <Controls user={props.user} />
                    </Box>
                ) : joined && joined == "JOINING" ? (
                    <Heading size={"md"}>Joining the meeting...</Heading>
                ) : (
                    <>
                    <Heading size={"md"}>Click Join to enter your call room ✌️</Heading>
                    <Box display={"flex"} justifyContent={"center"} mt="20px">
                        <Button onClick={joinMeeting} bg="#00B998">Join</Button>
                        {/* <Button onClick={joinMeeting}>Cancel</Button> */}
                    </Box>
                    </>
                )}
            </Box>
        </Box>
    );
}

export default MeetingView;


// function Container() {
//     return <HLSPlayer />;
//   }
  
//   function HLSPlayer() {
//     return <div style={{ height: "100vh", width: "100vw" }}></div>;
//   }