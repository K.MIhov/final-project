import { useState, useContext } from "react";
import { Input, Button, Box, Divider, useColorModeValue } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import AppContext from "../../context/AuthContext";

function JoinScreen({ setActiveCall, getMeetingAndToken }) {
  const [meetingId, setMeetingId] = useState(null);
  const { userData } = useContext(AppContext);

  const handleShowCall = () => {
    setActiveCall(false);
  }
  const onClickMakeCall = async () => {
    await getMeetingAndToken(meetingId);
    // console.log(meetingId);
  };
  const onClickJoinCall = async () => {
    // console.log(userData.requestedCall.id)
    console.log('***************************');
    console.log(Object.values(userData.requestedCall).join(''))
    console.log(Object.values(userData.requestedCall))

    console.log('***************************');

    await getMeetingAndToken(Object.values(userData.requestedCall).join(''))
  }
  return (
    <Box position="fixed"
      top="0"
      left="0"
      right="0"
      bottom="0"
      zIndex="9999"
      bg="rgba(0, 0, 0, 0.5)"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Box
        bg="gray.700"
        // bg={useColorModeValue( "gray.50",'gray.700')}
        p="4"
        borderRadius="md"
        boxShadow="md"
        // maxWidth="300px"
        // height={"180px"}
      >
        <CloseIcon pb="5px" onClick={handleShowCall} cursor={"pointer"} />
        {/* <Input
          type="text"
          placeholder="Enter Meeting Id"
          onChange={(e) => {
            setMeetingId(e.target.value);
          }}
        /> */}
        <Divider orientation="horizontal" mt="20px" mb="20px" />
        <Button onClick={onClickJoinCall}>Join</Button>
        {" or "}
        <Button onClick={onClickMakeCall}>Make Call</Button>
      </Box>
    </Box>
  );
}

export default JoinScreen;