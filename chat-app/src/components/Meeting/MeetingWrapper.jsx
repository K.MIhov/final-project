import {
  MeetingProvider,
} from "@videosdk.live/react-sdk";
import { useState, useContext } from 'react';

import AppContext from "../../context/AuthContext";
import { createMeeting } from "../../services/generateToken";
import { authToken } from "../../services/generateToken";
import JoinScreen from "./JoinScreen";
import MeetingView from "./MeetingView"
import { update, ref } from "firebase/database";
import { db } from "../../config/firebase";

function MeetingWrapper() { //{ setCallId }
  const [meetingId, setMeetingId] = useState(null);
  const { userData } = useContext(AppContext);

  //Getting the meeting id by calling the api we just wrote
  const getMeetingAndToken = async (id) => {
    console.log(id);
    const meetingId =
      id == null ? await createMeeting({ token: authToken }) : id;
    setMeetingId(meetingId);
    // setCallId(meetingId);
  };

  //This will set Meeting Id to null when meeting is left or ended
  const onMeetingLeave = () => {

    setMeetingId(null);
  };

  return authToken && meetingId ? (
    <MeetingProvider
      config={{
        meetingId,
        micEnabled: true,
        webcamEnabled: true,
        name: userData.handle,
      }}
      token={authToken}
    >
      <MeetingView meetingId={meetingId} onMeetingLeave={onMeetingLeave} />
    </MeetingProvider>
  ) : (
    <JoinScreen getMeetingAndToken={getMeetingAndToken} />
  );
}

export default MeetingWrapper;