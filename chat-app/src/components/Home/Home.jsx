import { Box, Flex, Text, Image, Grid } from "@chakra-ui/react";
import { useEffect, useRef } from "react";
import image1 from '../../assets/beam-remote-work-from-home.gif';
import image2 from '../../assets/beam-meditation.gif';
import image3 from '../../assets/beam-woman-sitting-at-desk-and-programming.gif';
import image4 from '../../assets/beam-analytics.gif';
import image5 from '../../assets/business-3d-businesswoman-is-satisfied-with-business-statistics-456x456.png'
import './Home.css'
import Header from "../Header/Header";
import Statistics from "../Statistics/Statistics";
import Customers from "../../views/Customers/Customers";
import Footer from "../../views/Footer/Footer";
import BasicStatistics from "../Statistics2/Statistics2";
import WithSpeechBubbles from "../../views/Customers2/Customers2";
import AOS from 'aos';

const sections = [
    {
        id: "section1",
        text: "Create a private community where you belong",
        additional_text: ' GameHub servers are organized into topic-based channels where you can collaborate, share, and just talk about your day without clogging up a group chat.',
        image: 'https://i.ibb.co/M2hSZck/portrait-young-cheerful-man-earphones-sitting-table-with-laptop-home-1.jpg',
        align: "right"
    },
    {
        id: "section2",
        text: "Connect effortlessly with your friends",
        additional_text: "Join a voice channel at your convenience. Your friends in the server will see that you're online and can instantly join the conversation without needing to make a call.",
        image: 'https://i.ibb.co/p0QQgjr/Orange-And-Black-Modern-International-Youth-Day-Poster.png',
        align: "left"
    },
    {
        id: "section3",
        text: "Build a thriving community",
        additional_text: "Create and manage your own community with powerful moderation tools and customizable member access. Grant special privileges to members, set up private channels, and more.",
        image: 'https://i.ibb.co/dMsfVVn/Blue-White-Modern-International-Youth-Day-Poster.png',
        align: "right"
    },

];

function HomePage() {
    const observer = useRef();
    const sectionRefs = useRef([]);
    useEffect(() => {
        AOS.init({ duration: 2000 });
    }, []);

    useEffect(() => {
        observer.current = new IntersectionObserver(
            (entries) => {
                entries.forEach((entry) => {
                    const { id } = entry.target.dataset;
                    if (entry.isIntersecting) {
                        entry.target.classList.add("visible")
                    } else {
                        entry.target.classList.remove("visible")
                    }
                });
            },
            { threshold: 0.2 }
        );

        sectionRefs.current.forEach((ref) => observer.current.observe(ref));

        return () => {
            if (observer.current) observer.current.disconnect();
        };
    }, []);

    return (
        <Box>
            <Header />
            {sections.map((section, index) => (
                <Flex
                    key={section.id}
                    ref={(el) => sectionRefs.current[index] = el}
                    data-id={section.id}
                    direction={section.align === "right" ? "row-reverse" : "row"}
                    alignItems="center"
                    justifyContent="space-around"
                    py={16}
                    // bg={index % 2 ? "#121634" : "#292F3F"}
                    bg={'gray.50'}
                    // className="fade-in-section"
                >
                    <Box w="40%" data-aos="fade-up"
                    >
                        <Text fontSize="3xl" fontWeight="bold" color="gray.700">{section.text}</Text>
                        <Text mt={4} fontSize="xl" color="gray.700"> {section.additional_text}</Text>
                    </Box>
                    <Box w="25%">
                        <Image
                            data-aos="fade-up"

                            boxShadow={"md"}
                            // border={"1px solid #00B998"}
                            borderRadius={"10px 10px 10px 10px"}
                            src={section.image}
                            alt={section.text}
                        />
                    </Box>
                </Flex>
            ))}
            <Grid templateColumns="repeat(2, 1fr)" gap={6} bg={'gray.50'}>
                <Box            data-aos="fade-up"
>
                    {/* <Statistics /> */}
                    <BasicStatistics />
                </Box>
                <Box d="flex" alignItems="center" justifyContent="center" w="50%">
                    <Image
                        // position={"fixed"}
                        // top="0"
                        // bottom="0"
                        // boxSize={""}
                        mt="160px"
                        borderRadius={"10px 10px 10px 10px"}
                        src={'https://i.ibb.co/7KhhNjx/digital-graph-performance-with-businessman-hand-overlay.jpg'}
                    />
                </Box>
            </Grid>
            <WithSpeechBubbles></WithSpeechBubbles>
            {/* <Customers/> */}
            <Footer />
        </Box>
    );
}

export default HomePage;
