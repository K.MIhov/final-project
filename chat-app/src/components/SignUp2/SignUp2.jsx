import {
    Flex,
    Image,
    Stack,
    useColorModeValue,
} from '@chakra-ui/react';
import SimpleCard2 from "./SimpleCard";

export default function SplitScreen2() {
    return (
        <Stack bg={useColorModeValue('gray.100', 'gray.800')} h={'100vh'} minW={'100vh'} direction={{ base: 'column', md: 'row' }}>
            <Flex p={8} flex={1} align={'center'} justify={'center'}>
                <SimpleCard2></SimpleCard2>
            </Flex>
            <Flex flex={1}>
                <Image
                    display={{ base: "none", xl: "block" }}
                    alt={'Login Image'}
                    objectFit={'cover'}
                    // h={"100vh"}
                    src={
                        'https://i.ibb.co/3y40dLn/side-view-employee-working-laptop.jpg'
                    }
                />
            </Flex>
        </Stack>
    );
}
