import { ArrowBackIcon } from "@chakra-ui/icons";
import {
    Button,
    Flex,
    FormControl,
    FormLabel,
    Heading,
    IconButton,
    Input,
    InputGroup,
    InputRightElement,
    Link,
    Stack,
    Tooltip,
    useToast,
    Box,
    Text,
    useColorModeValue,
    useColorMode,
    Switch
} from '@chakra-ui/react';
import { useContext, useState } from "react";
import { FiEye, FiEyeOff } from 'react-icons/fi';
import { useNavigate } from "react-router-dom";
import { auth } from "../../config/firebase";
import AppContext from "../../context/AuthContext";
import { GoogleOutlined } from "@ant-design/icons";
import { signInWithPopup } from "firebase/auth";
import { LOG_IN_PAGE, formControl, initialForm, toasted } from "../../common/constants";
import { googleProvider } from '../../config/firebase';
import { registerUser } from "../../services/auth.service";
import { createUserHandle, getUserByHandle } from "../../services/users.service";

export default function SimpleCard2() {
    const [form, setForm] = useState(initialForm);
    const [showPassword, setShowPassword] = useState(false);
    const { colorMode, toggleColorMode } = useColorMode();

    const { setUser } = useContext(AppContext);
    const navigate = useNavigate();
    const toast = useToast();

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    }

    const onRegister = () => {
        const emailRegex = /\S+@\S+\.\S+/;
        if (!emailRegex.test(form.email) || !form.email) {
            toast(toasted[0]);
            return;
        }
        if (form.handle < 4 || form.handle > 32) {
            toast(toasted[1]);
            return;
        }
        if (!form.displayName) {
            toast(toasted[2]);
            return;
        }
        if (form.password.length < 7 || form.password.length > 64) {
            toast(toasted[3]);
            return;
        }

        getUserByHandle(form.handle)
            .then(snapshot => {
                if (snapshot.exists()) {
                    toast(toasted[4])
                    throw new Error('username taken')
                }
                return registerUser(form.email, form.password);
            })
            .then(credential => {
                return createUserHandle(form.handle, credential.user.uid, form.displayName, form.phoneNumber, credential.user.email, form.isAdmin, form.isBlocked, form.profileImageUrl, 'none', 'none')

            })
            .then(() => {
                navigate(`/log-in`);
            })
            .catch(e => console.log(e));
    }
    const onSignUpWithGoogle = () => {
        signInWithPopup(auth, googleProvider)
            .then(() => {
                navigate(`/log-in`);
            })
            .catch((err) => {
                console.error(err);
            });
    };
    const updateForm = prop => e => {
        setForm({
            ...form,
            [prop]: e.target.value,
        });
    };
    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('gray.100', 'gray.800')}
        >
            <Stack spacing={8} mx={'auto'} maxW={'lg'} w={'lg'} py={12} px={6}>
                <Stack align={'center'}>
                    <Heading fontSize={'4xl'}>Create a free account</Heading>
                    <Text fontSize={'lg'} color={'gray.600'}>
                        to enjoy all of our newest <Link color={"#00B998"}>features</Link>
                    </Text>
                </Stack>
                <Box
                    rounded={'lg'}
                    bg={useColorModeValue('white', 'gray.700')}
                    boxShadow="0 5px 5px rgba(0, 185, 152, 0.4)"
                    p={8}
                >
                    <Stack spacing={4} >
                        <Tooltip label="Back">
                            <ArrowBackIcon onClick={() => navigate(`${LOG_IN_PAGE}`)} boxSize={5} cursor="pointer" />
                        </Tooltip>
                        {formControl.map((forms) => (
                            <FormControl pb="5px" key={forms.id} isRequired id={forms.id}>
                                <FormLabel>{forms.input}</FormLabel>
                                <Input value={form[forms.id]} type={forms.id} name={forms.id} onChange={updateForm(forms.id)} />
                            </FormControl>
                        ))}
                        <FormControl isRequired id="password">
                            <FormLabel>Password</FormLabel>
                            <InputGroup>
                                <Input
                                    name="password"
                                    value={form.password}
                                    onChange={updateForm('password')}
                                    type={showPassword ? "text" : "password"}

                                />
                                <InputRightElement>
                                    <IconButton
                                        borderRadius={"25% 25%"}
                                        aria-label="toggle password visibility"
                                        onClick={togglePasswordVisibility}
                                        icon={showPassword ? <FiEye /> : <FiEyeOff />}
                                        size="md"
                                    />
                                </InputRightElement>
                            </InputGroup>
                        </FormControl>
                        <Stack spacing={10}>
                            <Button
                                bg="#00B998"
                                boxShadow="0 5px 5px rgba(0, 185, 152, 0.4)"
                                color={'white'}
                                onClick={onRegister}
                            >
                                Sign up
                            </Button>

                        </Stack>
                        <Text fontWeight={"bold"} textAlign={"center"}>
                            Light mode is still in progress. We suggest you to turn the dark mode ON to get the full experience.
                        </Text>
                        <Switch display={"flex"} size={"lg"} justifyContent={"center"} id='theme-switch' onChange={toggleColorMode} isChecked={colorMode === 'dark'}></Switch>
                        {/* <Button
                            bg="#00B998"
                            boxShadow="0 5px 5px rgba(0, 185, 152, 0.4)"
                            color={'white'}
                            onClick={onSignUpWithGoogle}
                            >
                            <GoogleOutlined style={{ fontSize: '24px' }} />
                            <Text>  Sign up with Google</Text>

                        </Button> */}
                        <Link
                            display="flex"
                            justifyContent="center" mt={2}
                        >
                            Terms of Use and Privacy Policy
                        </Link>
                    </Stack>
                </Box>
            </Stack>
        </Flex>
    );
}