import AppContext from "../../context/AuthContext";
import Search from "../Search/Search";
import { useState, useContext } from "react";
import { addChannel } from "../../services/channels.service";
import { Link } from "react-router-dom";
import {
  TEAMS_MAX_LENGTH_TITLE,
  TEAMS_MIN_LENGTH_TITLE,
} from "../../common/constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDeleteLeft } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";
import {
  Container,
  FormControl,
  FormLabel,
  Input,
  Button,
  Box,
  Text,
  Card,
  Avatar,
  CardBody,
  Modal,
  ModalContent,
  ModalOverlay,
  ModalHeader,
} from "@chakra-ui/react";

const GroupChat = ({ isOpen, onClose, isTrue }) => {
  const [selectedUsers, setSelectedUsers] = useState(new Map());
  const { userData } = useContext(AppContext);
  const [form, setForm] = useState({
    subject: "",
  });

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };
  const navigate = useNavigate();

  const handleAddUser = (user) => {
    if (
      !selectedUsers.has(user.handle) &&
      !selectedUsers.has(userData.handle)
    ) {
      setSelectedUsers((prevSelectedUsers) =>
        new Map(prevSelectedUsers).set(userData.handle, userData)
      );
      setSelectedUsers((prevSelectedUsers) =>
        new Map(prevSelectedUsers).set(user.handle, user)
      );
    } else if (!selectedUsers.has(user.handle)) {
      setSelectedUsers((prevSelectedUsers) =>
        new Map(prevSelectedUsers).set(user.handle, user)
      );
    } else {
      alert(`${user.handle} is already added to the group`);
      return;
    }
  };

  const removeUser = (handle) => {
    const updatedSelectedUsers = new Map(selectedUsers);
    updatedSelectedUsers.delete(handle);
    setSelectedUsers(updatedSelectedUsers);
  };


  const handleSubmit = (e, isSingleChat) => {
    e.preventDefault();
    const participants = Array.from(selectedUsers.values()).map(
      (user) => user.uid
    );

    if (
      form.subject.length < TEAMS_MIN_LENGTH_TITLE ||
      form.subject.length > TEAMS_MAX_LENGTH_TITLE ||
      isSingleChat
    ) {
      alert(
        `The subject must be between ${TEAMS_MIN_LENGTH_TITLE} and ${TEAMS_MAX_LENGTH_TITLE} symbols`
      );
      return;
    }

    if (!userData) {
      alert("Please login in your account");
      return;
    }

    if (participants.length <= 1) {
      alert("Please add members");
      return;
    }

    let teamList = null;
    if (participants.length > 2) {
      teamList = {
        teamName: form.subject,
        teamPhoto: "none",
        description: "none",
        listOfParticipants: participants,
      };
    } else {
      teamList = {
        listOfParticipants: participants,
      };
    }

    addChannel(teamList, userData.uid);

    setForm({
      subject: "",
    });

    setSelectedUsers(new Map());

    navigate(`/channel`);
  };

  return ( 
    <>
    <Modal isOpen={isOpen} onClose={onClose} size={'xl'}>
    <ModalOverlay />
      <ModalContent>
      <ModalHeader
      color={"white"}
      bg={"#00B998"}
      >Create group chat</ModalHeader>
      <Container
        maxW={"full"}
        // mt={"50px"}
        p={"20px"}
        // border={"1px"}
        // borderRadius={"5px"}
        // borderColor={"blue.400"}
      >
        <FormControl id="subject">
          {/* <FormLabel>Team name</FormLabel> */}
          <Input
            type="text"
            placeholder="Team name..."
            size="md"
            id="subject"
            value={form.subject}
            onChange={updateForm("subject")}
            focusBorderColor={"#00B998"}
          />
        </FormControl>
        <FormControl id="category">
          {/* <FormLabel>Search for a member</FormLabel> */}
          <Search 
          handleAddUser={handleAddUser} />
        </FormControl>
        {selectedUsers.size > 1 ? (
          <Box mt={"10px"} p={0}>
            <Text>Added Users:</Text>
            {Array.from(selectedUsers.values()).map((obj, index) => {
              if (index > 0) {
                return (
                  <Card
                    direction={{ base: "column", sm: "row" }}
                    overflow="hidden"
                    variant="outline"
                    key={obj.uid}
                    m={"10px 0px"}
                    p={"10px"}
                  >
                    <Link to={`/profile/${obj.uid}`}>
                      <Avatar name={obj.handle} size={"sm"} mt={"2px"} />
                    </Link>

                    <CardBody p={"5px 5px"}>
                      <Text>{obj.handle}</Text>
                    </CardBody>
                    <Button onClick={() => removeUser(obj.handle)}>
                      <FontAwesomeIcon icon={faDeleteLeft} size={"lg"}/>
                    </Button>
                  </Card>
                );
              }
            })}
          </Box>
        ) : (
          ""
        )}
        <Box
        display={'flex'}
        alignItems={'center'}
        justifyContent={'center'}
        >
          <Button 
          type="submit" 
          onClick={(e) => handleSubmit(e)} 
          mt={"15px"}
          color={"white"}
          p={'10px 25px'}
          bg={'#00B998'}
          border={'1px'}
          borderColor={'#00B998'}
          _hover={{
            bg: "#00B998",
            boxShadow: "0 10px 20px rgba(0, 185, 152, 0.4)",
            transform: "translateY(-2px)",
          }}
          >
            Create
          </Button>
        </Box>
      </Container>
      </ModalContent>
      </Modal>
    </>
  );
};

export default GroupChat;
