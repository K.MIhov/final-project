import { GridItem, Text, Box, Container, Center, Button, useDisclosure, useColorModeValue } from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDoorOpen,
  faMessage,
  faUserGroup,
} from "@fortawesome/free-solid-svg-icons";
import GroupChat from "../GroupChat/GroupChat";
import { useState } from "react";

const GridChild = ({ channels, specificRoom }) => {
  const [isOpen, setIsOpen] = useState(false);

  const openGroupChat = () => {
    setIsOpen(true);
  };

  const closeGroupChat = () => {
    setIsOpen(false);
  };
  return (
    <>
      {channels.length ? channels.map((channel) => (
        <GridItem
          key={channel.id}
          w={"100%"}
          color={"white"}
          onClick={() => specificRoom(channel.id)}
          h={"200px"}
          borderRadius={"5px"}
          border={"1px"}
          borderColor={"#00B998"}
          bg={"#00B998"}
          position={"relative"}
          cursor={"pointer"}
          boxShadow={"0 0 10px rgba(0, 0, 0, 0.3)"}
          transition={"box-shadow 0.3s ease-out, transform 0.3s ease-out"}
          _hover={{
            bg: "#00B998",
            borderColor: "#00B998",
            color: "white",
            boxShadow: "0 10px 20px rgba(0, 185, 152, 0.4)",
            transform: "translateY(-5px)",
          }}
        >
          <Box
            display={"flex"}
            flexDirection={"column"}
            alignItems={"center"}
            justifyContent={"center"}
            height={"100%"}
          >
            <FontAwesomeIcon icon={faDoorOpen} size={"2xl"} />
            <Text
              fontSize={"20px"}
              fontWeight={"bold"}
              marginBottom={"20px"}
              marginTop={"20px"}
            >
              {channel.team.teamName}
            </Text>
            <Box display={"flex"} alignItems={"center"}>
              <FontAwesomeIcon
                icon={faUserGroup}
                size={"sm"}
                style={{ marginLeft: "10px" }}
              />
              <Text m={"0px 5px"}>{channel.team.listOfParticipants.length}</Text>
            </Box>
          </Box>
        </GridItem>
      )) :
        <Container
          maxW={"full"}
          p={"0px"}
        >
          <Center>
            <Text
              fontSize={"30px"}
              color={"#00B998"}
              fontWeight={"bold"}
            >
              Hello, you don`t have any group chats <br /> You can create a group chat from here
            </Text>
          </Center>
          <Center mt={"20px"}>
            <Button
              bg={'#00B998'}
              color="white"
              border={"1px"}
              borderColor={"#00B998"}
              _hover={{
                bg: "#00B998",
                borderColor: "#00B998",
                color: "white",
                boxShadow: "0 10px 20px rgba(0, 185, 152, 0.4)",
                transform: "translateY(-2px)",
              }}
              onClick={openGroupChat}


            >
              Create
            </Button>
            <GroupChat isOpen={isOpen} onClose={closeGroupChat} />
          </Center>
        </Container>
      }
    </>
  );
};

export default GridChild;
