import { getAllChannels } from "../../services/channels.service";
import { useState, useEffect, useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faComment, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FONT_SIZE_SIDEBARCONTACTS, PADDING_SIDEBARCONTACTS, MARGIN_SIDEBARCONTACTS } from "../../common/constants";
import {
  Box,
  Button,
  Container,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
  useColorModeValue,
  useBreakpointValue,
} from "@chakra-ui/react";
import AppContext from "../../context/AuthContext";
import { ref, onChildAdded, off } from "firebase/database";
import { db } from "../../config/firebase";
import { createChat } from "../../services/channels.service";

const SidebarContacts = ({ onChatClick, id, chatId, selectedChat, switchDisplay, setSwitchDisplay }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [chats, setChats] = useState([]);
  const [channel, setChannel] = useState(null);
  const [query, setQuery] = useState("");
  const { userData } = useContext(AppContext);
  console.log(selectedChat)
  console.log(id)
  console.log(chatId)

  // const buttonFontSize = useBreakpointValue(FONT_SIZE_SIDEBARCONTACTS);
  const padding = useBreakpointValue(PADDING_SIDEBARCONTACTS);
  const margin = useBreakpointValue(MARGIN_SIDEBARCONTACTS);

  useEffect(() => {
    setChats([]);
    const chatRef = ref(db, `channels/${id}/chat`);
    const onChatAdded = onChildAdded(chatRef, (snapshot) => {
      const chatId = snapshot.key;
      const updatedChat = snapshot.val();
      console.log(chatId)
      updatedChat.id = chatId
      setChats((prevChats) => ({
        ...prevChats,
        [chatId]: updatedChat,
      }));
    });

    return () => {
      off(chatRef, "child_added", onChatAdded);
    };
  }, [id]);

  useEffect(() => {
    if (userData) {
      getAllChannels().then((res) => {
        const filterChannel = res.filter((obj) => obj.id === id);
        setChannel(filterChannel[0]);
      });
    }
  }, [userData]);

  const onTyping = (e) => {
    e.preventDefault();
    setQuery(e.target.value);
  };

  const handleDisplayAttribute = () => {
    setSwitchDisplay('Chat');
  }

  return (
    <>
      <Container
        display={switchDisplay === "DMs" ? { base: "block", xl: "block" } : { base: "none", xl: "block" }}
        maxW={"full"}
        // zIndex={50}
        bg={useColorModeValue("gray.100", "#1F232F")}
        borderRadius={"10px "}
        p={"0px"}
        h={'100vh'}
      >
        <Text
          // color={"white"}
          fontSize={"20px"}
          fontWeight={"bold"}
          // m={"20px 0px"}
          p={"20px"}
          borderBottom={"1px"}
          borderColor={'#292F3F'}
        >
          Chats
        </Text>
        {/* {console.log(channel)} */}
        {userData && channel && userData.uid === channel.owner ? (
          <Box
            onClick={onOpen}
            borderRadius={"5px"}
            p={padding}
            bg={"#292F3F"}
            _hover={{ bg: "#00B998" }}
            m={margin}
            cursor={"pointer"}
          >
            Create Text Chat
            <FontAwesomeIcon
              icon={faPlus}
              size={"lg"}
              cursor={"pointer"}
              style={{ marginLeft: "10px" }}
            />

            <Modal isOpen={isOpen} onClose={onClose} size={"lg"}>
              <ModalOverlay />
              <ModalContent>
                <ModalHeader bg={"#00B998"}>Create chat</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <Input
                    placeholder={"Chat name"}
                    value={query}
                    onChange={onTyping}
                    m={"20px 0px"}
                    focusBorderColor={"#00B998"}
                  />
                  <Box
                    display={"flex"}
                    alignItems={"center"}
                    justifyContent={"center"}
                  >
                    <Button
                      type="submit"
                      onClick={() => createChat(channel.id, query)}
                      mb={"20px"}
                      p={"10px 25px"}
                      bg={"gray.700"}
                      border={"1px"}
                      borderColor={"#00B998"}
                      _hover={{
                        bg: "#00B998",
                        boxShadow: "0 10px 20px rgba(0, 185, 152, 0.4)",
                        transform: "translateY(-2px)",
                      }}
                    >
                      Create
                    </Button>
                  </Box>
                </ModalBody>
              </ModalContent>
            </Modal>
          </Box>
        ) : (
          ""
        )}
        {console.log(chats)}
        {userData &&
          Object.values(chats).map((chat) => (
            // {console.log()}
            <Box
              key={chat.name}
              onClick={() => onChatClick(chat, channel)}
              cursor={"pointer"}
              display={"flex"}
              flexDirection={"row"}
              alignItems={"center"}
              m={margin}
              p={padding}
              border={"1px"}
              borderRadius={"5px"}
              borderColor={"#292F3F"}
              _hover={{
                bg: "#00B998"
              }}
              bg={selectedChat.id === chat.id ? "#00B998" : "transparent"}
            >
              {console.log(selectedChat.id)}
              {console.log(chat.id)}
              <FontAwesomeIcon
                icon={faComment}
                size={"sm"}
                onClick={onOpen}
                cursor={"pointer"}
                style={{ marginRight: "5px" }}
              />

              {chat.name}

            </Box>
          ))}
      </Container>
    </>
  );
};

export default SidebarContacts;
