import {
  Text,
  Drawer,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  DrawerHeader,
  DrawerBody,
  Card,
  Avatar,
  CardBody,
  Image,
  CardHeader,
  Container,
  useColorModeValue,
} from "@chakra-ui/react";
import { useContext, useEffect, useState } from "react";
import { getUserByHandle } from "../../services/users.service";
import AppContext from "../../context/AuthContext";
import { useNavigate } from "react-router-dom";
import { removeSpecificNotification } from "../../services/notifications.service";
import { db } from "../../config/firebase";
import { ref, off, onValue } from "firebase/database";
import { groupBy } from "../../services/channels.service";

const Notifications = ({ isOpen, onClose, isNotification }) => {
  const [notification, setNotification] = useState([]);
  const [users, setUsers] = useState(new Map());
  const [groupByChannels, setGroupByChannels] = useState(null);
  const { userData } = useContext(AppContext);
  // const { updateNotificationCount } = useContext(NotificationContext);
  const navigate = useNavigate();

  useEffect(() => {
    const notificationRef = ref(db, `notifications`);

    const onNotificationAdded = onValue(notificationRef, (snapshot) => {
      const newNotification = snapshot.val();
      setNotification((_) => {
        const updatedNotifications = Object.entries(newNotification).map(
          ([key, value]) => {
            return {
              ...value,
              notificationId: key,
            };
          }
        );
        const filteredNotifications = updatedNotifications.filter(
          (notification) => notification.idUser === userData.uid
        );

        return filteredNotifications;
      });
    });

    return () => {
      off(notificationRef, onNotificationAdded);
    };
  }, [userData?.uid]);

  useEffect(() => {
    if (notification.length > 0) {
      const fetchUserPromises = notification.map((notificationItem) =>
        getUserByHandle(notificationItem.fromWho)
      );

      Promise.all(fetchUserPromises)
        .then((snapshots) => {
          const newUsersMap = new Map(users);
          snapshots.forEach((snapshot) => {
            const user = snapshot.val();
            if (user) {
              newUsersMap.set(user.userId, user);
            }
          });

          setUsers(newUsersMap);

          const groupedNotifications = groupBy(
            notification,
            (obj) => obj.channel
          );
          setGroupByChannels(groupedNotifications);
        })
        .catch((error) => {
          console.error("Error fetching users:", error);
        });
    }
  }, [notification]);

  const removeNotification = (id) => {
    console.log(id);
    const specificChannel = groupByChannels.get(id);
    console.log(groupByChannels);
    console.log(specificChannel);
    console.log(specificChannel[0].teamInfo.listOfParticipants);
    if (specificChannel[0].teamInfo.listOfParticipants.length === 2) {
      navigate(`/friends/${id}`);
    } else {
      navigate(`/channel/${id}`);
    }

    removeSpecificNotification(specificChannel);
    onClose();
  };

  return (
    <>
      {notification.length > 0 ? (
        <Drawer isOpen={isOpen} placement="right" onClose={onClose} size={"sm"}>
          <DrawerOverlay />
          <DrawerContent>
            <DrawerCloseButton color={useColorModeValue("white" ,"#292F3F")} />
            <DrawerHeader color={"white"} bg={"#00B998"}>Notifications</DrawerHeader>
            <DrawerBody
              color={"whiteAlpha.900"}
              fontSize={"20px"}
              fontWeight={"bold"}
              bg={useColorModeValue("white" ,"#292F3F")}
              sx={{
                "&::-webkit-scrollbar": {
                  width: "5px",
                },
                "&::-webkit-scrollbar-thumb": {
                  background: "#00B998",
                  borderRadius: "5px",
                },
                "&:hover::-webkit-scrollbar": {
                  width: "5px",
                },
              }}
            >
              {notification.map((obj) => {
                const user = users.get(obj.userId);
                const time = new Date(obj.created_at).toLocaleString();
                console.log(obj);
                return (
                  <Card
                    direction={{ base: "column", sm: "col" }}
                    overflow={"hidden"}
                    variant={"outline"}
                    key={obj.notificationId}
                    m={"10px 0px"}
                    p={"10px"}
                    onClick={() =>
                      removeNotification(obj.channel, obj.notificationId)
                    }
                    cursor={"pointer"}
                    borderColor={"#00B998"}
                    bg={useColorModeValue("white" ,"#1F232F")}
                    // background={"#1F232F"}
                    boxShadow={"0 0 10px rgba(0, 0, 0, 0.3)"}
                    transition={
                      "box-shadow 0.3s ease-out, transform 0.3s ease-out"
                    }
                    _hover={{
                      bg: "#00B998",
                      color: "white",
                      boxShadow: "0 10px 20px rgba(0, 185, 152, 0.4)",
                      transform: "translateY(-5px)",
                    }}
                  >
                    {user && (
                      <CardHeader
                        p={"5px"}
                        display={"flex"}
                        alignItems={"center"}
                        justifyContent={"space-between"}
                      >
                        <Container
                          display={"flex"}
                          alignItems={"center"}
                          w={"59%"}
                          p={"0px"}
                        >
                          <Avatar
                            name={user.handle}
                            src={user.profileImageUrl}
                            size={"sm"}
                            mt={"2px"}
                          />
                          <Text ml={"10px"}>{user.handle}</Text>
                        </Container>
                        <Text fontSize={"13px"}>{time}</Text>
                      </CardHeader>
                    )}
                    <CardBody p={"5px 5px"}>
                      {obj.type === "text" ? (
                        <Container w={"full"} p={"0px"}>
                          <Text>Missed message :</Text>
                          {obj.missedMessage}
                          {console.log("tuk")}
                        </Container>
                      ) : obj.type === "gif" ? (
                        <Image
                          src={obj.missedMessage}
                          w={"200px"}
                          h={"200px"}
                        />
                      ) : obj.type === "image" ? (
                        <Image src={obj.missedMessage.url} />
                      ) : (
                        <Text>{obj.missedMessage.url}</Text>
                      )}
                    </CardBody>
                  </Card>
                );
              })}
            </DrawerBody>
          </DrawerContent>
        </Drawer>
      ) : (
        <Drawer isOpen={isOpen} placement="right" onClose={onClose} size={"sm"}>
          <DrawerOverlay />
          <DrawerContent>
            <DrawerCloseButton />
            <DrawerHeader
              bg={"#00B998"}
              fontSize={"20px"}
              fontWeight={"bold"}
              color={"whiteAlpha.900"}
            >
              Notifications
            </DrawerHeader>
            <DrawerBody bg={useColorModeValue( "gray.50","#292F3F")} fontSize={"16px"} fontWeight={"bold"}>
              There are no notifications
            </DrawerBody>
          </DrawerContent>
        </Drawer>
      )}
    </>
  );
};
export default Notifications;