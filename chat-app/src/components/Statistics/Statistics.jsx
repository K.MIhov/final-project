import { useEffect, useState } from "react";
import { ref, onValue } from 'firebase/database';
import { db } from '../../config/firebase';
import { Box, Flex, Text, HStack, Divider, Heading } from "@chakra-ui/react";
import { TeamOutlined, CommentOutlined, CoffeeOutlined, BarChartOutlined } from "@ant-design/icons";
import { Pie } from '@ant-design/charts';

const Statistics = () => {
    const [memberCount, setMemberCount] = useState(0);
    const [teamsCount, setTeamsCount] = useState(0);
    const [channelsCount, setChannelsCount] = useState(0);

    useEffect(() => {
        const dbRef = ref(db, 'users');
        const unsubscribe = onValue(dbRef, (snapshot) => {
            const firebaseData = snapshot.val();
            if (firebaseData) {
                const numUsers = Object.keys(firebaseData).length
                setMemberCount(numUsers)
            }
        }, (error) => {
            console.error('Error', error)
        })

        return () => unsubscribe();
    }, [])

    useEffect(() => {
        const dbRef = ref(db, 'channels');
        const unsubscribe = onValue(dbRef, (snapshot) => {
            const firebaseData = snapshot.val();
            if (firebaseData) {
                const numChannels = Object.keys(firebaseData).length;
                setChannelsCount(numChannels);
            }
        }, (error) => {
            console.error("Error", error)
        })

        return () => unsubscribe();
    }, [])


    useEffect(() => {
        const dbRef = ref(db, 'teams');
        const unsubscribe = onValue(dbRef, (snapshot) => {
            const firebaseData = snapshot.val();
            if (firebaseData) {
                const numTeams = Object.keys(firebaseData).length;
                setTeamsCount(numTeams);
            }
        }, (error) => {
            console.error("Error", error)
        })

        return () => unsubscribe();
    }, [])


    const data = [
        { type: 'Members', value: memberCount },
        { type: 'Teams', value: teamsCount },
        { type: 'Channels', value: channelsCount },
    ];

    var config = {
        appendPadding: 10,
        data: data,
        angleField: 'value',
        colorField: 'type',
        radius: 0.8,
        label: {
            type: 'outer',
            content: '{name} {percentage}',
            style: {
                fill: '#ffffff',
                fontSize: 14,
            },
        },
        legend: {
            itemName: {
                style: {
                    fill: '#ffffff',
                    fontSize: 14,
                },
            },
        },
        interactions: [{ type: 'element-active' }],
    };






    return (
        <Flex alignItems="center" justifyContent="center" pb="50px" pt="35px">
            <Box
                borderRadius={'5px'}
                border={'1px'}
                borderColor={'blue.300'}
                position={'relative'}
                cursor={'pointer'}
                boxShadow={'0 0 10px rgba(0, 0, 0, 0.3)'}
                p="20px"
                w={{ base: '80%', md: '30%' }}
            >
                <Box>
                    <BarChartOutlined />
                    <Heading mb="10px" size="md" pl="35%">
                        Chatify Statistics
                    </Heading>
                </Box>
                <Divider mb="10px" />
                <Flex direction="column" alignItems="center">
                    <Pie {...config} />
                </Flex>
                <HStack justify="space-between" alignItems="center">
                    <Box>
                        <TeamOutlined />
                        <Text fontSize="lg" fontWeight="bold">
                            Total users
                        </Text>
                        <Text fontSize="2xl" fontWeight="bold">
                            {memberCount}
                        </Text>
                    </Box>
                    <Box>
                        <CommentOutlined />
                        <Text fontSize="lg" fontWeight="bold">
                            Total teams
                        </Text>
                        <Text fontSize="2xl" fontWeight="bold">
                            {teamsCount}
                        </Text>
                    </Box>
                    <Box>
                        <CoffeeOutlined />
                        <Text fontSize="lg" fontWeight="bold">
                            Total channels
                        </Text>
                        <Text fontSize="2xl" fontWeight="bold">
                            {channelsCount}
                        </Text>
                    </Box>
                </HStack>
            </Box>
        </Flex>
    );
};



export default Statistics;