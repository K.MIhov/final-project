import {
    Accordion,
    AccordionItem,
    AccordionButton,
    AccordionPanel,
    Box,
    useToast,
    Text,
    Button
} from "@chakra-ui/react";
import FAQData from "../../data/FAQ";
import { nextId } from "../../common/keys";
import { FaThumbsUp, FaThumbsDown } from 'react-icons/fa';

const AccordionComponent = ({ activate }) => {
    const toast = useToast();

    return (
        <Accordion
            // mt={"20%"}
            allowToggle
            // h="100vh"
            // boxShadow={'0 0 10px rgba(0, 0, 0, 0.2)'}
        >
            {FAQData.map((item) => {
                if (activate === item.id) {
                    return (
                        <AccordionItem key={nextId()}
                            border={"1px"}
                            borderColor={"gray.700"}
                            borderRadius={"10px 10px 10px 10px"}
                            mb={'5px'}
                            boxShadow={'0 0 10px rgba(0, 0, 0, 0.2)'}
                        >
                            <h2>
                                <AccordionButton
                                    _expanded={{ bg: 'green.300', color: 'white' }}
                                >
                                    <Box as="span" flex="1" textAlign="left" color="gray.700" >
                                        {item.questions}
                                    </Box>
                                </AccordionButton>
                            </h2>
                            <AccordionPanel pb={8} color="black">
                                {item.answer}
                                <Box mt={4}>
                                    <Text> Was this answer helpful ? </Text>
                                    <Button
                                        variant="outline"
                                        colorScheme="green"
                                        size="sm"
                                        onClick={() => {
                                            toast({
                                                title: "Thank you for your feedback!",
                                                status: "success",
                                                duration: 3000,
                                                isClosable: true
                                            })
                                        }}
                                        style={{ marginRight: "0.5em" }}>
                                        <FaThumbsUp style={{ display: "inline-flex", alignItems: "center" }} />
                                    </Button>
                                    <Button
                                        variant="outline"
                                        colorScheme="red"
                                        size="sm"
                                        onClick={() => {
                                            toast({
                                                title: "Sorry for your experience :(",
                                                status: "warning",
                                                duration: 3000,
                                                isClosable: true
                                            })
                                        }}
                                        style={{ marginLeft: "0.5em" }}>
                                        <FaThumbsDown style={{ display: "inline-flex", alignItems: "center" }} />
                                    </Button>
                                </Box>
                            </AccordionPanel>
                        </AccordionItem>
                    )
                }
            })}
        </Accordion>
    );
};

export default AccordionComponent;
