import {
    Heading,
    Avatar,
    Box,
    Center,
    Text,
    Stack,
    Button,
    useColorModeValue,
} from '@chakra-ui/react';
import { useContext } from 'react';
import AppContext from '../../context/AuthContext';
import { useNavigate } from 'react-router-dom';
import { logoutUser } from "../../services/auth.service";
import { CHANNEL_PAGE } from '../../common/constants';

export default function SocialProfileSimple() {
    const { userData } = useContext(AppContext);
    const navigate = useNavigate();
    return (
        <Center >
            <Box
                // maxW={'320px'}
                w={'full'}
                bg={useColorModeValue('gray.50', 'gray.800')}
                boxShadow={'2xl'}
                rounded={'lg'}
                p={6}
                textAlign={'center'}>
                {userData && (
                    <Avatar
                        size={'xl'}
                        src={userData.profileImageUrl}
                        alt={'Avatar Alt'}
                        mb={4}
                        pos={'relative'}
                        border={"2px solid #00B998"}
                        // boxShadow="0 10px 15px rgba(0, 185, 152, 0.4)"
                        _after={{
                            content: '""',
                            w: 4,
                            h: 4,
                            bg: 'green.300',
                            border: '2px solid white',
                            rounded: 'full',
                            pos: 'absolute',
                            bottom: 0,
                            right: 3,
                        }}
                    />
                )}
                {userData && (
                    <Heading fontSize={'2xl'} fontFamily={'body'}>
                        {userData.displayName}
                    </Heading>
                )}
                {userData && (
                    <Text fontWeight={600} color={'gray.500'} mb={4}>
                        {userData.email}
                    </Text>
                )}

                <Stack mt={8} direction={'row'} spacing={4}>
                    <Button
                        onClick={logoutUser}
                        flex={1}
                        bg={"red"}
                        fontSize={'sm'}
                        rounded={'full'}
                        _focus={{
                            bg: 'gray.200',
                        }}>
                        Logout
                    </Button>
                    <Button
                        onClick={() => navigate(`${CHANNEL_PAGE}`)}
                        flex={1}
                        fontSize={'sm'}
                        rounded={'full'}
                        bg="#00B998"
                        color={'white'}
                        boxShadow="0 5px 5px rgba(0, 185, 152, 0.4)"
                        _hover={{
                            bg: 'green.200',
                        }}
                        _focus={{
                            bg: 'blue.500',
                        }}>
                        Continue
                    </Button>
                </Stack>
            </Box>
        </Center>
    );
}