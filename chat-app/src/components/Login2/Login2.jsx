import { ArrowBackIcon } from "@chakra-ui/icons";
import {
    Box,
    Button,
    Checkbox,
    Flex,
    FormControl,
    FormLabel,
    HStack,
    Heading,
    IconButton,
    Image,
    Input,
    InputGroup,
    InputRightElement,
    Link,
    Stack,
    Text,
    Tooltip,
    useColorModeValue,
    useToast
} from '@chakra-ui/react';
import { browserLocalPersistence, browserSessionPersistence, setPersistence } from 'firebase/auth';
import { useContext, useState } from "react";
import { FiEye, FiEyeOff } from 'react-icons/fi';
import { useNavigate } from "react-router-dom";
import { auth } from "../../config/firebase";
import AppContext from "../../context/AuthContext";
import { loginUser } from "../../services/auth.service";
import { HOME_PAGE, SIGN_UP_PAGE, TERMS_PAGE, toasted } from "../../common/constants";
import SocialProfileSimple from './UserExists';

export default function SplitScreen() {
    return (
        <Stack  h={'100vh'} minW={'100vh'} direction={{ base: 'column', md: 'row' }} bg={useColorModeValue('gray.100', 'gray.800')}>
            <Flex p={8} flex={1} align={'center'} justify={'center'}>
                <SimpleCard></SimpleCard>
            </Flex>
            <Flex flex={1}>
                <Image
                display={{base: "none", xl: "block"}}
                    alt={'Login Image'}
                    objectFit={'cover'}
                    src={
                        'https://i.ibb.co/3y40dLn/side-view-employee-working-laptop.jpg'
                    }
                />
            </Flex>
        </Stack>
    );
}

function SimpleCard() {
    const [showPassword, setShowPassword] = useState(false);
    const [rememberMe, setRememberMe] = useState(false);
    const { setUser } = useContext(AppContext);
    const navigate = useNavigate();
    const toast = useToast();

    const [form, setForm] = useState({
        email: '',
        password: '',
    });
    const handleRememberMe = () => {
        setRememberMe(!rememberMe);
    }
    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    }

    const updateForm = prop => e => {
        setForm({
            ...form,
            [prop]: e.target.value,
        });
    };
    // const onLoginWithGoogle = () => {
    //     signInWithPopup(auth, googleProvider)
    //         .then(() => {
    //             toast(toasted[6]);
    //             navigate('/');
    //         })
    //         .catch((err) => {
    //             console.error(err);
    //         });
    // };

    const onLogin = () => {
        if (!form.email || !form.password) {
            toast(toasted[5]);
            return;
        }
        // const persistenceType = rememberMe ? browserLocalPersistence : browserSessionPersistence;
        // doesn't work

        if (rememberMe) {
            console.log(rememberMe);
            setPersistence(auth, browserLocalPersistence)
                .then(() => {
                    return loginUser(form.email, form.password);
                })
                .then((userCredential) => {
                    setUser({
                        user: userCredential.user,
                    });
                    toast(toasted[6]);
                    navigate(`/channel`);
                })
                .catch((error) => {
                    console.error(error);
                    toast(toasted[5]);
                });
        } else {
            setPersistence(auth, browserSessionPersistence)
                .then(() => {
                    return loginUser(form.email, form.password);
                })
                .then((userCredential) => {
                    setUser({
                        user: userCredential.user,
                    });
                    toast(toasted[6]);
                    navigate(`/channel`);
                })
                .catch((error) => {
                    console.error(error);
                    toast(toasted[5]);
                });
        }
    };

    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('gray.100', 'gray.800')}>
            <Stack spacing={8} mx={'auto'} maxW={'lg'} w={'lg'} py={12} px={6}>
                <Stack align={'center'}>
                    <Heading fontSize={'4xl'}>Sign in to your account</Heading>
                    <Text fontSize={'lg'} color={'gray.600'}>
                        to enjoy all of our newest <Link color={"#00B998"}>features</Link>
                    </Text>
                </Stack>
                <Box
                    rounded={'lg'}
                    bg={useColorModeValue('white', 'gray.700')}
                    boxShadow="0 5px 5px rgba(0, 185, 152, 0.4)"
                    p={8}
                >
                    {auth.currentUser ?
                        (<SocialProfileSimple />
                        ) : (
                            <Stack spacing={4} >
                                <HStack>
                                    <Tooltip label="Back">
                                        <ArrowBackIcon onClick={() => navigate(`${HOME_PAGE}`)} boxSize={5} cursor="pointer" />
                                    </Tooltip>
                                    <Text fontWeight="bold" cursor="pointer" onClick={() => navigate(`${HOME_PAGE}`)}>Home</Text>
                                </HStack>
                                {console.log(`auth:`, auth.currentUser)}
                                <FormControl id="email" isRequired>
                                    <FormLabel>Email address</FormLabel>
                                    <Input
                                        type="text"
                                        name="email"
                                        value={form.email}
                                        onChange={updateForm('email')}
                                    />
                                </FormControl>
                                <FormControl isRequired id="password">
                                    <FormLabel>Password</FormLabel>
                                    <InputGroup>
                                        <Input
                                            name="password"
                                            value={form.password}
                                            onChange={updateForm('password')}
                                            type={showPassword ? "text" : "password"}
                                        />
                                        <InputRightElement>
                                            <IconButton
                                                borderRadius={"25% 25%"}
                                                aria-label="toggle password visibility"
                                                onClick={togglePasswordVisibility}
                                                icon={showPassword ? <FiEye /> : <FiEyeOff />}
                                                size="md"
                                            />
                                        </InputRightElement>
                                    </InputGroup>
                                </FormControl>
                                <Stack spacing={10}>
                                    <Stack
                                        direction={{ base: 'column', sm: 'row' }}
                                        align={'start'}
                                        justify={'space-between'}>
                                        <Checkbox onChange={handleRememberMe}>Remember me</Checkbox>
                                        <Link color={'#00B998'} fontWeight={"bold"}>Forgot password?</Link>
                                    </Stack>
                                    <Button
                                        // bg={'blue.400'}
                                        color={'white'}
                                        bg="#00B998"
                                        boxShadow="0 5px 10px rgba(0, 185, 152, 0.4)"
                                        onClick={onLogin}>
                                        Sign in
                                    </Button>
                                </Stack>
                                {/* <Button
                        // bg={'blue.400'}
                        color={'white'}
                        onClick={onLoginWithGoogle}
                        bg="#00B998" 
                        boxShadow="0 5px 10px rgba(0, 185, 152, 0.4)"
                        _hover={{
                            bg: 'blue.500',
                        }}>
                        <GoogleOutlined style={{ fontSize: '24px' }} />
                        <Text>  Sign in with Google</Text>

                    </Button> */}
                                <Link
                                    onClick={() => navigate(`${SIGN_UP_PAGE}`)}
                                    display="flex"
                                    justifyContent="center"
                                    fontWeight="bold"
                                    cursor={"pointer"}
                                >
                                    Create account for free  ✌️
                                </Link>
                                <Link
                                    onClick={() => navigate(`${TERMS_PAGE}`)}
                                    display="flex"
                                    justifyContent="center" mt={2}
                                >
                                    Terms of Use and Privacy Policy
                                </Link>
                            </Stack>
                        )}
                </Box>
            </Stack>
        </Flex>
    );
}
