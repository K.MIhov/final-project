import {
    Grid,
} from "@chakra-ui/react";

import NavBar from "../NavBar/NavBar"
import SidebarCalls from "./SideBarCalls";
import CallHistory from "./CallHistory";
const Calls = () => {
    const displayAttribute = { base: "none", xl: "block" }
    return (
        <Grid templateColumns={{ base: "1fr", xl: "7% 23% 70%", "2xl": "5% 25% 70%" }} gap={0}>
            <NavBar></NavBar>
            <SidebarCalls />
            <CallHistory displayAttribute={displayAttribute} />
        </Grid>

    )
}

export default Calls;
