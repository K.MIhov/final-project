import {
  Avatar,
  AvatarBadge,
  Box,
  Button,
  Container,
  Divider,
  HStack,
  Spacer,
  Text,
  VStack,
  useColorModeValue
} from "@chakra-ui/react";
import {
  MeetingProvider,
} from "@videosdk.live/react-sdk";
import { ChevronRightIcon } from '@chakra-ui/icons'
import { useContext, useState } from "react";
import { BsTelephone } from "react-icons/bs";
import AppContext from "../../context/AuthContext";
import { authToken, createMeeting } from "../../services/generateToken";
import MeetingView from "../Meeting/MeetingView";
import { requestSingleUserCall } from "../../services/call.service";
import { useNavigate } from "react-router-dom";

export default function SidebarCalls() { 
  const [meetingId, setMeetingId] = useState(null);
  const [user, setUser] = useState(null);
  const { userData, friendList } = useContext(AppContext);
 const navigate = useNavigate();
  const handleCall = async (friend) => {
      const id = await createMeeting({token: authToken});
      setUser(friend);
      // console.log(friend)
      requestSingleUserCall(userData.handle, friend, id)
      .then(() => setMeetingId(id))
      .catch((err) =>  console.error(`Error ocurred while trying to reach the user: ${err.message}`))

  }
  const onMeetingLeave = () => {

      setMeetingId(null);
    };

    return (
      <>
        <Container bg={useColorModeValue('gray.100',"#1F232F")} h={"100vh"} borderRight={useColorModeValue("1px solid #D9D9D9" ,"1px solid #292F3F")}{...containerStyles}>
          <VStack  
          borderBottom={"1px"}
          borderColor={useColorModeValue("gray.300","#292F3F")} p="10px" spacing={1}>
            <Text color={useColorModeValue('gray.800',"white")} {...textStyles}>
              CONTACTS</Text>
              <HStack
              w="100%"
              // justifyContent="flex-start"
              >
            <Text
            // w="100%"
            pl="10px"
            fontWeight={"bold"}
            cursor={"pointer"}
            onClick={() => navigate("/call-history")}
              // p="5px 10px"
            >HISTORY</Text>
            <ChevronRightIcon/>
            </HStack>
          </VStack>
          <Box>
            {console.log(friendList)}
            {userData && friendList && (
              <Box >
                {friendList.map((friend, index) => (
                  <Box key={index} mt="20px" ml="10px" w="95%" color={"white"}>
                          <HStack p="10px" bg="#00B998" borderRadius={"10px 10px 10px 10px"}>
                      {friend.connections ? (
                        <Avatar
                        border={"1px solid white"}
                          name={friend.handle}
                          src={friend.profileImageUrl}
                          {...avatarStyles}
                        >
                          <AvatarBadge boxSize="1em" bg="green.500" />
                        </Avatar>
                      ) : (
                        <Avatar
                          name={friend.handle}
                          src={friend.profileImageUrl}
                          {...avatarStyles}
                        />
                      )}
                      <Box>
                        <VStack align="flex-start">
                          <Text fontWeight="bold" mb="-10px" mt="5px">
                            {friend.handle}
                          </Text>
                          {friend.connections ? (
                            <Box>
                              <Text>Online</Text>
                            </Box>
                          ) : (
                            <Box>
                              <Text>Offline</Text>
                            </Box>
                          )}
                        </VStack>
                      </Box>
                      <Spacer />
                      <Button
                      boxShadow={"lg"}
                      bg="green.600"
                        onClick={() => handleCall(friend.handle)}
                        {...buttonStyles}
                      >
                        <BsTelephone style={{ fontSize: "15px" }} />
                      </Button>
                    </HStack>
                    <Divider mt="20px" orientation="horizontal" />
                  </Box>
                ))}
              </Box>
            )}
            {authToken && meetingId && (
              <MeetingProvider
                config={{
                  meetingId,
                  micEnabled: true,
                  webcamEnabled: true,
                  name: userData.handle,
                }}
                token={authToken}
              >
                <MeetingView
                  user={user}
                  meetingId={meetingId}
                  onMeetingLeave={onMeetingLeave}
                />
              </MeetingProvider>
            )}
          </Box>
        </Container>
      </>
    );
}

const containerStyles = {
  maxW: "full",
  // zIndex: 50,
  // bg: "#1F232F",
  p: "5px 10px",
  maxH: "100vh",
  overflowY: "auto",
  // borderRight: "1px solid #292F3F",
  // borderRadius: "10px 10px 10px 10px",
  sx: {
    "&::-webkit-scrollbar": {
      width: "0px",
    },
    "&::-webkit-scrollbar-thumb": {
      background: "#00B998",
      borderRadius: "10px",
    },
    "&:hover::-webkit-scrollbar": {
      width: "5px",
    },
  },
};

const textStyles = {
  // color: "white",
  fontSize: "18px",
  fontWeight: "bold",
  w: "100%",
  p: "5px 10px",
  mt: "2px",
  borderRadius: "5px",
};

const avatarStyles = {
  border: "2px solid white",
  style: { width: "60px", height: "60px" },
  mr: "15px",
};

const buttonStyles = {
  borderRadius: "50px 50px",
};

// {activeCall && (
//     authToken && meetingId ? (
//         <MeetingProvider
//         config={{
//           meetingId,
//           micEnabled: true,
//           webcamEnabled: true,
//           name: userData.handle,
//         }}
//         token={authToken}
//       >
//     <MeetingView user={user}  setActiveCall={setActiveCall} meetingId={meetingId} onMeetingLeave={onMeetingLeave} />

//       </MeetingProvider>
//    ) : (
//        <JoinScreen setActiveCall={setActiveCall} getMeetingAndToken={getMeetingAndToken} />
//      ) 
//     )}

// const getMeetingAndToken = async (id) => {
//     console.log(id);
//     const meetingId =
//       id == null ? await createMeeting({ token: authToken }) : id;
//     setMeetingId(meetingId);
//     // setCallId(meetingId);
//   };

//   //This will set Meeting Id to null when meeting is left or ended
//   const onMeetingLeave = () => {
//     setMeetingId(null);
//   };
// {showCall && (authToken && meetingId ? (
                      
//     <MeetingProvider
//         config={{
//             meetingId,
//             micEnabled: true,
//             webcamEnabled: true,
//             name: userData.handle,
//         }}
//         token={authToken}
//     >{console.log(showCall, authToken, meetingId)}
//         <MeetingView meetingId={meetingId} onMeetingLeave={onMeetingLeave} />
//     </MeetingProvider>
// ) : (
//     <JoinScreen userHandle={userHandle} setShowCall={setShowCall} getMeetingAndToken={getMeetingAndToken} />
// ))}


  // useEffect(() => {
  //     // setLoading(true);
  //     if (userData.friendList) {
  //         const fetchFriendList = async () => {
  //             const friendList = await Promise.all(
  //                 Object.keys(userData.friendList).map((user) => {
  //                     return getUserByHandle(user).then((snapshot) => snapshot.val());
  //                 })
  //             );
  //             setFriendList(friendList);
  //             // setLoading(false);
  //         };

  //         fetchFriendList();
  //     }
  // }, [userData.friendList])