import {
    Box,
    Heading,
    Divider,
    VStack,
    Image,
} from "@chakra-ui/react"


export default function NoCallHistory() {
    return (
        <>
        <Box  mt={"2%"} display="flex" justifyContent={"center"}>
            <VStack>
                <Heading size={"lg"} color="gray.600">
                    Hey There!
                </Heading>
                <Divider mt="20px" orientation='horizontal' />
            </VStack>
        </Box>
        <Box display="flex" mt="10%" justifyContent={"center"} >
            <Image w="30%" src="https://i.ibb.co/mHH5YjS/White-Chatting-Letter-Initial-Logo-7.png"></Image>
        </Box>
    </>
    )
}