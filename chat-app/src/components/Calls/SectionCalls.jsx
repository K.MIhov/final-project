import {
  Avatar,
  Box,
  Divider,
  HStack,
  Spacer,
  Text,
  Button,
  useColorModeValue
} from "@chakra-ui/react";
import moment from "moment";
import { useContext, useEffect, useState } from "react";
import { BsTelephoneInbound, BsTelephoneOutbound } from "react-icons/bs";
import AppContext from "../../context/AuthContext";
import { getUserByHandle } from "../../services/users.service";
import PropTypes from "prop-types";
import Loader from "../Loader/Loader";

const SectionCalls = ({ section }) => {
  const [allCallHistory, setAllCallHistory] = useState([]);
  const [incomingCallHistory, setIncomingCallHistory] = useState([]);
  const [outgoingCallHistory, setOutgoingCallHistory] = useState([]);
  const [callLimit, setCallLimit] = useState(7);
  const [showMore, setShowMore] = useState(false);
  const { userData } = useContext(AppContext);

  useEffect(() => {
    (async () => {
      if (userData) {
        const incomingCalls = [];
        const outgoingCalls = [];
        const allCalls = [];

        await Promise.all(
          Object.values(userData.callHistory).map(async (data) => {
            const userSnapshot = await getUserByHandle(data.user);
            const user = userSnapshot.val();

            const callEntry = {
              user: user,
              type: data.type,
              timestamp: data.createdOn,
            };

            if (data.type === "Incoming") {
              incomingCalls.unshift(callEntry);
            } else if (data.type === "Outgoing") {
              outgoingCalls.unshift(callEntry);
            }
            allCalls.unshift(callEntry);
          })
        );
        setAllCallHistory(allCalls);
        setIncomingCallHistory(incomingCalls);
        setOutgoingCallHistory(outgoingCalls);
      }
    })();
  }, [userData]);

  const showMoreCalls = () => {
    setShowMore(true);
    setCallLimit(callLimit + 7);
  };

  if(allCallHistory.length === 0){
    return (
      <Loader/>
    )
  }

  return (
    <>
      <Box pl="2%"  w="95%" color={useColorModeValue("#1F232F", 'gray.100')}>
        {section === "All" && allCallHistory.length > 0 && (
          <>
            {allCallHistory.slice(0, callLimit).map((call, index) => (
              <Box  key={index} mt={"20px"} ml={"10px"}>
                <HStack pb="10px" >
                  <Avatar
                    display={{ base: "none", sm: "block" }}
                    name={call.user.displayName}
                    mr="20px"
                    src={call.user.profileImageUrl}
                    border="1px solid #00B998"
                  />
                  {call.type === "Outgoing" ? (
                    <>
                      <BsTelephoneOutbound />
                      <Text pl="10px">{`Outgoing Call`}</Text>
                      <Text fontWeight={"bold"}>{call.user.displayName}</Text>
                    </>
                  ) : (
                    <>
                      <BsTelephoneInbound />
                      <Text pl="10px">{`Incoming Call from`}</Text>
                      <Text fontWeight={"bold"}>{call.user.displayName}</Text>
                    </>
                  )}
                  <Spacer></Spacer>
                  <Text display={{ base: "none", sm: "block" }}
                  >{moment(call.timestamp).calendar()}</Text>
                </HStack>
                <Divider mt="20px" orientation="horizontal" />
              </Box>
            ))}
            {allCallHistory.length > callLimit && (
              <Box display={"flex"} justifyContent={"center"}>
                <Button onClick={showMoreCalls} mt="20px">
                  Show More
                </Button>
              </Box>
            )}
          </>
        )}
        {section === "Incoming" && incomingCallHistory.length > 0 && (
          <>
            {incomingCallHistory.slice(0, callLimit).map((call, index) => (
              <Box key={index} mt={"20px"} ml={"10px"}>
                <HStack pb="10px">
                  <Avatar
                    display={{ base: "none", sm: "block" }}

                    border="1px solid #00B998"
                    mr="20px"
                    name={call.user.displayName}
                    src={call.user.profileImageUrl}
                  ></Avatar>
                  <BsTelephoneInbound />
                  <Text pl="10px">{`Incoming Call from `}</Text>
                  <Text fontWeight={"bold"}>{call.user.displayName}</Text>
                  <Spacer></Spacer>
                  <Text display={{ base: "none", sm: "block" }}
                  >{moment(call.timestamp).calendar()}</Text>
                </HStack>
                <Divider mt="20px" orientation="horizontal" />
              </Box>
            ))}
            {incomingCallHistory.length > callLimit && (
              <Box display={"flex"} justifyContent={"center"}>
                <Button onClick={showMoreCalls} mt="20px">
                  Show More
                </Button>
              </Box>
            )}
          </>
        )}
        {section === "Outgoing" && outgoingCallHistory.length > 0 && (
          <>
            {outgoingCallHistory.slice(0, callLimit).map((call, index) => (
              <Box key={index} mt={"20px"} ml={"10px"}>
                <HStack pb="10px">
                  <Avatar
                    display={{ base: "none", sm: "block" }}
                    border="1px solid #00B998"
                    name={call.user.displayName}
                    mr="20px"
                    src={call.user.profileImageUrl}
                  ></Avatar>
                  <BsTelephoneOutbound />
                  <Text pl="10px">{`Outgoing Call`}</Text>
                  <Text fontWeight={"bold"}>{call.user.displayName}</Text>
                  <Spacer></Spacer>
                  <Text display={{ base: "none", sm: "block" }}
                  >{moment(call.timestamp).calendar()}</Text>
                </HStack>
                <Divider mt="20px" orientation="horizontal" />
              </Box>
            ))}
            {outgoingCallHistory.length > callLimit && (
              <Box display={"flex"} justifyContent={"center"}>
                <Button onClick={showMoreCalls} mt="20px">
                  Show More
                </Button>
              </Box>
            )}
          </>
        )}
      </Box>
    </>
  );
};
const stackStyles = {
  sx: {
    "&::-webkit-scrollbar": {
      width: "0px",
    },
    "&::-webkit-scrollbar-thumb": {
      background: "gray.700",
      borderRadius: "10px",
    },
    "&:hover::-webkit-scrollbar": {
      width: "5px",
    },
  },
}
SectionCalls.propTypes = {
  section: PropTypes.string.isRequired,
};

export default SectionCalls;
