import {
  Box,
  Button,
  Flex,
  HStack,
  Heading,
  Menu,
  Spacer,
  useColorModeValue,
  Text
} from "@chakra-ui/react";
import { useContext, useState } from "react";
import AppContext from "../../context/AuthContext";
import SectionCalls from "./SectionCalls";
import { ChevronLeftIcon } from "@chakra-ui/icons";
import { useNavigate } from "react-router-dom";

export default function CallHistory({ displayAttribute }) {
  const [section, setSection] = useState('All');
  const navigate = useNavigate()
  const { userData } = useContext(AppContext);

  const handleSection = (sect) => {
    setSection(sect);
  }

  return (
    <>
      <Flex {...flexStyles} display={displayAttribute ? displayAttribute : { base: "block", xl: "block" }} as="nav" flexDirection="column" alignItems="flex-start-right">
        <Box {...boxStyles}>
          <Flex
          {...flexStyles2}
            h={"70px"}
            overflowX="auto"
            alignItems="center"
            justifyContent="space-between" >
            <HStack pr="5px">
              <ChevronLeftIcon />
              <Text fontWeight={"bold"} onClick={() => navigate('/calls')} cursor={"pointer"} fontSize={"15px"} display={{ base: "block", xl: "block" }}>CONTACTS</Text>
            </HStack>
            <HStack spacing={{ base: 5, xl: 10 }} alignItems="center">
              <Box borderRight="1px solid white" pl={{ base: "0px", xl: '50px' }}>
                <Heading pl="0px" pr="30px" size="md" cursor={"pointer"} display={{ base: "none", md: "block" }}>HISTORY</Heading>
              </Box>
              <Menu>
                <Button onClick={() => handleSection('All')} {...buttonStyles}>All</Button>
                <Button onClick={() => handleSection('Incoming')} {...buttonStyles} position="relative">Incoming</Button>
                <Button onClick={() => handleSection('Outgoing')} {...buttonStyles} position="relative">Outgoing</Button>
              </Menu>
              <Spacer></Spacer>
            </HStack>
          </Flex>
        </Box>
        <Box style={{ overflowY: "auto", maxHeight: "calc(100vh - 70px)" }}>
          {userData && (
            <SectionCalls section={section} />
          )}
        </Box>
      </Flex>
    </>
  );
}


const flexStyles = {
  maxW: "full",
  color: "white",
  // maxH: "100vh",
  overflowY: "auto",
  // bg: "#292F3F",
  p: "0px",
  sx: {
    "&::-webkit-scrollbar": {
      width: "5px",
    },
    "&::-webkit-scrollbar-thumb": {
      background: "gray.700",
      borderRadius: "10px",
    },
  },
};

const flexStyles2 = {
  sx: {
      "&::-webkit-scrollbar": {
          width: "1px",
          height: "7px"
      },
      "&::-webkit-scrollbar-thumb": {
          background: "gray.700",
          borderRadius: "10px",
      },
  },
};

const boxStyles = {
  bg: '#00B998',
  // borderRadius: "10px 10px 10px 10px",
  boxShadow: "md",
};

const buttonStyles = {
  bg: '#34C7AD',
  _focus: { bg: "#00B998" },
};

