import {
    Box,
    chakra,
    Flex,
    SimpleGrid,
    Stat,
    StatLabel,
    StatNumber,
    useColorModeValue,
} from '@chakra-ui/react';
import { onValue, ref } from 'firebase/database';
import { useEffect, useState } from "react";
import { BsPeople, BsPerson } from 'react-icons/bs';
import { FiServer } from 'react-icons/fi';
import { db } from '../../config/firebase';

function StatsCard(props) {
    const { title, stat, icon } = props;
    return (
        <Stat
            px={{ base: 2, md: 4 }}
            py={'5'}
            shadow={'xl'}
            border={'1px solid #00B998'}
            borderColor={useColorModeValue('gray.800', 'gray.500')}
            rounded={'lg'}>
            <Flex justifyContent={'space-between'}>
                <Box pl={{ base: 2, md: 4 }}>
                    <StatLabel color={"gray.700"}
                        fontWeight={'medium'} isTruncated>
                        {title}
                    </StatLabel>
                    <StatNumber color={"gray.700"}
                        fontSize={'2xl'} fontWeight={'medium'}>
                        {stat}
                    </StatNumber>
                </Box>
                <Box
                    my={'auto'}
                    color={"gray.700"}
                    alignContent={'center'}>
                    {icon}
                </Box>
            </Flex>
        </Stat>
    );
}

export default function BasicStatistics() {
    const [memberCount, setMemberCount] = useState(0);
    const [teamsCount, setTeamsCount] = useState(0);
    const [channelsCount, setChannelsCount] = useState(0);

    useEffect(() => {
        const dbRef = ref(db, 'users');
        const unsubscribe = onValue(dbRef, (snapshot) => {
            const firebaseData = snapshot.val();
            if (firebaseData) {
                const numUsers = Object.keys(firebaseData).length
                setMemberCount(numUsers)
            }
        }, (error) => {
            console.error('Error', error)
        })

        return () => unsubscribe();
    }, [])

    useEffect(() => {
        const dbRef = ref(db, 'channels');
        const unsubscribe = onValue(dbRef, (snapshot) => {
            const firebaseData = snapshot.val();
            if (firebaseData) {
                const numChannels = Object.keys(firebaseData).length;
                setChannelsCount(numChannels);
            }
        }, (error) => {
            console.error("Error", error)
        })
    
        return () => unsubscribe();
    }, [])
    
    


    useEffect(() => {
        const dbRef = ref(db, 'channels'); 
        let numTeams = 0;
    
        const unsubscribe = onValue(dbRef, (snapshot) => {
            const firebaseData = snapshot.val();
    
            if (firebaseData) {
                for (let channelId in firebaseData) {
                    const channelData = firebaseData[channelId];
                    if ('team' in channelData) {
                        numTeams += 1;
                    }
                }
    
                setTeamsCount(numTeams);
            }
        }, (error) => {
            console.error("Error", error)
        })
    
        return () => unsubscribe();
    }, [])
    

    console.log(memberCount, teamsCount, channelsCount)

    // const data = [
    //     { type: 'Members', value: memberCount },
    //     { type: 'Teams', value: teamsCount },
    //     { type: 'Channels', value: channelsCount },
    // ];

    return (
        <Box maxW="xl" mx={'auto'} pt={5} px={{ base: 2, sm: 12, md: 17 }}>
            <chakra.h1
                textAlign={'center'}
                color={"gray.700"}
                fontSize={'4xl'}
                py={10}
                fontWeight={'bold'}>
                Our company is expanding, you could be too.
            </chakra.h1>
            <SimpleGrid spacing={{ base: 5, lg: 8 }}>
                <StatsCard
                    title={'Members'}
                    stat={memberCount}
                    icon={<BsPerson size={'3em'} />}
                />
                <StatsCard
                    title={'Teams'}
                    stat={teamsCount}
                    icon={<BsPeople size={'3em'} />}
                />
                <StatsCard
                    title={'Channels'}
                    stat={channelsCount}
                    icon={<FiServer size={'3em'} />}
                />
            </SimpleGrid>
        </Box>
    );
}