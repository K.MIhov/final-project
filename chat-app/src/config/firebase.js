// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { GoogleAuthProvider, getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import {getStorage} from 'firebase/storage'


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDc5AQ2I0AkPtrwLzVF_WYN_wPT2GN0kHA",
  authDomain: "chat-app-6396a.firebaseapp.com",
  projectId: "chat-app-6396a",
  storageBucket: "chat-app-6396a.appspot.com",
  messagingSenderId: "714766282906",
  appId: "1:714766282906:web:a751aa63ebd81c277a2f23",
  databaseURL: "https://chat-app-6396a-default-rtdb.europe-west1.firebasedatabase.app/"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
// the Firebase authentication handler
export const auth = getAuth(app);
// the Realtime Database handler
export const db = getDatabase(app);
// Google Auth
export const googleProvider = new GoogleAuthProvider();
// storage
export const storage = getStorage(app)
