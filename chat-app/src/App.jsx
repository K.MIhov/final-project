/* eslint-disable react-hooks/exhaustive-deps */
import { getDatabase, onDisconnect, onValue, push, ref, serverTimestamp, set } from 'firebase/database';
import { useEffect, useState } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { Route, Routes } from "react-router-dom";
import Channel from './components/Channel/Channel';
import GroupChat from './components/GroupChat/GroupChat';
import Home from './components/Home/Home';
import Landing from './components/Landing/Landing';
import MainLayout from './components/MainLayout/MainLayout';
import { auth } from './config/firebase';
import AppContext from './context/AuthContext';
import { getUserData } from './services/users.service';
import About from './views/About/About';
import Careers from './views/Careers/Careers';
import ContactUs from './views/ContactUs/ContactUs';
import FAQ from './views/FAQ/FAQ';
import TermsAndRules from './views/Terms/Terms';
import Calls from './components/Calls/Calls';
import SidebarWithHeader from './components/EditProfile/EditProfile';
import FriendsView from './components/Friends/Friends';
import ErrorPage from './views/ErrorPage/ErrorPage';
import CallHistory from './components/Calls/CallHistory';
import FriendsNavBar from './components/Friends/FriendsNavBar';
import SplitScreen from './components/Login2/Login2';
import SplitScreen2 from './components/SignUp2/SignUp2';
import { getUserFriendList } from './services/users.service';
import Calendar from './views/Calendar/Calendar';
import Chess from './views/Chess/Chess';
import Draw from './views/Draw/Draw';
import Fun from './views/Fun/Fun';
import Music from './views/Music/Music';
import PrivacyPolicy from './views/Privacy/Privacy';
import YouTube from './views/Youtube/Youtube';
function App() {
  const [user] = useAuthState(auth);
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
    userFriends: null,
  });
  useEffect(() => {
    if (user !== null) {
      setAppState({
        ...appState,
        user: user
      });

    }
  }, [user]);

  useEffect(() => {

    if (appState.user === null) return;
    getUserData(appState.user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error("Something went wrong!");
        }

        setAppState({
          ...appState,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
        });
      })
      .catch((e) => alert(e.message));
  }, [appState.user]);

  useEffect(() => {
    if (!appState.userData) return;
    if (appState.userData.friendList !== 'none' && appState.userData.friendList) {
      getUserFriendList(appState.userData.friendList)
        .then((result) => setAppState({
          ...appState,
          friendList: result
        }))
        .catch((err) => console.err(`Error ocurred while fetching FriendList ${err.message}.`))
    }
  }, [appState.userData])

  useEffect(() => {
    if (!appState.userData) {
      return;
    }
    const db = getDatabase();
    const myConnectionsRef = ref(db, `users/${appState.userData.handle}/connections`);

    const lastOnlineRef = ref(db, `users/${appState.userData.handle}/lastOnline`);
    const connectedRef = ref(db, '.info/connected');
    onValue(connectedRef, (snap) => {
      if (snap.val() === true) {
        const con = push(myConnectionsRef);
        onDisconnect(con).remove();
        set(con, true);
        onDisconnect(lastOnlineRef).set(serverTimestamp());
      }
    });
  }, [appState.userData])

  return (
    <>
      <AppContext.Provider value={{ ...appState, setUser: setAppState }}>
        <Routes>
          <Route path='/' element={<Landing />} />
          <Route path='/home' element={<Home />} />
          {user && <Route path='/create-channel' element={<GroupChat />} />}
          {user && <Route path="/channel" element={<Channel />} />}
          <Route path='/careers' element={<Careers />} />
          <Route path='/terms' element={<TermsAndRules />} />
          <Route path='/contact-us' element={<ContactUs />} />
          <Route path='/about-us' element={<About />} />
          <Route path='/faq' element={<FAQ />} />
          <Route path='/privacy' element={<PrivacyPolicy />} />
          {user && <Route path="/profile" element={<SidebarWithHeader />} />}
          {user && <Route path="/channel/:id" element={<MainLayout />} />}
          <Route path="/channel/:id/:chatId" element={<MainLayout />} />
          {user && <Route path="/friends" element={<FriendsView />} />}
          {user && <Route path="/friends/:id" element={<FriendsView />} />}
          {user && <Route path="/calls" element={<Calls />} />}
          <Route path="/log-in" element={<SplitScreen />} />
          <Route path="/sign-up" element={<SplitScreen2 />} />
          {user && <Route path='/fun' element={<Fun />} />}
          {user && <Route path='/youtube' element={<YouTube />} />}
          {user && <Route path='/music' element={<Music />} />}
          {user && <Route path='/chess' element={<Chess />} />}
          {user && <Route path='/draw' element={<Draw />} />}
          {user && <Route path='/calendar' element={<Calendar />} />}
          {user && <Route path='/friends-view' element={<FriendsNavBar />} />}
          {user && <Route path='/call-history' element={<CallHistory />} />}
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </AppContext.Provider>
    </>
  )
}

export default App
