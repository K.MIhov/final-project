/* eslint-disable react/prop-types */
// import PropTypes from 'prop-types';
import { Navigate } from "react-router-dom";
import { HOME_PAGE } from "../common/constants";

const Protected = ({ protectionCheck, children }) => {

    if (!protectionCheck) {
        alert('You don\'t have access to this functionality as a blocked user!');
        return <Navigate to={`${HOME_PAGE}`} replace />;
    }
    return children;
};

// Protected.propTypes = {
//     protectionCheck: PropTypes.bool.isRequired,
//     children: PropTypes.element.isRequired,
//   };
export default Protected;