import { get, set, ref, query, equalTo, orderByChild } from "firebase/database";
import { db } from "../config/firebase";

export const getUserByHandle = (handle) => {
  // console.log(handle)
  return get(ref(db, `users/${handle}`));
};

export const getUserByUid = (uid) => {
  return get(query(ref(db, "users"), orderByChild("uid"), equalTo(uid)));
};


export const createUserHandle = (
  handle,
  uid,
  displayName,
  phoneNumber,
  email,
  isAdmin,
  isBlocked,
  profileImageUrl,
  requestedCall,
  friendList
  // createdOn,
  // lastActive,
  // friendList = {},
) => {
  // console.log(profileImageUrl)
  return set(ref(db, `users/${handle}`), {
    handle,
    uid,
    displayName,
    phoneNumber,
    email,
    isAdmin: isAdmin,
    isBlocked: isBlocked,
    profileImageUrl: profileImageUrl,
    requestedCall: requestedCall,
    friendList: friendList,
    // createdOn: createdOn.getTime(),
    // lastActive: lastActive,
    // friendList,
  });
};

export const getUserData = (uid) => {
  if (!uid) {
    throw new Error("getUserData () called with undefined or null");
  }
  return get(query(ref(db, "users"), orderByChild("uid"), equalTo(uid)));
};

export const getUserFriendList = async (friendList) => {
  const friends =  await Promise.all(
    Object.keys(friendList).map((user) => {
      return getUserByHandle(user).then((snapshot) => snapshot.val());
    })
  );
  return friends;
}

export const getCallHistory = async (calls) => {
  const friends =  await Promise.all(
    Object.keys(calls).map((user) => {
      return getUserByHandle(user).then((snapshot) => snapshot.val());
    })
  );
  return friends;
}

export const getUserDataByEmail = (email) => {
  console.log(email);
  return get(query(ref(db, "users"), orderByChild("email"), equalTo(email)));
};

export const getChannelByTeamName = (name) => {
  return get(query(ref(db, "channels"), orderByChild("teamName"), equalTo(name)));
};
export const getUserDataByUserName = (handle) => {
  console.log(handle);
  return get(query(ref(db, "users"), orderByChild("handle"), equalTo(handle)));
};


const fromNotificationDocument = (snapshot) => {
  const userDoc = snapshot.val();

  return Object.keys(userDoc).map((key) => {
    const user = userDoc[key];
    // if (!channel.messages) channel.messages = {}
    return {
      ...user,
    };
  });
};

export const getAllUsers= () => {
  return get(ref(db, "users")).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return fromNotificationDocument(snapshot);
  });
};