import { db } from "../config/firebase";
import {
    update,
    ref,
    push,
    serverTimestamp
} from "firebase/database";
import { getUserData } from "./users.service";
import { getUserByHandle } from "./users.service";
export const requestSingleUserCall = async (currentUser, userToReach, id) => {
    await Promise.all([
        update(ref(db, `users/${userToReach}`), { requestedCall: { category: 'Single', data: { user: currentUser, meetingId: id } } }),
        push(ref(db, `users/${userToReach}/callHistory`), { type: `Incoming`, user: currentUser, createdOn: serverTimestamp() }),
        push(ref(db, `users/${currentUser}/callHistory`), { type: `Outgoing`, user: userToReach, createdOn: serverTimestamp() })
    ])
};

export const requestGroupCall = async (friends, currentUser, id) => {
    await Promise.all([
        friends.listOfParticipants.map(async (friend) => {
            return await getUserData(friend)
                .then(async (snapshot) => {
                    const value = snapshot.val();
                    const user = Object.values(value)[0];
                    if (currentUser.handle !== user.handle) {
                        if (friends.listOfParticipants.length > 1) {
                            await Promise.all([
                                update(ref(db, `users/${user.handle}`), { requestedCall: { category: 'Group', data: { user: friends, meetingId: id } } }),
                                // push(ref(db, `users/${friend}/callHistory`), { type: `Incoming`, user: userData.handle, createdOn: serverTimestamp() }),
                                // push(ref(db, `users/${userData.handle}/callHistory`), { type: `Outgoing`, user: friend.teamName, category: 'Group', createdOn: serverTimestamp() })
                            ])
                        }
                    }
                    return user;
                });
        })
    ])
};

export const getCallHistory = async (currentUser, allCalls, incomingCalls, outgoingCalls) => {

    await Promise.all(Object.values(currentUser.callHistory).map(async (data) => {
        const userSnapshot = await getUserByHandle(data.user);
        const user = userSnapshot.val();

        const callEntry = {
            user: user,
            type: data.type,
            timestamp: data.createdOn
        };

        if (data.type === 'Incoming') {
            incomingCalls.unshift(callEntry);
        } else if (data.type === 'Outgoing') {
            outgoingCalls.unshift(callEntry);
        }
        allCalls.unshift(callEntry);
    }))
}