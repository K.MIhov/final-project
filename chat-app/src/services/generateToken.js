//Auth token we will use to generate a meeting and connect to it
export const authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcGlrZXkiOiJmY2FjYTNmNS0wZTIwLTQwM2MtYWUxZC0xYmIyZTM5ZjVlOTUiLCJwZXJtaXNzaW9ucyI6WyJhbGxvd19qb2luIl0sImlhdCI6MTY4NjEyMDgwNSwiZXhwIjoxODQzOTA4ODA1fQ.H4ENc4ZRtPz7vMJl8PB6HxLWSHS91i3xt1Sg-eBMNps";
// API call to create meeting
export const createMeeting = async ({ token }) => {
    // console.log(token)
    // if()
  const res = await fetch(`https://api.videosdk.live/v2/rooms`, {
    method: "POST",
    headers: {
      authorization: `${token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({}),
  });
  //Destructuring the roomId from the response

  const { roomId } = await res.json();
  // console.log(roomId);
  return roomId;
};