import { ref, push, get, remove } from "firebase/database";
import { db } from "../config/firebase";

const fromNotificationDocument = (snapshot) => {
  const notificationDoc = snapshot.val();

  return Object.keys(notificationDoc).map((key) => {
    const notification = notificationDoc[key];
    // if (!channel.messages) channel.messages = {}
    return {
      ...notification,
      notificationId: key,
      created_at: new Date(notification.created_at),
    };
  });
};

export const getAllNotifications = () => {
  return get(ref(db, "notifications")).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return fromNotificationDocument(snapshot);
  });
};


//TODO: май трябва да сменя пътя за всеки чат
export const sendNotification = (uid, channelId, handle, timeStamp, message, type, team, messageId) => {
    const notification = {
      channel: channelId,
      idUser: uid,
      fromWho: handle,
      created_at: timeStamp,
      missedMessage: message,
      seen: false,
      type: type,
      teamInfo: team,
      idMessage: messageId
    };
    return push(ref(db, 'notifications'), notification);
};

//TODO: all notifications

export const removeSpecificNotification = (arrOfNotifications) => {
  arrOfNotifications.forEach(notification => {
    remove(ref(db, `notifications/${notification.notificationId}`))
  });
}