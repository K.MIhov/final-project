import { ref, push, get, remove, onValue, set } from "firebase/database";
import { db } from "../config/firebase";
import { getUserByHandle } from "./users.service";

export const addEvent = async (event, currentUserHandle, participantHandles) => {
  const newEventRef = push(ref(db, 'calendar'));
  const newEventKey = newEventRef.key;

  let participants = { [currentUserHandle]: true };
  for(let handle of participantHandles){
      const userSnapshot = await getUserByHandle(handle);
      if(userSnapshot.exists()){
          participants[handle] = true;
      }
  }

  return set(newEventRef, {
      id: newEventKey,
      ...event,
      participants,
  });
};



export const getEvents = (currentUserHandle) => {
  return get(ref(db, "calendar")).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    const events = snapshot.val();
    return Object.keys(events)
      .map((key) => ({ id: key, ...events[key] }))
      .filter((event) => event.participants.includes(currentUserHandle));
  });
};

export const deleteEvent = (eventId) => {
  return remove(ref(db, `calendar/${eventId}`));
};

export const subscribeToEvents = (callback, currentUserHandle) => {
  return onValue(ref(db, "calendar"), (snapshot) => {
    if (!snapshot.exists()) {
      callback([]);
      return;
    }

    const events = snapshot.val();
    const eventsArray = Object.keys(events).map((key) => ({
      id: key,
      title: events[key].title,
      start: new Date(events[key].start),
      end: new Date(events[key].end),
      allDay: events[key].allDay,
    })).filter((event) => event.participants && event.participants[currentUserHandle]);

    console.log(eventsArray);
    callback(eventsArray);
  });
};

