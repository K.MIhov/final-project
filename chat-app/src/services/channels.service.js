import { ref, push, get, set, remove, update, child } from "firebase/database";
import { db, storage } from "../config/firebase";
import { uploadBytes, getDownloadURL } from "firebase/storage";
import { ref as refStorage } from "firebase/storage";

const fromChannelDocument = (snapshot) => {
  const channelsDocument = snapshot.val();

  return Object.keys(channelsDocument).map((key) => {
    const channel = channelsDocument[key];
    return {
      ...channel,
      id: key,
      createdOn: new Date(channel.createdOn),
    };
  });
};

export const getAllChannels = () => {
  return get(ref(db, "channels")).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return fromChannelDocument(snapshot);
  });
};

export const addChannel = async (participants, uid) => {
  try {
    const channelRef = ref(db, "channels");
    const channelResult = await push(channelRef, {
      team: participants,
      owner: uid,
      createdOn: Date.now(),
      isPrivate: true,
      adminList: [uid],
    });

    const channelId = channelResult.key;
    const chatName = "general";

    await createChat(channelId, chatName);

    const channel = await getChannelById(channelId);
    const chatId = Object.keys(channel.chat)[0];
    const chat = channel.chat[chatId];

    return { ...channel, chat: { [chatId]: chat } };
  } catch (error) {
    throw new Error(`Error occurred while adding channel and chat: ${error.message}`);
  }
};

export const createChat = (channelId, chatName) => {
  const chat = {
    channelId: channelId,
    messages: {},
    name: chatName,
    createdOn: Date.now(),
  };

  return push(ref(db, `channels/${channelId}/chat`), chat).then(
    (chatResult) => {
      const chatId = chatResult.key;
      const chatWithId = {
        ...chat,
        id: chatId,
      };

      return set(ref(db, `channels/${channelId}/chat/${chatId}`), chatWithId);
    }
  );
};

export const getChannelById = (id) => {
  return get(ref(db, `channels/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Channel with id ${id} does not exist!`);
    }
    const channel = JSON.parse(JSON.stringify(result.val()));
    channel.id = id;
    channel.createdOn = new Date(channel.createdOn);

    return channel;
  });
};

export const addMessage = (id, channelId, message) => {
  // const messagesRef = ref(db, `chats/${id}/messages`);
  const messagesRef = ref(db, `channels/${channelId}/chat/${id}/messages`);
  const newMessageRef = push(messagesRef);
  const messageId = newMessageRef.key;

  const messageWithId = { ...message, id: messageId, reactions: message.reactions || [] };

  return set(newMessageRef, messageWithId);
};

export const deleteChannel = (id) => {
  return remove(ref(db, `channels/${id}`));
};

export const removeMember = (id, idMember, participants) => {
  const filterTeam = participants.filter((members) => members !== idMember);
  return update(ref(db, `channels/${id}/team`), {
    listOfParticipants: filterTeam,
  });
};

export const leaveChat = (id, idMember, participants) => {
  const filterTeam = participants.filter((member) => member !== idMember);
  update(ref(db, `channels/${id}/team/`), { listOfParticipants: filterTeam });
};

export const addTeamMember = (id, idMember, participants) => {
  const add = [...participants];
  add.push(idMember);
  update(ref(db, `channels/${id}/team/`), { listOfParticipants: add });
};

export const changePhoto = (name, id, file) => {
  if (file === null) {
    return;
  }
  if (file === null) {
    return;
  }

  const imageRef = refStorage(storage, `teamPhoto/${name}`);
  uploadBytes(imageRef, file)
    .then((snapshot) => getDownloadURL(snapshot.ref))
    .then((url) => update(ref(db, `channels/${id}/team`), { teamPhoto: url }))
    .catch((err) => console.error(err));
};

export const updateDescriptionText = (id, text) => {
  return update(ref(db, `channels/${id}/team`), { description: text });
};

export const markMessageAsSeen = (chatId, channelId, messageId, uid) => {
  const messageRef = ref(
    db,
    `channels/${channelId}/chat/${chatId}/messages/${messageId}`
  );
  const seenByRef = child(messageRef, "seenBy");

  const updates = { [uid]: true };
  return update(seenByRef, updates);
};

export const addOrRemoveReaction = async (channelId, chatId, messageId, reaction) => {
  // Get a reference to the reactions array in the database.
  const reactionsRef = ref(db, `channels/${channelId}/chat/${chatId}/messages/${messageId}/reactions`);

  // Get the current reactions from the database.
  const snapshot = await get(reactionsRef);
  const currentReactions = snapshot.exists() ? snapshot.val() : [];

  // Check if this user has already reacted with this emoji.
  const existingReactionIndex = currentReactions.findIndex(
    (r) => r.from === reaction.from && r.emoji === reaction.emoji
  );

  if (existingReactionIndex >= 0) {
    // Reaction already exists, remove it.
    currentReactions.splice(existingReactionIndex, 1);
  } else {
    // New reaction, add it.
    currentReactions.push(reaction);
  }

  // Write the updated reactions array back to the database.
  return set(reactionsRef, currentReactions);
};

export const addFileMessage = (id, channelId, file, user, seenByUsers) => {
  if (file === null) {
    return;
  }

  const fileRef = refStorage(storage, `files/${file.name}`);
  uploadBytes(fileRef, file)
    .then((snapshot) => getDownloadURL(snapshot.ref))
    .then((url) => {
      const messageWithFile = {
        from: user.handle,
        created_at: new Date().toISOString(),
        uid: user.uid,
        message: {
          name: file.name,
          url: url,
        },
        type: file.type,
        seenBy: seenByUsers,
      };

      addMessage(id, channelId, messageWithFile);
    })
    .catch((err) => console.error(err));
};

export const groupBy = (
  data,
  keySelector,
  valueSelector = (x => x)
) => {
  const map = new Map();

  for (const obj of data) {
      const key = keySelector(obj);
      const value = valueSelector(obj);

      if (!map.has(key)) {
        map.set(key, []);
      }

      map.get(key).push(value);
  }

  return map;
}