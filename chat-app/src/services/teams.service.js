import {
    ref,
    get,
  } from "firebase/database";
  import { db } from "../config/firebase";

const fromChatDocument = (snapshot) => {
  const teamDocument = snapshot.val();

  return Object.keys(teamDocument).map((key) => {
    const team = teamDocument[key];
    if (!team.teamPhoto) team.teamPhoto = ""
    if (!team.description) team.description = ""
    return {
      ...team,
    };
  });
};

export const getTeam = (id) => {
  return get(ref(db, `channels/${id}/team`))
  .then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return fromChatDocument(snapshot);
  });
};