import { LogoutOutlined, SettingOutlined, CrownOutlined, UserOutlined, LeftCircleOutlined } from '@ant-design/icons';

export const CHANNELS_MIN_LENGTH_TITLE = 3;

export const CHANNELS_MAX_LENGTH_TITLE = 40;

export const TEAMS_MIN_LENGTH_TITLE = 3;

export const TEAMS_MAX_LENGTH_TITLE = 40;

export const TWITTER_URL = "https://twitter.com/";

export const FACEBOOK_URL = "https://www.facebook.com/";

export const YOUTUBE_URL = "https://www.youtube.com/";

export const INSTAGRAM_URL = "https://www.instagram.com/";

export const GITLAB_URL =
  "https://about.gitlab.com/why-gitlab/?utm_medium=cpc&utm_source=google&utm_campaign=singleappci_emea_pr_rsa_nb_phrase&utm_content=why-gitlab&utm_term=gitlab%20ci%20tools&_bt=644592637178&_bk=gitlab%20ci%20tools&_bm=b&_bn=g&_bg=145507599576&gclid=CjwKCAjwo7iiBhAEEiwAsIxQEdDp_vLGZ8tcrPdK0iAEgmlC_WTPLI9iEsloQQVWiwQ8zFzrP8ROmRoChC8QAvD_BwE";

export const FOOTER_IMAGE =
  "https://i.ibb.co/sgr67G7/Geometric-Fox-Head-Gaming-Technology-Logo-2.png";


export const ALEX_IMG = "https://i.imgur.com/BUAsE4c.jpg";

export const PETKO_IMG = "https://i.imgur.com/dVDsyAH.jpg";

export const KRASI_IMG = "https://imgur.com/S5l11gO.jpg";

export const userInfo = [
    {
        label: 'Display Name',
        value: 'displayName',
        buttonText: 'Edit',
    },
    {
        label: 'Email',
        value: 'email',
        buttonText: 'Edit',
    },
    {
        label: 'Phone Number',
        value: 'phoneNumber',
        buttonText: 'Edit',
    },
];

export const CHANNEL_PAGE = '/channel';

export const LinkItems = [
    { name: 'Back', icon: LeftCircleOutlined, nav: `/channel` },
    { name: 'My Account', icon: UserOutlined },
    { name: 'Profile', icon: CrownOutlined },
    // { name: 'Settings', icon: SettingOutlined },
    { name: 'Log out', icon: LogoutOutlined }
];

export const moreItems = ['Start Voice Call', 'Start Video Call'];

export const MenuItems = ['Edit Profile', 'Friend List', 'Switch Account'];

export const toasted = [
    {
        title: "Please enter a valid email",
        status: "error",
        duration: 5000,
        isClosable: true
    },
    {
        title: 'handle must be between 4 and 32 symbols',
        status: "error",
        duration: 5000,
        isClosable: true,
    },
    {
        title: "Please enter a Display Name",
        status: "error",
        duration: 5000,
        isClosable: true
    },
    {
        title: "Password must be between 7 and 64 symbols",
        status: "error",
        duration: 5000,
        isClosable: true
    },
    {
        title: "Username already taken",
        status: "error",
        duration: 5000,
        isClosable: true
    },
    {
        title: "Please enter valid credentials",
        status: "error",
        duration: 5000,
        isClosable: true
    },
    {
       title: "successfully logged in",
        status: "success",
        duration: 5000,
        isClosable: true 
    },
    {
        title: "change successfully added",
        status: "success",
        duration: 5000,
        isClosable: true
    },
    {
        title: "Please enter a text between 5 and 30 characters",
        status: "error",
        duration: 5000,
        isClosable: true
    },
]
export const initialForm = {
    email: '',
    password: '',
    handle: '',
    phoneNumber: '',
    displayName: '',
    isAdmin: false,
    isBlocked: false,
    profileImageUrl: 'none',
    userStatus: "none",
    createdOn: new Date(),
}

export const formControl = [
    { id: 'email', input: 'Email', },
    { id: 'handle', input: 'Username' },
    { id: 'displayName', input: 'Display Name' },
    { id: 'phoneNumber', input: 'Phone Number' },
    // { id: 'firstName', input: 'First Name' },
    // { id: 'password', input: 'Password'},
]

export const GIPHY_API_KEY = 'aLKjscAAUvCH0DxH1xWYBonUELV64qlK';

export const YOUTUBE_API_KEY = 'AIzaSyAlTCee37mUeb2XJbpxCUXx8Paqt9_PCO8';

export const LANDING_PAGE = '/';

export const HOME_PAGE = '/home';

export const CREATE_CHANNEL_PAGE = '/create-channel';

export const CAREERS_PAGE = '/careers';

export const TERMS_PAGE = '/terms';

export const CONTACT_US_PAGE = '/contact-us';

export const ABOUT_US_PAGE = '/about-us';

export const FAQ_PAGE = '/faq';

export const PRIVACY_PAGE = '/privacy';

export const PROFILE_PAGE = '/profile';

export const FRIENDS_PAGE = '/friends';

export const CALLS_PAGE = '/calls';

export const LOG_IN_PAGE = '/log-in';

export const SIGN_UP_PAGE = '/sign-up';

export const FUN_PAGE = '/fun';

export const YOUTUBE_PAGE = '/youtube';

export const MUSIC_PAGE = '/music';

export const CHESS_PAGE = '/chess';

export const DRAW_PAGE = '/draw';

export const CALENDAR_PAGE = '/calendar';

export const FONT_SIZE_SIDEBARCONTACTS = {
    base: "14px",
    lg: "14px",
    xl: "20px",
    "2xl": "20px",
}

export const PADDING_SIDEBARCONTACTS = {
    base: "5px",
    lg: "8px",
    xl: "10px",
    "2xl": "10px",
}

export const MARGIN_SIDEBARCONTACTS = {
    base: "14px",
    lg: "14px",
    xl: "20px",
    "2xl": "20px",
}