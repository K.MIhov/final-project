export const status = {
    ONLINE: 'Online',
    OFFLINE: 'Offline',
    BUSY: 'Busy',
    AWAY: 'Away',
    IN_A_MEETING: 'In a meeting',
};