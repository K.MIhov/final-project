import React from "react";

const UserProfileContext = React.createContext({
  userPosts: [], // Default values
  userComments: [],
});

export default UserProfileContext;
