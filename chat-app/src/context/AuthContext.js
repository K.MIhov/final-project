import { createContext } from "react";

const AppContext = createContext({
  user: null,
  userData: null,
  userFriends: null,
  setUser() {
    // real implementation comes from App.jsx
  },
});

export default AppContext;
